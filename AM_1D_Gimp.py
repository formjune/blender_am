# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


import os
import math
from gimpfu import *


def mergeComposeBottomLayers(image, layer):
    pdb.gimp_image_undo_group_start(image)
    split_index = image.layers.index(layer)
    pdb.gimp_message(split_index)
    length = len(image.layers)
    for merge_layer in image.layers[length + 1:split_index - 1:-1]:
        copy = pdb.gimp_layer_new_from_drawable(merge_layer, image)
        pdb.gimp_image_insert_layer(image, copy, None, length)
    background = pdb.gimp_layer_new(image, image.width, image.height, RGBA_IMAGE, "merged", 100, NORMAL_MODE)
    pdb.gimp_image_insert_layer(image, background, None, length)
    while length != len(image.layers) - 1:
        background = image.layers[length]
        pdb.gimp_image_merge_down(image, background, EXPAND_AS_NECESSARY)

    for i in xrange(split_index + 1):
        background = image.layers[length]
        layer = image.layers[i]
        name = layer.name
        copy = pdb.gimp_layer_new_from_drawable(background, image)
        pdb.gimp_image_insert_layer(image, copy, None, i + 1)
        pdb.gimp_image_merge_down(image, layer, EXPAND_AS_NECESSARY)
        image.layers[i].name = name

    image.remove_layer(image.layers[length])
    pdb.gimp_image_undo_group_end(image)
    pdb.gimp_displays_flush()


def topLayerInAllLayers(image, layer):
    pdb.gimp_image_undo_group_start(image)
    split_index = image.layers.index(layer) + 1
    sources = [layer for layer in image.layers[split_index - 1::-1] if layer.visible]
    targets = [i for i in range(split_index, len(image.layers)) if image.layers[i].visible]
    pdb.gimp_message("%i source layers | %i target layers" % (len(sources), len(targets)))
    for current_source in sources:
        for current_target in targets:
            current_target = image.layers[current_target]
            i = pdb.gimp_image_get_item_position(image, current_target)
            copy = pdb.gimp_layer_new_from_drawable(current_source, image)
            pdb.gimp_image_insert_layer(image, copy, None, i)
            pdb.gimp_image_merge_down(image, copy, EXPAND_AS_NECESSARY)

    pdb.gimp_image_undo_group_end(image)
    pdb.gimp_displays_flush()

   
def exportAllLayers(image, layer, save_path, save_ext, compression):
    for layer in image.layers:
        top_layer = pdb.gimp_layer_new_from_drawable(layer, image)
        top_layer.mode = 28
        top_layer.opacity = 100.0
        file_name = os.path.join(save_path, "%s.%s" % (layer.name, save_ext))
        if save_ext == "png":
            pdb.file_png_save(image, top_layer, file_name, file_name, 1, compression, 0, 0, 0, 0, 0)
        else:
            pdb.gimp_file_save(image, top_layer, file_name, '?')


def iterateIndexedColor(image, layer, v_1=256, v_2=8, steps=5, dither=0, step_type=0, show_steps=False):
    def indexed(value):
        if image.base_type != 0:
            pdb.gimp_image_convert_rgb(image)
        pdb.gimp_image_convert_indexed(image, dither, 0, value, False, True, '')
        steps_array.append(str(int(value)))

    steps_array = []
    pdb.gimp_image_undo_group_start(image)
    indexed(v_1)
    for divide in xrange(steps, 1, -1):
        if step_type == 0:
            vl_1 = math.log(v_1)
            vl_2 = math.log(v_2)
            vl_1 -= (vl_1 - vl_2) / divide
            v_1 = int(round(math.e ** vl_1))
        else:
            v_1 -= (v_1 - v_2) / divide
            v_1 = int(round(v_1))
        indexed(v_1)
    indexed(v_2)

    pdb.gimp_image_undo_group_end(image)
    pdb.gimp_displays_flush()
    if show_steps:
        pdb.gimp_message(", ".join(steps_array))


register(
    "merge_compose_top_layers",
    "Puts copies of top visible layers over other visible layers and merge down",
    "Puts copies of top visible layers over other visible layers and merge down",
    "Andrey Menshikov, 1D_Inc (concept designer)",
    "Andrey Menshikov, 1D_Inc (concept designer)",
    "October 2018",
    "<Image>/1D/Merge Compose Top Layers",
    "RGB*, GRAY*",
    [],
    [],
    topLayerInAllLayers)

register(
        "export_all_layers",
        "Export all layers as raw images",
        "Export all layers as raw images",
        "Andrey Menshikov, 1D_Inc (concept designer)",
        "Andrey Menshikov, 1D_Inc (concept designer)",
        "August 2019",
        "<Image>/1D/Export All Layers",
        "RGB*, GRAY*",
        [
            (PF_DIRNAME, "path_dir", "path", ""),
            (PF_STRING, "extension", "extension", "png"),
            (PF_SPINNER, "png_compression", "compress", 7, [0, 9, 1])
         ],
        [],
        exportAllLayers)

register(
    "merge_compose_bottom_layers",
    "Puts copies of top visible layers over other visible layers and merge down",
    "Puts copies of top visible layers over other visible layers and merge down",
    "Andrey Menshikov, 1D_Inc (concept designer)",
    "Andrey Menshikov, 1D_Inc (concept designer)",
    "March 2020",
    "<Image>/1D/Merge Compose Bottom Layers",
    "RGB*, GRAY*",
    [],
    [],
    mergeComposeBottomLayers)


register(
        "iterate_indexed_color",
        "Gradually decrease number of indexed colors",
        "Gradually decrease number of indexed colors",
        "Andrey Menshikov, 1D_Inc (concept designer)",
        "Andrey Menshikov, 1D_Inc (concept designer)",
        "April 2020",
        "<Image>/1D/Iterate Indexed Color",
        "INDEXED*, RGB*, GRAY*",
        [
            (PF_SPINNER, "from", "from", 256, [1, 256, 1]),
            (PF_SPINNER, "to", "to", 8, [1, 256, 1]),
            (PF_INT, "step", "step", 5),
            (PF_OPTION, "dither", "dither", 0, ("none", "fs", "fslowbleed", "fixed")),
            (PF_OPTION, "step_type", "step type", 0, ("logarithm", "linear")),
            (PF_BOOL, "log", "show log", True)
        ],
        [],
        iterateIndexedColor)


main()
