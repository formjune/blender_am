"""
3d match
selection order: up, center, aim

2d match
selection order: center, aim
no height - preserve Z position

align pivot
selection order: up, center, aim
scale not preserved

match pivot
copy matrix from active to selected objects
"""


import mathutils
import bpy
import bmesh
# from .amTools import *


bl_info = {
    "name": "AM 1D Match",
    "author": "Andrey Menshikovn, Paul Kotelevets aka 1D_Inc (concept design)",
    "version": ("1", "0", "2"),
    "blender": (2, 7, 9),
    "location": "View3D > Tool Shelf > 1D > AM1D",
    "description": "AM 1D script pack",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Mesh"}


class BlenderParam(object):
    """wrapper for blender global options"""

    def __init__(self, name):
        self.name = name

    def __call__(self):
        return self.get()

    def get(self):
        return bpy.context.scene.am_match_settings.__getattribute__(self.name)

    def set(self, value):
        bpy.context.scene.am_match_settings.__setattribute__(self.name, value)


def createMatrix(p, aim, up, scale=mathutils.Vector((1, 1, 1))):
    """build matrix. aim - X, up - Y"""
    aim = (aim - p).normalized()
    up = up - p
    v = aim.cross(up).normalized()
    up = v.cross(aim).normalized()
    return mathutils.Matrix([
        [aim.x * scale.x, up.x * scale.y, v.x * scale.z, p.x],
        [aim.y * scale.x, up.y * scale.y, v.y * scale.z, p.y],
        [aim.z * scale.x, up.z * scale.y, v.z * scale.z, p.z],
        [0, 0, 0, 1]
    ])


def getPoints(mesh, number=3):
    """get N last points"""
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.context.scene.objects.active = mesh
    bpy.ops.object.mode_set(mode="EDIT")
    bm = bmesh.from_edit_mesh(mesh.data)

    vertices = []
    for elem in reversed(bm.select_history):
        if isinstance(elem, bmesh.types.BMVert):
            vertices.append(elem)
            if len(vertices) == number:
                break

    if len(vertices) == 3:
        for edge in vertices[0].link_edges:
            if edge.other_vert(vertices[0]) == vertices[2]:
                vertices[1], vertices[2] = vertices[2], vertices[1]
                break

    result = [mesh.matrix_world * v.co for v in vertices], [v.index for v in vertices]
    bmesh.update_edit_mesh(mesh.data)
    bm.free()
    bpy.ops.object.mode_set(mode="OBJECT")
    if len(vertices) == number:
        return result
    else:
        raise ValueError("not enough in history")


def moveObject(matrix_source, matrix_target):
    """match selected objects"""
    for mesh in bpy.context.selected_objects:
        mesh.matrix_world = matrix_target * (matrix_source.inverted() * mesh.matrix_world)


class Store3Points(bpy.types.Operator):
    bl_idname = "mesh.am1d_match_3_store"
    bl_label = "AM MATCH 3 Points Store"
    bl_description = "store target by 3 vertices: up, center, aim"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        try:
            points = getPoints(context.active_object, 3)[0]
        except ValueError:
            self.report({"ERROR"}, "not enough in history")
            return {"FINISHED"}
        Apply3Points.aim = points[0]
        Apply3Points.center = points[1]
        Apply3Points.up = points[2]
        return {"FINISHED"}


class Apply3Points(bpy.types.Operator):
    bl_idname = "mesh.am1d_match_3"
    bl_label = "AM MATCH 3 Points"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "move all selected objects by active's 3 vertices: up, center, aim"
    center = mathutils.Vector()
    aim = mathutils.Vector((1, 0, 0))
    up = mathutils.Vector((0, 1, 0))

    def execute(self, context):
        try:
            points = getPoints(context.active_object, 3)[0]
        except ValueError:
            self.report({"ERROR"}, "not enough in history")
            return {"FINISHED"}
        moveObject(createMatrix(points[1], points[0], points[2]), createMatrix(self.center, self.aim, self.up))
        return {"FINISHED"}


class Store2Points(bpy.types.Operator):
    bl_idname = "mesh.am1d_match_2_store"
    bl_label = "AM MATCH 2 Points Store"
    bl_description = "store target by 2 vertices: center, aim"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        try:
            points = getPoints(context.active_object, 2)[0]
        except ValueError:
            self.report({"ERROR"}, "not enough in history")
            return {"FINISHED"}
        Apply2Points.aim = points[0]
        Apply2Points.center = points[1]
        return {"FINISHED"}


class Apply2Points(bpy.types.Operator):
    bl_idname = "mesh.am1d_match_2"
    bl_label = "AM MATCH 2 Points"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "move all selected objects by active's 2 vertices: center, aim"
    center = mathutils.Vector()
    aim = mathutils.Vector((1, 0, 0))
    is_flat = bpy.props.BoolProperty(default=False)

    def execute(self, context):
        try:
            points = getPoints(context.active_object, 2)[0]
        except ValueError:
            self.report({"ERROR"}, "not enough in history")
            return {"FINISHED"}
        if self.is_flat:
            self.center.z = points[1].z
        self.aim.z = self.center.z
        up = self.center + mathutils.Vector((0, 0, 1))
        points[0].z = points[1].z
        up_source = points[1] + mathutils.Vector((0, 0, 1))
        moveObject(createMatrix(points[1], points[0], up_source), createMatrix(self.center, self.aim, up))
        return {"FINISHED"}


class AlignPivot(bpy.types.Operator):
    bl_idname = "mesh.am1d_align_pivot"
    bl_label = "AM MATCH Align Pivot"
    bl_description = "align pivot of active or selected objects by 3 vertices: up, center, aim"
    bl_options = {'REGISTER', 'UNDO'}

    TABLE = {
        "XY": ((1, 0, 0), (0, 1, 0)), "X-Y": ((1, 0, 0), (0, -1, 0)),
        "XZ": ((1, 0, 0), (0, 0, -1)), "X-Z": ((1, 0, 0), (0, 0, 1)),
        "YX": ((0, 1, 0), (1, 0, 0)), "Y-X": ((0, -1, 0), (1, 0, 0)),
        "YZ": ((0, 0, 1), (1, 0, 0)), "Y-Z": ((0, 0, -1), (1, 0, 0)),
        "ZX": ((0, 1, 0), (0, 0, 1)), "Z-X": ((0, -1, 0), (0, 0, -1)),
        "ZY": ((0, 0, -1), (0, 1, 0)), "Z-Y": ((0, 0, 1), (0, -1, 0)),
        "-XY": ((-1, 0, 0), (0, 1, 0)), "-X-Y": ((-1, 0, 0), (0, -1, 0)),
        "-XZ": ((-1, 0, 0), (0, 0, 1)), "-X-Z": ((-1, 0, 0), (0, 0, -1)),
        "-YX": ((0, 1, 0), (-1, 0, 0)), "-Y-X": ((0, -1, 0), (-1, 0, 0)),
        "-YZ": ((0, 0, -1), (-1, 0, 0)), "-Y-Z": ((0, 0, 1), (-1, 0, 0)),
        "-ZX": ((0, 1, 0), (0, 0, -1)), "-Z-X": ((0, -1, 0), (0, 0, 1)),
        "-ZY": ((0, 0, 1), (0, 1, 0)), "-Z-Y": ((0, 0, -1), (0, -1, 0)),
    }

    pivot = BlenderParam("align_pivot")

    def execute(self, context):
        try:
            points, indices = getPoints(context.active_object, 3)
        except ValueError:
            self.report({"ERROR"}, "not enough in history")
            return {"FINISHED"}

        aim, up = self.TABLE[self.pivot()]
        scale = context.active_object.matrix_world.to_scale()
        matrix = createMatrix(mathutils.Vector(), mathutils.Vector(aim), mathutils.Vector(up))
        matrix = createMatrix(points[1], points[0], points[2], scale) * matrix

        # store vertices keys
        matrix_list = []
        bpy.ops.object.select_linked(type='OBDATA')
        for node in context.selected_objects:
            if node == context.active_object:
                continue
            old_matrix = createMatrix(
                node.matrix_world * node.data.vertices[indices[1]].co,
                node.matrix_world * node.data.vertices[indices[0]].co,
                node.matrix_world * node.data.vertices[indices[2]].co
            )
            scales = {
                (node.matrix_world * mathutils.Vector((1, 0, 0, 0))).freeze(): node.scale.x,
                (node.matrix_world * mathutils.Vector((-1, 0, 0, 0))).freeze(): node.scale.x,
                (node.matrix_world * mathutils.Vector((0, 1, 0, 0))).freeze(): node.scale.y,
                (node.matrix_world * mathutils.Vector((0, -1, 0, 0))).freeze(): node.scale.y,
                (node.matrix_world * mathutils.Vector((0, 0, 1, 0))).freeze(): node.scale.z,
                (node.matrix_world * mathutils.Vector((0, 0, -1, 0))).freeze(): node.scale.z
            }
            matrix_list.append((node, old_matrix, scales))

        # move vertices
        for v in context.active_object.data.vertices:
            v.co = matrix.inverted() * (context.active_object.matrix_world * v.co)
        context.active_object.matrix_world = matrix

        # match instances
        for node, old_matrix, scales in matrix_list:
            new_matrix = createMatrix(
                node.matrix_world * node.data.vertices[indices[1]].co,
                node.matrix_world * node.data.vertices[indices[0]].co,
                node.matrix_world * node.data.vertices[indices[2]].co
            )
            node.matrix_world = old_matrix * (new_matrix.inverted() * node.matrix_world)
            for axis in range(3):
                vector = mathutils.Vector((0, 0, 0, 0))
                vector[axis] = 1
                vector = node.matrix_world * vector
                node.scale[axis] = scales[min(scales, key=lambda k: vector.angle(k))]
        return {"FINISHED"}


# temp section

class Settings(bpy.types.PropertyGroup):
    align_pivot = bpy.props.EnumProperty(
        items=[
            ("XY", "yx", ""),
            ("XZ", "zx", ""),
            ("YX", "xy", ""),
            ("YZ", "zy", ""),
            ("ZX", "xz", ""),
            ("ZY", "yz", "")],
        default="YX")
    cbs_match_object_3d = bpy.props.BoolProperty(default=False)
    cbs_match_object_2d = bpy.props.BoolProperty(default=False)
    cbs_match_pivot = bpy.props.BoolProperty(default=False)


class CollapseMaker(object):
    """creates collapse boxes"""

    def __init__(self, main_layout, settings):
        self.main_layout = main_layout
        self.settings = settings

    def get(self, prop_name, text, layout=None):
        if layout:
            column = layout.column()
        else:
            column = self.main_layout.column()
        splitter = column.split()
        prop = self.settings.__getattribute__(prop_name)
        icon = 'DOWNARROW_HLT' if prop else 'RIGHTARROW'
        splitter.prop(self.settings, prop_name, text=text, icon=icon)
        if prop:
            box = column.column(align=True).box().column()
            column_inside = box.column(align=True)
            return column_inside


class Layout(bpy.types.Panel):
    bl_label = "AM 1D Match"
    bl_idname = "Andrey 1D Match"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = '1D'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        """col - main column layout. do not rewrite
        col_in - main column inside every section
        col_in_n - sub layout"""
        s = context.scene.am_match_settings
        maker = CollapseMaker(self.layout, s)  # create collapsable columns

        column_1 = maker.get("cbs_match_object_3d", "3D Match")
        if column_1:
            row_2 = column_1.row()
            row_2.operator("mesh.am1d_match_3_store", text="store")
            row_2.operator("mesh.am1d_match_3", text="apply")

        column_1 = maker.get("cbs_match_object_2d", "XY Match")
        if column_1:
            column_1.operator("mesh.am1d_match_2_store", text="store")
            row_2 = column_1.row()
            row_2.operator("mesh.am1d_match_2", text="apply").is_flat = False
            row_2.operator("mesh.am1d_match_2", text="apply no height").is_flat = True

        column_1 = maker.get("cbs_match_pivot", "Pivot")
        if column_1:
            row_2 = column_1.row()
            row_2.prop(s, "align_pivot", text="up aim", expand=True)
            column_1.operator("mesh.am1d_align_pivot", text="align pivot")


def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.am_match = bpy.props.PointerProperty(type=bpy.types.PropertyGroup)
    bpy.types.Scene.am_match_settings = bpy.props.PointerProperty(type=Settings)


register()