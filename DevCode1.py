
import bpy
import bmesh
import mathutils


class LoopProfile(bpy.types.Operator):
    bl_idname = "mesh.am1d_loop_profile"
    bl_label = "Loop Profile"
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def createMatrix(p, aim, up=mathutils.Vector((0, 0, 1)), scale=1):
        """build matrix. aim - X, up - Y"""
        aim = (aim - p).normalized()
        v = aim.cross(up).normalized()
        up = v.cross(aim).normalized()
        return mathutils.Matrix([
            [aim.x * scale, up.x * scale, v.x * scale, p.x],
            [aim.y * scale, up.y * scale, v.y * scale, p.y],
            [aim.z * scale, up.z * scale, v.z * scale, p.z],
            [0, 0, 0, 1]
        ])

    def execute(self, context):

        bpy.ops.object.mode_set(mode="OBJECT")
        pipe = bpy.context.active_object
        shape = [mesh for mesh in context.selected_objects if mesh != pipe][0]
        for vertex in shape.data.vertices:
            if vertex.select:
                scale_in = (shape.matrix_world * vertex.co - shape.matrix_world.translation).to_3d().length
                matrix_in = self.createMatrix(shape.matrix_world.translation, shape.matrix_world * vertex.co).inverted()
                break
        else:
            raise RuntimeError

        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(pipe.data)
        for vertex in bm.verts:
            if not vertex.select:
                continue

            for edge in vertex.link_edges:
                if edge.select:
                    continue

                other_vertex = edge.other_vert(vertex)
                point_1 = pipe.matrix_world * vertex.co
                point_2 = pipe.matrix_world * other_vertex.co
                scale = (point_1 - point_2).length / scale_in
                normal = vertex.normal.to_4d()
                normal.w = 0
                normal = (pipe.matrix_world * normal).to_3d()
                matrix_out = self.createMatrix(point_1, point_2, normal, scale)
                shape_copy = shape.copy()
                shape_copy.data = shape.data
                bpy.data.scenes[0].objects.link(shape_copy)
                shape_copy.matrix_world = matrix_out * (matrix_in * shape.matrix_world)
        bm.update_edit_mesh(pipe.data)
        bpy.ops.object.mode_set(mode="OBJECT")
        return {"FINISHED"}
