# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# https://bitbucket.org/formjune/blender_am


# VERSION LOG:
# 1.12.48 (20-06-06): select shortest edges fix
# 1.13.00 (20-06-07): svg and maplus are separated
# 1.13.01 (20-07-07): poke view loops added
# 1.13.02 (20-07-07): poke view loops renamed
# 1.13.03 (20-07-07): poke loops report updated
# 1.13.04 (20-08-04): select minimal seam added
# 1.13.05 (20-08-04): select minimal seam multi mesh
# 1.13.06 (20-08-04): select minimal seam mode
# 1.13.07 (20-08-04): select minimal seam edit mode
# 1.13.09 (20-08-04): select minimal seam edit mode
# 1.13.10 (20-08-08): multi union from bargool
# 1.13.11 (20-08-14): multi substract
# 1.13.12 (20-08-14): multi substract checkbox off
# 1.13.13 (20-08-18): mats select border
# 1.13.14 (20-08-18): mats select border
# 1.13.15 (20-08-30): vertex round position
# 1.13.16 (20-09-2): embelishment
# 1.13.17 (20-09-9): corner edges added
# 1.13.21 (20-09-13): scribe auto select text
# 1.13.22 (20-09-17): vertex round checkboxes
# 1.13.23 (20-09-17): vertex round checkboxes
# 1.13.24 (20-09-20): mats equalize and colorize added. poke loops removed. poke loops view renamed
# 1.13.25 (20-09-21): mats equalize renamed. added hue
# 1.13.29 (20-09-23): active material support. bug fix
# 1.13.30 (20-09-25): mats colorize rename
# 1.13.31 (20-09-27): put triangulation added
# 1.13.32 (20-10-01): select triangulation added
# 1.13.33 (20-10-04): added divider
# 1.13.34 (20-10-11): added offset
# 1.13.35 (20-10-11): obname
# 1.13.36 (20-10-15): find and replace
# 1.13.38 (20-10-28): contour match
# 1.13.39 (20-10-28): contour match
# 1.13.40 (20-11-21): offset edges
# 1.14.0 (20-11-22): freed from release
# 1.14.0 (21-01-12): pavel script added
# 1.14.5 (21-02-21): symmetry select added
# 1.14.7 (21-03-02): loop profile added


import math
import collections
import bpy
import mathutils
import bmesh


bl_info = {
    "name": "AM 1D Test",
    "author": "Andrey Menshikovn, Paul Kotelevets aka 1D_Inc (concept design)",
    "version": ("1", "14", "7"),
    "blender": (2, 7, 9),
    "location": "View3D > Tool Shelf > 1D > AM1D",
    "description": "AM 1D script pack",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Mesh"}


class Tools(object):

    @staticmethod
    def safeEditMode():
        """reset bmesh for edit mode"""
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")

    @staticmethod
    def activeVertex(bm):
        for elem in reversed(bm.select_history):
            if isinstance(elem, bmesh.types.BMVert):
                return elem
        return None

    @staticmethod
    def activeEdge(bm):
        for elem in reversed(bm.select_history):
            if isinstance(elem, bmesh.types.BMEdge):
                return elem
        return None

    @staticmethod
    def activeFace(bm):
        for elem in reversed(bm.select_history):
            if isinstance(elem, bmesh.types.BMFace):
                return elem
        return None

    @staticmethod
    def view3d(context):
        # returns first 3d view, normally we get from context
        for area in context.window.screen.areas:
            if area.type == "VIEW_3D":
                v3d = area.spaces[0]
                rv3d = v3d.region_3d
                for region in area.regions:
                    if region.type == "WINDOW":
                        return region, rv3d, v3d, area
        return None, None, None, None


class BlenderParam(object):
    """wrapper for blender global options"""

    def __init__(self, name):
        self.name = name

    def __call__(self):
        return self.get()

    def get(self):
        return bpy.context.scene.am_test_settings.__getattribute__(self.name)

    def set(self, value):
        bpy.context.scene.am_test_settings.__setattribute__(self.name, value)


class Settings(bpy.types.PropertyGroup):
    splash_loop_type = bpy.props.EnumProperty(
        items=[
            ("view", "view", ""),
            ("middle", "middle", ""),
            ("far", "far", ""),
            ("near", "near", "")
        ],
        default="view")
    splash_loop_end = bpy.props.BoolProperty(default=False)
    splash_loop_percent = bpy.props.FloatProperty(default=10, min=0, max=10, step=10, precision=2)
    minimal_seam_number = bpy.props.IntProperty(default=2, min=1, step=1)
    multi_union_triangulate = bpy.props.BoolProperty(default=False)
    multi_union_solver = bpy.props.EnumProperty(
        items=[
            ("BMESH", "BMESH", ""),
            ("CARVE", "CARVE", "")
        ])
    vertex_round_x = bpy.props.BoolProperty(default=False)
    vertex_round_y = bpy.props.BoolProperty(default=False)
    vertex_round_z = bpy.props.BoolProperty(default=True)
    vertex_round_digits = bpy.props.IntProperty(default=2, min=0, max=10, step=1)
    vertex_round_step = bpy.props.FloatProperty(default=0, min=0, step=1, precision=3)
    do_offset_percent = bpy.props.FloatProperty(default=.1, min=0, max=1, precision=3, step=10)
    find_and_replace_source = bpy.props.StringProperty()
    find_and_replace_target = bpy.props.StringProperty()
    select_ratio_faces_n = bpy.props.FloatProperty(default=500, min=0, precision=3, step=1000)
    select_asym_axis = bpy.props.EnumProperty(
        items=[
            ("X", "X", ""),
            ("Y", "Y", ""),
            ("Z", "Z", "")
        ],
        default="X")
    select_asym_method = bpy.props.EnumProperty(
        items=[
            ("FAST", "FAST", ""),
            ("ACCURATE", "ACCURATE", "")
        ],
        default="FAST")
    select_asym_tolerance = bpy.props.FloatProperty(default=1e-3, min=0, step=1, precision=3)


class CollapseSettings(bpy.types.PropertyGroup):
    cbs_splash_loop = bpy.props.BoolProperty(default=False)
    cbs_minimal_seam = bpy.props.BoolProperty(default=False)
    cbs_multi_union = bpy.props.BoolProperty(default=False)
    cbs_vertex_round = bpy.props.BoolProperty(default=False)
    cbs_do_offset = bpy.props.BoolProperty(default=False)
    cbs_find_replace = bpy.props.BoolProperty(default=False)
    cbs_contour = bpy.props.BoolProperty(default=False)
    cbs_ratio_faces = bpy.props.BoolProperty(default=False)
    cbs_select_asym = bpy.props.BoolProperty(default=False)


class SplashLoop(bpy.types.Operator):
    bl_idname = "mesh.am1d_splash_loop"
    bl_label = "Splash Loop"
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def getConnected(vertex):
        vertices = []
        for edge in vertex.link_edges:
            other_vertex = edge.other_vert(vertex)
            if other_vertex.select:
                vertices.append(other_vertex)
        return vertices

    @classmethod
    def findPaths(cls, bm):

        end_vertices = {v for v in bm.verts if v.select and len(cls.getConnected(v)) == 1}
        all_vertices = {v for v in bm.verts if v.select}

        paths = []
        while end_vertices:
            vertex = end_vertices.pop()
            path = [vertex]
            all_vertices.discard(vertex)

            while vertex not in end_vertices:
                next_vertices = [v for v in cls.getConnected(vertex) if v in all_vertices]
                if len(next_vertices) != 1:
                    return []
                vertex = next_vertices.pop()
                all_vertices.discard(vertex)
                path.append(vertex)

            end_vertices.discard(vertex)
            if len(path) > 2:
                paths.append(path)
        return paths

    def execute(self, context):

        bpy.ops.object.mode_set(mode="EDIT")
        mesh = context.active_object
        matrix = mesh.matrix_world
        matrix_inverted = matrix.inverted()
        bm = bmesh.from_edit_mesh(mesh.data)
        active_vertex = None
        if bm.select_history:
            elem = bm.select_history[-1]
            if isinstance(elem, bmesh.types.BMVert):
                active_vertex = elem

        paths = self.findPaths(bm)
        percent = context.scene.am_test_settings.splash_loop_percent / 10
        for path in paths:

            # camera
            rv3d = Tools.view3d(context)[1]
            vector_cam = mathutils.Vector((0, 0, 1)) * rv3d.view_matrix - mathutils.Vector() * rv3d.view_matrix
            vector_cam.normalize()
            point_1 = matrix * path[0].co
            point_2 = matrix * path[-1].co

            if context.scene.am_test_settings.splash_loop_type == "view":
                vector_up = point_1 - point_2
                vector_up.normalize()

            else:
                vector_up = mathutils.Vector((1, 0, 0)) * rv3d.view_matrix - mathutils.Vector() * rv3d.view_matrix
                vector_up.normalize()
                if context.scene.am_test_settings.splash_loop_type == "middle":
                    point_1 = (point_1 + point_2) / 2
                elif context.scene.am_test_settings.splash_loop_type == "far":
                    point_1 = max([point_1, point_2], key=lambda p: (p - mathutils.Vector() * rv3d.view_matrix).length)
                else:
                    point_1 = min([point_1, point_2], key=lambda p: (p - mathutils.Vector() * rv3d.view_matrix).length)
                point_2 = point_1 + vector_up

            if active_vertex in path[1:-1]:
                point_3 = matrix * active_vertex.co
            else:
                point_3 = point_1 + vector_cam.cross(vector_up)
            if not context.scene.am_test_settings.splash_loop_end:
                path = path[1:-1]

            for vertex in path:
                point = matrix * vertex.co
                result = mathutils.geometry.intersect_ray_tri(point_1, point_2, point_3, vector_cam, point, False) \
                    or mathutils.geometry.intersect_ray_tri(point_1, point_2, point_3, -vector_cam, point, False)
                if result is not None:
                    vertex.co = matrix_inverted * (point + (result - point) * percent)
        bmesh.update_edit_mesh(context.active_object.data)
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        return {"FINISHED"}


class SelectShortestEdges(bpy.types.Operator):
    bl_idname = "mesh.am1d_select_shortest_edges"
    bl_label = "Select Shortest Edges"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='EDIT')
        bm = bmesh.from_edit_mesh(context.active_object.data)
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='EDGE')
        for f in bm.faces:
            f.select = False
        edges = [e for e in bm.edges if not e.hide]
        edges = set(sorted(edges, key=lambda e: e.calc_length())[:context.scene.am_test_settings.minimal_seam_number])
        for e in bm.edges:
            e.select = e in edges
        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='EDIT')
        return {"FINISHED"}


class ShortSeamChains(bpy.types.Operator):
    bl_idname = "mesh.am1d_short_seam_chains"
    bl_label = "Short Seam Chains"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):

        if context.mode == "OBJECT":
            for mesh in context.selected_objects:
                mesh.select = bool(self.checkMesh(mesh, context))
            self.report({"INFO"}, "%i objects contain short seams steps" % len(context.selected_objects))

        else:
            bpy.ops.object.mode_set(mode='OBJECT')
            edges_number = self.checkMesh(context.active_object, context)
            if edges_number:
                bpy.ops.object.mode_set(mode='EDIT')
            self.report({"INFO"}, "%i edges are selected" % edges_number)

        return {"FINISHED"}

    @staticmethod
    def checkMesh(mesh, context):
        if mesh.type != "MESH":
            return 0

        context.scene.objects.active = mesh
        data = []  # (edges, verts),

        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(context.active_object.data)
        for edge in bm.edges:
            if not edge.seam:
                continue
            group_1 = group_2 = -1
            for i, block in enumerate(data):
                if group_1 == -1 and edge.verts[0] in block[1]:
                    group_1 = i
                if group_2 == -1 and edge.verts[1] in block[1]:
                    group_2 = i

            # two different groups
            if group_1 != -1 and group_2 != -1 and group_1 != group_2:
                data[group_1][0].extend(data[group_2][0])
                data[group_1][1].update(data[group_2][1])
                block = data[group_1]
                data.pop(group_2)

            # same groups
            elif group_1 == group_2 and group_1 != -1:
                block = data[group_1]

            # first group
            elif group_1 != -1:
                block = data[group_1]

            # second group
            elif group_2 != -1:
                block = data[group_2]

            # create new group
            else:
                data.append(([], set()))
                block = data[-1]

            block[0].append(edge)
            block[1].add(edge.verts[0])
            block[1].add(edge.verts[1])

        for face in bm.faces:
            face.select = False

        edge_number = 0
        for block in data:
            if len(block[0]) > context.scene.am_test_settings.minimal_seam_number:
                continue
            for edge in block[0]:
                edge_number += 1
                edge.select = True

        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode='OBJECT')
        return edge_number


class BatchUnionOperator(bpy.types.Operator):
    bl_idname = 'mesh.am1d_bool_multi_union'
    bl_label = 'AM Bool Multi Union'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        obj = context.active_object
        if obj.data.users > 1:
            self.report({'ERROR'}, 'Active is multiuser data object')
            return {'CANCELLED'}
        do_triangulate = context.scene.am_test_settings.multi_union_triangulate
        solver = context.scene.am_test_settings.multi_union_solver
        scene = context.scene
        if do_triangulate:
            for o in context.selected_objects:
                # Only in edge select
                # bpy.ops.mesh.select_non_manifold()
                scene.objects.active = o
                bpy.ops.object.mode_set(mode='EDIT')
                bpy.ops.mesh.select_all(action='SELECT')
                bpy.ops.mesh.quads_convert_to_tris()
                bpy.ops.object.mode_set(mode='OBJECT')

        scene.objects.active = obj
        selected_objects = [o for o in context.selected_objects if o != obj]
        for o in selected_objects:
            bpy.ops.object.modifier_add(type='BOOLEAN')
            obj.modifiers["Boolean"].operation = 'UNION'
            obj.modifiers["Boolean"].object = o
            obj.modifiers["Boolean"].solver = solver
            bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Boolean")
            if do_triangulate:
                bpy.ops.object.mode_set(mode='EDIT')
                bpy.ops.mesh.select_all(action='SELECT')
                bpy.ops.mesh.quads_convert_to_tris()
                bpy.ops.object.mode_set(mode='OBJECT')
        obj.select = False
        bpy.ops.object.delete()
        obj.select = True
        return {'FINISHED'}


class BatchDifferenceOperator(bpy.types.Operator):
    bl_idname = 'mesh.am1d_bool_multi_difference'
    bl_label = 'AM Bool Multi Difference'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        obj = context.active_object
        if obj.data.users > 1:
            self.report({'ERROR'}, 'Active is multiuser data object')
            return {'CANCELLED'}
        do_triangulate = context.scene.am_test_settings.multi_union_triangulate
        solver = context.scene.am_test_settings.multi_union_solver
        scene = context.scene

        for o in context.selected_objects:
            if o == obj:
                continue
            new_o = o.copy()
            new_o.data = o.data.copy()
            bpy.data.scenes[0].objects.link(new_o)
            new_o.select = True
            o.select = False

        if do_triangulate:
            for o in context.selected_objects:
                scene.objects.active = o
                bpy.ops.object.mode_set(mode='EDIT')
                bpy.ops.mesh.select_all(action='SELECT')
                bpy.ops.mesh.quads_convert_to_tris()
                bpy.ops.object.mode_set(mode='OBJECT')

        scene.objects.active = obj
        selected_objects = [o for o in context.selected_objects if o != obj]
        for o in selected_objects:
            bpy.ops.object.modifier_add(type='BOOLEAN')
            obj.modifiers["Boolean"].operation = 'DIFFERENCE'
            obj.modifiers["Boolean"].object = o
            obj.modifiers["Boolean"].solver = solver
            bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Boolean")
            if do_triangulate:
                bpy.ops.object.mode_set(mode='EDIT')
                bpy.ops.mesh.select_all(action='SELECT')
                bpy.ops.mesh.quads_convert_to_tris()
                bpy.ops.object.mode_set(mode='OBJECT')
        obj.select = False
        bpy.ops.object.delete()
        obj.select = True
        return {'FINISHED'}


class VertexRoundPositionDigits(bpy.types.Operator):
    bl_idname = 'mesh.am1d_vertex_round_position_digits'
    bl_label = 'Vertex Round Position Digits'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        x = bpy.context.scene.am_test_settings.vertex_round_x
        y = bpy.context.scene.am_test_settings.vertex_round_y
        z = bpy.context.scene.am_test_settings.vertex_round_z
        n = bpy.context.scene.am_test_settings.vertex_round_digits
        if context.mode == "EDIT_MESH":
            self.editMode(context, n, x, y, z)
        else:
            self.objectMode(context, n, x, y, z)
        return {"FINISHED"}

    @staticmethod
    def objectMode(context, n, x, y, z):
        for node in context.selected_objects:
            point = node.matrix_world.translation
            point = (round(point.x, n) if x else point.x,
                     round(point.y, n) if y else point.y,
                     round(point.z, n) if z else point.z)
            node.matrix_world.translation = mathutils.Vector(point)

    @staticmethod
    def editMode(context, n, x, y, z):
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        matrix = context.active_object.matrix_world
        matrix_back = matrix.inverted()
        bm = bmesh.from_edit_mesh(context.active_object.data)

        for vertex in bm.verts:
            if not vertex.select:
                continue
            point = matrix * vertex.co
            point = (round(point.x, n) if x else point.x,
                     round(point.y, n) if y else point.y,
                     round(point.z, n) if z else point.z)
            vertex.co = matrix_back * mathutils.Vector(point)

        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")


class VertexRoundPositionStep(bpy.types.Operator):
    bl_idname = 'mesh.am1d_vertex_round_position_step'
    bl_label = 'Vertex Round Position Step'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        x = bpy.context.scene.am_test_settings.vertex_round_x
        y = bpy.context.scene.am_test_settings.vertex_round_y
        z = bpy.context.scene.am_test_settings.vertex_round_z
        n = bpy.context.scene.am_test_settings.vertex_round_step
        if context.mode == "EDIT_MESH":
            self.editMode(context, n, x, y, z)
        else:
            self.objectMode(context, n, x, y, z)
        return {"FINISHED"}

    @staticmethod
    def round(value, n, new):
        if not new:
            return value
        d, m = divmod(abs(value), n)
        return math.copysign(n * (d + round(m / n)), value)

    @classmethod
    def objectMode(cls, context, n, x, y, z):
        for node in context.selected_objects:
            point = node.matrix_world.translation
            point = cls.round(point.x, n, x), cls.round(point.y, n, y), cls.round(point.z, n, z)
            node.matrix_world.translation = mathutils.Vector(point)

    @classmethod
    def editMode(cls, context, n, x, y, z):
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        matrix = context.active_object.matrix_world
        matrix_back = matrix.inverted()
        bm = bmesh.from_edit_mesh(context.active_object.data)

        for vertex in bm.verts:
            if not vertex.select:
                continue
            point = matrix * vertex.co
            point = cls.round(point.x, n, x), cls.round(point.y, n, y), cls.round(point.z, n, z)
            vertex.co = matrix_back * mathutils.Vector(point)

        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")


class BuildOFFSet(bpy.types.Operator):
    bl_idname = "mesh.am1d_build_offset"
    bl_label = "Build OFFset"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        Tools.safeEditMode()
        bm = bmesh.from_edit_mesh(context.active_object.data)
        matrix = context.active_object.matrix_world
        points = {(matrix * face.calc_center_median()).freeze() for face in bm.faces if face.select}
        try:
            point = matrix * Tools.activeFace(bm).calc_center_median()
        except AttributeError:
            point = mathutils.Vector()
        points_list = []
        while points:
            point = min(points, key=lambda p: (point - p).length)
            points.remove(point)
            points_list.append(point)

        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")

        # create new object
        mesh = bpy.data.objects.new("FF", bpy.data.meshes.new("mesh"))
        context.scene.objects.link(mesh)
        context.scene.objects.active = mesh

        # fill data
        Tools.safeEditMode()
        bm = bmesh.from_edit_mesh(context.active_object.data)
        vertices = [bm.verts.new(p) for p in points_list]
        for i in range(len(vertices) - 1):
            bm.edges.new((vertices[i], vertices[i + 1]))
        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")
        return {"FINISHED"}


class DoOFFSet(bpy.types.Operator):
    bl_idname = "mesh.am1d_do_offset"
    bl_label = "Do OFFset"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        percent = context.scene.am_test_settings.do_offset_percent
        bpy.ops.object.mode_set(mode="OBJECT")
        active_object = context.active_object
        matrix_back = active_object.matrix_world.inverted()
        try:
            context.scene.objects.active = context.scene.objects["FF"]
        except KeyError:
            self.report({"INFO"}, "FF doesn't exist")
            return {"FINISHED"}
        Tools.safeEditMode()
        bm = bmesh.from_edit_mesh(context.active_object.data)
        matrix = context.active_object.matrix_world
        points = [matrix_back * (matrix * vertex.co) for vertex in bm.verts]
        if not points:
            self.report({"INFO"}, "FF has no vertices")
            return {"FINISHED"}
        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")

        # apply
        context.scene.objects.active = active_object
        Tools.safeEditMode()
        bm = bmesh.from_edit_mesh(context.active_object.data)
        for vertex in bm.verts:
            if not vertex.select:
                continue
            point = min(points, key=lambda p: (vertex.co - p).length)
            vertex.co = point * percent + vertex.co * (1.0 - percent)

        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        return {"FINISHED"}


class FindAndReplace(bpy.types.Operator):
    bl_idname = "mesh.am1d_find_and_replace"
    bl_label = "Find and Replace"
    bl_options = {'REGISTER', 'UNDO'}

    name_source = BlenderParam("find_and_replace_source")
    name_target = BlenderParam("find_and_replace_target")

    def execute(self, context):
        for mesh in context.selected_objects:
            mesh.name = mesh.name.replace(self.name_source(), self.name_target(), 1)
        return {"FINISHED"}


class ContourMatch(bpy.types.Operator):
    bl_idname = "mesh.am1d_contour_match"
    bl_label = "Contour Match"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        active = context.active_object
        selected = [mesh for mesh in context.selected_objects if mesh != active][0]

        # proceed selected
        bpy.ops.object.mode_set(mode="OBJECT")
        context.scene.objects.active = selected
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(selected.data)
        points = []
        for vertex in bm.verts:
            if not vertex.is_boundary:
                continue
            points.append(selected.matrix_world * vertex.co)
        bm.free()
        selected.select = False

        # proceed active
        bpy.ops.object.mode_set(mode="OBJECT")
        context.scene.objects.active = active
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(active.data)
        matrix_back = active.matrix_world.inverted()
        for vertex in bm.verts:
            if not vertex.is_boundary:
                vertex.select = False
                continue
            vertex.select = True
            point = active.matrix_world * vertex.co
            vertex.co = matrix_back * min(points, key=lambda p: (point - p).length)
        bmesh.update_edit_mesh(active.data)
        bm.free()

        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        return {"FINISHED"}


class VertexMarch(bpy.types.Operator):
    bl_idname = "mesh.am1d_vertex_march"
    bl_label = "Vertex March"
    bl_options = {'REGISTER', 'UNDO'}

    reverse = bpy.props.BoolProperty(default=False)

    def execute(self, context):
        active = context.active_object

        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(active.data)
        vertex = None
        for vertex in bm.verts:
            if vertex.select:
                break
        if vertex is None:
            return {"FINISHED"}

        visited = {vertex}
        start_point = vertex.co.copy()
        while True:
            edges = reversed(vertex.link_edges) if self.reverse else vertex.link_edges
            for edge in edges:
                next_vertex = edge.other_vert(vertex)
                if not next_vertex.select or next_vertex in visited:
                    continue
                vertex.co = next_vertex.co
                visited.add(next_vertex)
                vertex = next_vertex
                break

            # no next vertex
            else:
                vertex.co = start_point
                bmesh.update_edit_mesh(active.data)
                bm.free()
                return {"FINISHED"}


class ContourMarch(bpy.types.Operator):
    bl_idname = "mesh.am1d_contour_march"
    bl_label = "Contour March ++"
    bl_options = {'REGISTER', 'UNDO'}

    stop = bpy.props.BoolProperty(default=True)

    @staticmethod
    def vertexList(mesh, update_select=False):

        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.context.scene.objects.active = mesh
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(mesh.data)
        vertex = Tools.activeVertex(bm)
        visited = set()
        index_list = []

        # find next to start
        for edge in vertex.link_edges:
            next_vertex = edge.other_vert(vertex)
            if next_vertex.select:
                visited.add(vertex)
                index_list.append(vertex.index)
                vertex = next_vertex
                break

        while True:
            visited.add(vertex)
            if update_select:
                vertex.select = True
            index_list.append(vertex.index)
            for edge in vertex.link_edges:
                if not edge.is_boundary:
                    continue
                next_vertex = edge.other_vert(vertex)
                if not next_vertex.is_boundary or next_vertex in visited:
                    continue
                vertex = next_vertex
                break
            else:
                bm.free()
                bpy.ops.object.mode_set(mode="OBJECT")
                return index_list

    def execute(self, context):
        active = context.active_object
        selected = context.scene.objects["++"]
        vertex_target = self.vertexList(selected)
        vertex_source = self.vertexList(active, True)
        if len(vertex_source) != len(vertex_target):
            self.report({"INFO"},
                        "vertex arrays have different lengths\n%i vs %i" % (len(vertex_source), len(vertex_target)))
            if self.stop:
                return {"FINISHED"}

        matrix = active.matrix_world.inverted()
        for i, j in zip(vertex_target, vertex_source):
            active.data.vertices[j].co = matrix * (selected.matrix_world * selected.data.vertices[i].co)
        selected.select = False
        return {"FINISHED"}


class Extend(bpy.types.Operator):

    bl_idname = 'mesh.am1d_corner_edges'
    bl_label = 'Corner Edges'
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def bm_vert_active_get(bm):
        for elem in reversed(bm.select_history):
            if isinstance(elem, (bmesh.types.BMVert, bmesh.types.BMEdge, bmesh.types.BMFace)):
                return elem.index, str(elem)[3:4]
        return None, None

    @classmethod
    def get_active_edge(cls, bm):
        result = None
        elem, el = cls.bm_vert_active_get(bm)
        if elem != None:
            mode_ = str(el)[3:4]
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_mode(type='EDGE')
            bm.edges[elem].verts[1]
            elem, el = cls.bm_vert_active_get(bm)
            if elem != None:
                result = [bm.edges[elem].verts[0].index, bm.edges[elem].verts[1].index]

            if mode_ == 'V':
                bpy.ops.mesh.select_mode(type='VERT')
            elif mode_ == 'E':
                bpy.ops.mesh.select_mode(type='EDGE')
            elif mode_ == 'F':
                bpy.ops.mesh.select_mode(type='FACE')
            bpy.ops.object.mode_set(mode='OBJECT')
        return result

    def execute(self, context):

        view_matrix = Tools.view3d(context)[1].view_matrix.transposed()
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")

        obj = bpy.context.active_object
        me = obj.data

        bm = bmesh.new()
        bm.from_mesh(me)
        if hasattr(bm.verts, "ensure_lookup_table"):
            bm.verts.ensure_lookup_table()
            bm.edges.ensure_lookup_table()
            bm.faces.ensure_lookup_table()

        loop1 = self.get_active_edge(bm)
        if not loop1:
            self.report({"INFO"}, "No active edge selected")
            bm.free()
            bpy.ops.object.mode_set(mode="OBJECT")
            bpy.ops.object.mode_set(mode="EDIT")
            return {"FINISHED"}

        sel_edges = [e for e in bm.edges if e.select and e.verts[0].index not in loop1]
        loops = [[e.verts[0].index, e.verts[1].index] for e in sel_edges]
        if not loops:
            self.report({"INFO"}, "No edges to move")
            bm.free()
            bpy.ops.object.mode_set(mode="OBJECT")
            bpy.ops.object.mode_set(mode="EDIT")
            return {"FINISHED"}

        # create plane
        verts = me.vertices
        center = obj.matrix_world * verts[loop1[0]].co
        vec_1 = obj.matrix_world * verts[loop1[1]].co - center
        vec_2 = view_matrix * mathutils.Vector((0, 0, 1))
        normal = vec_1.cross(vec_2).normalized()

        for loop2 in loops:
            v1, v2 = loop2
            point_1 = obj.matrix_world * verts[v1].co
            point_2 = obj.matrix_world * verts[v2].co
            if abs(normal * (center - point_2)) < abs(normal * (center - point_1)):
                point_1, point_2 = point_2, point_1
                v1, v2 = v2, v1

            vector = (point_1 - point_2).normalized()
            if not vector * normal:
                continue
            length = normal * (center - point_1)
            if not length:
                continue
            length = abs(length) / math.cos(vector.angle(normal * length))
            verts[v1].co = obj.matrix_world.inverted() * (point_1 + vector * length)

        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        return {"FINISHED"}


class SetPlusName(bpy.types.Operator):
    """add ++ into name of upper objects"""

    bl_idname = "mesh.am1d_set_plus_name"
    bl_label = "Set ++ Names"
    bl_options = {'REGISTER', 'UNDO'}
    node = collections.namedtuple("add_plus_name", "node pivot height")

    def execute(self, context):

        nodes = []
        for node in bpy.data.objects:
            if not node.select or node.type != "MESH":
                continue
            center = mathutils.Vector(node.matrix_world.col[3][:2])  # XY center only
            height = node.matrix_world.col[3][2]
            nodes.append(self.node(node, center, height))

        # find lower node
        for node in nodes:
            for bottom_node in nodes:
                if (bottom_node.pivot - node.pivot).length <= .1 and bottom_node.height < node.height:
                    node.node.name = bottom_node.node.name + "++"
                    break

        return {"FINISHED"}


class SelectedCoordsToFile(bpy.types.Operator):
    """save list of selected nodes into file for autocad export"""

    bl_idname = "mesh.am1d_get_selected_coords_to_text"
    bl_label = "Get Coords From Selected"
    bl_options = {'REGISTER', 'UNDO'}
    file_name = "Coords"

    def execute(self, context):

        if self.file_name in bpy.data.texts:
            text_block = bpy.data.texts[self.file_name]
        else:
            text_block = bpy.data.texts.new(name=self.file_name)
        text_block.clear()

        for node in bpy.data.objects:
            if not node.select:
                continue

            text_block.write("%s/%s/(%.3f %.3f %.3f)\n" % (node.name, node.data.name, *node.matrix_world.col[3][:3]))

        return {"FINISHED"}


class ZeroScale(bpy.types.Operator):

    bl_idname = "mesh.am1d_zero_scale"
    bl_label = "Zero Scale"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.transform.resize(value=(0, 0, 0), constraint_axis=(False, False, False),
                                 constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED',
                                 proportional_edit_falloff='SMOOTH', proportional_size=6.7275)
        return {"FINISHED"}


class IndexMats(bpy.types.Operator):
    bl_idname = "mesh.am1d_index_mats"
    bl_label = "Index Mats"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        active = context.active_object
        selected = [node for node in context.selected_objects if node != active]
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(active.data)
        materials = [m.material_index for m in bm.faces]
        bpy.ops.object.mode_set(mode="OBJECT")

        for mesh in selected:
            for i in range(len(active.data.materials)):
                mesh.data.materials[i] = active.data.materials[i]
            context.scene.objects.active = mesh
            bpy.ops.object.mode_set(mode="EDIT")
            bm = bmesh.from_edit_mesh(mesh.data)
            for face, index in zip(bm.faces, materials):
                face.material_index = index
            bpy.ops.object.mode_set(mode="OBJECT")
        return {"FINISHED"}


class SelectRatioFaces(bpy.types.Operator):
    bl_idname = "mesh.am1d_select_ratio_faces"
    bl_label = "Select SA ratio faces"
    bl_options = {'REGISTER', 'UNDO'}

    n = BlenderParam("select_ratio_faces_n")

    def execute(self, context):
        if context.mode == "OBJECT":
            for mesh in context.selected_objects:
                mesh.select = mesh.type == "MESH" and self.checkMesh(mesh)
        else:
            if self.checkMesh(context.active_object):
                bpy.ops.object.mode_set(mode="EDIT")
            else:
                bpy.ops.object.mode_set(mode="OBJECT")
        return {"FINISHED"}

    def checkMesh(self, mesh):
        bpy.context.scene.objects.active = mesh
        Tools.safeEditMode()
        bm = bmesh.from_edit_mesh(mesh.data)
        count = False
        for face in bm.faces:
            area = max(face.calc_area(), 1e-4)
            perimeter = 0
            for edge in face.edges:
                perimeter += (edge.verts[0].co - edge.verts[1].co).length

            if perimeter / area >= self.n():
                count = face.select = True
            else:
                face.select = False

        bmesh.update_edit_mesh(mesh.data)
        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")
        return count


class MatsSelectBorder(bpy.types.Operator):
    bl_idname = 'mesh.am1d_mats_select_border'
    bl_label = 'Mats Select Border'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='EDIT')
        bm = bmesh.from_edit_mesh(context.active_object.data)
        # selection = [edge.select for edge in bm.edges]
        # if True not in selection:
        #     selection = [True] * len(bm.edges)
        for face in bm.faces:
            face.select = False
        bpy.ops.mesh.select_mode(type="EDGE")
        for edge in bm.edges:
            if len(edge.link_faces) != 2:
                continue
            edge.select = edge.link_faces[0].material_index != edge.link_faces[1].material_index
        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='EDIT')
        return {"FINISHED"}


class SelectBetween(bpy.types.Operator):
    bl_idname = "mesh.am1d_select_between"
    bl_label = "Select Between"
    bl_description = "Select all objects between two selected in Outliner"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        all_nodes = sorted([node for node in context.scene.objects], key=lambda node: node.name)
        nodes = [node for node in all_nodes if node.select]
        if len(nodes) != 2:
            return {"FINISHED"}
        active_layers = {i for i in range(20) if bpy.data.scenes["Scene"].layers[i]}
        start = all_nodes.index(nodes[0])
        end = all_nodes.index(nodes[1])
        for node in all_nodes[start + 1:end]:
            if active_layers.intersection({i for i in range(20) if node.layers[i]}):
                node.select = True
            else:
                node.select = False
        return {"FINISHED"}


class DistanceFromCenter(bpy.types.Operator):
    """save list of selected nodes into file for autocad export"""

    bl_idname = "mesh.am1d_get_coords_from_center"
    bl_label = "Get Coords from Center"
    bl_options = {'REGISTER', 'UNDO'}
    file_name = "Coords"

    def execute(self, context):

        if self.file_name in bpy.data.texts:
            text_block = bpy.data.texts[self.file_name]
        else:
            text_block = bpy.data.texts.new(name=self.file_name)
        text_block.clear()

        points = []
        for node in bpy.data.objects:
            if not node.select or node.type != "MESH":
                continue

            center = mathutils.Vector(node.matrix_world.col[3][:3])
            matrix = node.matrix_world

            for number, point in enumerate(node.data.vertices):
                points.append((node.name, number, (matrix * point.co - center).length))

        points.sort(key=lambda x: x[2])
        for point in points:
            text_block.write("%s[%i] - %.3f\n" % point)

        return {"FINISHED"}


class ApplyAllBooleans(bpy.types.Operator):
    bl_idname = "mesh.am1d_apply_all_booleans"
    bl_label = "Apply All Booleans"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for modifier in tuple(context.active_object.modifiers):
            if modifier.type != "BOOLEAN":
                continue
            try:
                bpy.ops.object.modifier_apply(modifier=modifier.name)
            except RuntimeError:
                pass
        return {"FINISHED"}


class MeshDisplayOn(bpy.types.Operator):

    bl_idname = "mesh.am1d_mesh_display_on"
    bl_label = "Mesh Display On"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for mesh in context.selected_objects:
            if mesh.type != "MESH":
                continue
            context.scene.objects.active = mesh
            context.object.data.show_faces = True
            context.object.data.show_edges = True
            context.object.data.show_edge_crease = True
            context.object.data.show_edge_seams = True
            context.object.data.show_edge_sharp = True
        return {"FINISHED"}


class SelectAsym(bpy.types.Operator):

    bl_idname = "mesh.am1d_select_asym"
    bl_label = "Select Asym"
    bl_options = {'REGISTER', 'UNDO'}
    tolerance = BlenderParam("select_asym_tolerance")
    method = BlenderParam("select_asym_method")
    axis = BlenderParam("select_asym_axis")

    def execute(self, context):

        Tools.safeEditMode()
        bpy.ops.mesh.select_mode(type="VERT")
        bpy.ops.object.mode_set(mode="OBJECT")
        mesh = context.active_object
        tolerance = self.tolerance()
        axis = "XYZ".index(self.axis())
        method = self.method()

        for f in mesh.data.polygons:
            f.select = False
        for e in mesh.data.edges:
            e.select = False
        for v in mesh.data.vertices:
            v.select = False

        no_pairs = set(mesh.data.vertices)
        for v in mesh.data.vertices:
            if v not in no_pairs:
                continue

            point = v.co.copy()
            point[axis] *= -1

            if method == "FAST":
                face_id = mesh.closest_point_on_mesh(point)[-1]
                if face_id == -1:
                    continue
                vertices = [mesh.data.vertices[v] for v in mesh.data.polygons[face_id].vertices]
            else:
                vertices = no_pairs

            for next_v in vertices:
                if next_v == v:
                    continue
                if (next_v.co - point).length <= tolerance:
                    no_pairs.discard(next_v)
                    no_pairs.discard(v)
                    break

        for v in no_pairs:
            v.select = True
        bpy.ops.object.mode_set(mode="EDIT")
        self.report({"INFO"}, "%i vertices are asymmetrical" % len(no_pairs))
        return {"FINISHED"}


class SelectSym(bpy.types.Operator):
    bl_idname = "mesh.am1d_select_sym"
    bl_label = "Select Sym"
    bl_options = {'REGISTER', 'UNDO'}
    tolerance = BlenderParam("select_asym_tolerance")
    method = BlenderParam("select_asym_method")
    axis = BlenderParam("select_asym_axis")

    def execute(self, context):

        Tools.safeEditMode()
        bpy.ops.mesh.select_mode(type="VERT")
        bpy.ops.object.mode_set(mode="OBJECT")
        mesh = context.active_object
        tolerance = self.tolerance()
        axis = "XYZ".index(self.axis())
        method = self.method()

        to_select = {v for v in mesh.data.vertices if v.select}
        for v in list(to_select):
            point = v.co.copy()
            point[axis] *= -1
            if method == "FAST":
                face_id = mesh.closest_point_on_mesh(point)[-1]
                if face_id == -1:
                    continue
                vertices = [mesh.data.vertices[v] for v in mesh.data.polygons[face_id].vertices]
            else:
                vertices = mesh.data.vertices

            for next_v in vertices:
                if next_v == v:
                    continue
                if (next_v.co - point).length <= tolerance:
                    to_select.add(next_v)
                    break

        for f in mesh.data.polygons:
            f.select = False
        for e in mesh.data.edges:
            e.select = False
        for v in mesh.data.vertices:
            v.select = v in to_select
        bpy.ops.object.mode_set(mode="EDIT")
        return {"FINISHED"}


class LoopProfile(bpy.types.Operator):
    bl_idname = "mesh.am1d_loop_profile"
    bl_label = "Loop Profile"
    bl_description = """Spreads profile by active loop mesh object
Loop object needs loop selection, profile needs single vertex selection to set origin-to-vertex alignment size."""
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def createMatrix(p, aim, up=mathutils.Vector((0, 0, 1)), scale=1):
        """build matrix. aim - X, up - Y"""
        aim = (aim - p).normalized()
        v = aim.cross(up).normalized()
        up = v.cross(aim).normalized()
        return mathutils.Matrix([
            [aim.x * scale, up.x * scale, v.x * scale, p.x],
            [aim.y * scale, up.y * scale, v.y * scale, p.y],
            [aim.z * scale, up.z * scale, v.z * scale, p.z],
            [0, 0, 0, 1]
        ])

    def execute(self, context):

        bpy.ops.object.mode_set(mode="OBJECT")
        pipe = bpy.context.active_object
        shape = [mesh for mesh in context.selected_objects if mesh != pipe][0]
        for vertex in shape.data.vertices:
            if vertex.select:
                scale_in = (shape.matrix_world * vertex.co - shape.matrix_world.translation).to_3d().length
                matrix_in = self.createMatrix(shape.matrix_world.translation, shape.matrix_world * vertex.co).inverted()
                break
        else:
            self.report({"ERROR"}, "select 1 vertex on shape mesh")
            return {"FINISHED"}

        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(pipe.data)
        for vertex in bm.verts:
            if not vertex.select:
                continue

            for edge in vertex.link_edges:
                if edge.select:
                    continue

                other_vertex = edge.other_vert(vertex)
                point_1 = pipe.matrix_world * vertex.co
                point_2 = pipe.matrix_world * other_vertex.co
                scale = (point_1 - point_2).length / scale_in
                normal = vertex.normal.to_4d()
                normal.w = 0
                normal = (pipe.matrix_world * normal).to_3d()
                matrix_out = self.createMatrix(point_1, point_2, normal, scale)
                shape_copy = shape.copy()
                shape_copy.data = shape.data
                bpy.data.scenes[0].objects.link(shape_copy)
                shape_copy.matrix_world = matrix_out * (matrix_in * shape.matrix_world)
        bpy.ops.object.mode_set(mode="OBJECT")
        return {"FINISHED"}


class CollapseMaker(object):
    """creates collapse boxes"""

    def __init__(self, main_layout, settings):
        self.main_layout = main_layout
        self.settings = settings

    def get(self, prop_name, text, layout=None):
        if layout:
            column = layout.column()
        else:
            column = self.main_layout.column()
        splitter = column.split()
        prop = self.settings.__getattribute__(prop_name)
        icon = 'DOWNARROW_HLT' if prop else 'RIGHTARROW'
        splitter.prop(self.settings, prop_name, text=text, icon=icon)
        if prop:
            box = column.column(align=True).box().column()
            column_inside = box.column(align=True)
            return column_inside


class Layout(bpy.types.Panel):
    bl_label = "AM 1D Test"
    bl_idname = "Andrey 1D Test"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = '1D'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        """col - main column layout. do not rewrite
        col_in - main column inside every section
        col_in_n - sub layout"""
        maker = CollapseMaker(self.layout, context.scene.am_test_collapse_settings)  # create collapsable columns
        s = context.scene.am_test_settings
        column = self.layout.column(align=True)
        
        column_1 = maker.get("cbs_splash_loop", "Splash Loop", column)
        if column_1:
            column_1.prop(s, "splash_loop_type", text="")
            column_1.prop(s, "splash_loop_percent", text="influence")
            column_1.prop(s, "splash_loop_end", text="move ends")
            column_1.operator("mesh.am1d_splash_loop", text="Splash Loop")

        column_1 = maker.get("cbs_minimal_seam", "Select Minimal", column)
        if column_1:
            column_1.prop(s, "minimal_seam_number", text="")
            column_1.operator("mesh.am1d_short_seam_chains", text="Short Seam Chains")
            column_1.operator("mesh.am1d_select_shortest_edges", text="Select Shortest Edges")

        column_1 = maker.get("cbs_multi_union", "Bool Multi Union", column)
        if column_1:
            column_1.prop(s, "multi_union_solver", text="solver")
            column_1.prop(s, "multi_union_triangulate", text="triangulate")
            column_1.operator("mesh.am1d_bool_multi_union", text="Bool Multi Union")
            column_1.operator("mesh.am1d_bool_multi_difference", text="Bool Multi Difference")

        column_1 = maker.get("cbs_vertex_round", "Vertex Round Position", column)
        if column_1:
            row_2 = column_1.row()
            row_2.prop(s, "vertex_round_x", text="X", toggle=True)
            row_2.prop(s, "vertex_round_y", text="Y", toggle=True)
            row_2.prop(s, "vertex_round_z", text="Z", toggle=True)
            column_1.prop(s, "vertex_round_digits", text="digits")
            column_1.operator("mesh.am1d_vertex_round_position_digits", text="Round Digits")
            column_1.prop(s, "vertex_round_step", text="step")
            column_1.operator("mesh.am1d_vertex_round_position_step", text="Round Step")

        column_1 = maker.get("cbs_do_offset", "OFFset", column)
        if column_1:
            column_1.prop(s, "do_offset_percent", text="value")
            column_1.operator("mesh.am1d_build_offset", text="Build OFFset")
            column_1.operator("mesh.am1d_do_offset", text="Do OFFset")

        column_1 = maker.get("cbs_find_replace", "Find and Replace", column)
        if column_1:
            column_1.prop(s, "find_and_replace_source", text="from")
            column_1.prop(s, "find_and_replace_target", text="to")
            column_1.operator("mesh.am1d_find_and_replace", text="Apply")

        column_1 = maker.get("cbs_contour", "Contour Match ++", column)
        if column_1:
            column_1.operator("mesh.am1d_contour_match", text="Contour Match")
            column_1.operator("mesh.am1d_vertex_march", text="Vertex March").reverse = False
            column_1.operator("mesh.am1d_vertex_march", text="Vertex March reverse").reverse = True
            column_1.operator("mesh.am1d_contour_march", text="Contour March ++").stop = True
            column_1.operator("mesh.am1d_contour_march", text="Contour March +++").stop = False
        
        column_1 = maker.get("cbs_ratio_faces", "Select Ratio Faces", column)
        if column_1:
            column_1.operator("mesh.am1d_select_ratio_faces", text="select")
            column_1.prop(s, "select_ratio_faces_n", text="N")

        column_1 = maker.get("cbs_select_asym", "Select Asym", column)
        if column_1:
            column_1.prop(s, "select_asym_method", text="method")
            column_1.prop(s, "select_asym_axis", text="axis")
            column_1.prop(s, "select_asym_tolerance", text="tolerance")
            column_1.operator("mesh.am1d_select_sym", text="symmetry")
            column_1.operator("mesh.am1d_select_asym", text="asymmetry")

        column.operator("mesh.am1d_mesh_display_on", text="Mesh Display On")
        column.operator("mesh.am1d_corner_edges", text="corner edges")
        column.operator("mesh.am1d_set_plus_name", text="set ++ names")
        column.operator("mesh.am1d_select_between", text="Select Between")
        column.operator("mesh.am1d_get_selected_coords_to_text", text="Get coords from selected")
        column.operator("mesh.am1d_get_coords_from_center", text="Get verts coords from center")
        column.operator("mesh.am1d_zero_scale", text="Zero Scale")
        column.operator("mesh.am1d_index_mats", text="Index Mats")
        column.operator("mesh.am1d_mats_select_border", text="Mats Select Border")
        column.operator("mesh.am1d_apply_all_booleans", text="Apply All Booleans")
        column.operator("mesh.am1d_loop_profile", text="Loop Profile")


def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.am_test_settings = bpy.props.PointerProperty(type=Settings)
    bpy.types.Scene.am_test_collapse_settings = bpy.props.PointerProperty(type=CollapseSettings)


def unregister():
    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()
