import os
import datetime
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import uic


__location__ = os.path.dirname(__file__)
version = 58


# square generator


class Square(object):

    @staticmethod
    def pad_rectangle(rect):
        if rect["dx"] > 2:
            rect["x"] += 1
            rect["dx"] -= 2
        if rect["dy"] > 2:
            rect["y"] += 1
            rect["dy"] -= 2

    @staticmethod
    def layoutrow(sizes, x, y, dx, dy):
        covered_area = sum(sizes)
        width = covered_area / dy
        rects = []
        for size in sizes:
            rects.append({"x": x, "y": y, "dx": width, "dy": size / width})
            y += size / width
        return rects

    @staticmethod
    def layoutcol(sizes, x, y, dx, dy):
        covered_area = sum(sizes)
        height = covered_area / dx
        rects = []
        for size in sizes:
            rects.append({"x": x, "y": y, "dx": size / height, "dy": height})
            x += size / height
        return rects

    @classmethod
    def layout(cls, sizes, x, y, dx, dy):
        return (
            cls.layoutrow(sizes, x, y, dx, dy) if dx >= dy else cls.layoutcol(sizes, x, y, dx, dy)
        )

    @staticmethod
    def leftoverrow(sizes, x, y, dx, dy):
        # compute remaining area when dx >= dy
        covered_area = sum(sizes)
        width = covered_area / dy
        leftover_x = x + width
        leftover_y = y
        leftover_dx = dx - width
        leftover_dy = dy
        return leftover_x, leftover_y, leftover_dx, leftover_dy

    @staticmethod
    def leftovercol(sizes, x, y, dx, dy):
        # compute remaining area when dx >= dy
        covered_area = sum(sizes)
        height = covered_area / dx
        leftover_x = x
        leftover_y = y + height
        leftover_dx = dx
        leftover_dy = dy - height
        return leftover_x, leftover_y, leftover_dx, leftover_dy

    @classmethod
    def leftover(cls, sizes, x, y, dx, dy):
        if dx >= dy:
            return cls.leftoverrow(sizes, x, y, dx, dy)
        else:
            return cls.leftovercol(sizes, x, y, dx, dy)

    @classmethod
    def worst_ratio(cls, sizes, x, y, dx, dy):
        return max(
            [
                max(rect["dx"] / rect["dy"], rect["dy"] / rect["dx"])
                for rect in cls.layout(sizes, x, y, dx, dy)
            ]
        )

    # PUBLIC API

    @classmethod
    def squarify(cls, sizes, x, y, dx, dy):
        sizes = list(map(float, sizes))

        if len(sizes) == 0:
            return []

        if len(sizes) == 1:
            return cls.layout(sizes, x, y, dx, dy)

        # figure out where 'split' should be
        i = 1
        while i < len(sizes) and cls.worst_ratio(sizes[:i], x, y, dx, dy) >= cls.worst_ratio(
                sizes[: (i + 1)], x, y, dx, dy
        ):
            i += 1
        current = sizes[:i]
        remaining = sizes[i:]

        (leftover_x, leftover_y, leftover_dx, leftover_dy) = cls.leftover(current, x, y, dx, dy)
        return cls.layout(current, x, y, dx, dy) + cls.squarify(
            remaining, leftover_x, leftover_y, leftover_dx, leftover_dy
        )

    @classmethod
    def normalize_sizes(cls, sizes, dx, dy):
        total_size = sum(sizes)
        total_area = dx * dy
        sizes = map(float, sizes)
        sizes = map(lambda size: size * total_area / total_size, sizes)
        return list(sizes)

    @classmethod
    def plot(cls, sizes, norm_x=100, norm_y=None):
        normed = cls.normalize_sizes(sizes, norm_x, norm_y or norm_x)
        return cls.squarify(normed, 0, 0, norm_x, norm_y or norm_x)


# lst reader


class LSTReader(object):
    """creates lst tree"""

    @classmethod
    def fromDirectory(cls, directory):
        """build tree from directory"""
        tree = LSTTree()
        tree.root_directory = directory
        os.chdir(directory)
        for directory, folders, files in os.walk("."):
            directory = directory.replace("\\", "/").lstrip("./")
            if directory:
                tree.addFolder(directory, cls.getTime(directory))
            for file in files:
                long_name = os.path.join(directory, file)
                try:
                    tree.addFile(file, os.path.getsize(long_name), cls.getTime(long_name), False)
                except:
                    print("win error: " + long_name)
                    tree.addFile(file, 1, cls.getTime(long_name), True)

        return tree

    @classmethod
    def fromFile(cls, filename, encoding="utf-8", convert_zip=True):
        """build tree from lst file"""
        tree = LSTTree()
        with open(filename, "r", encoding=encoding) as file:
            line = file.readline().rstrip()
            if not line.count("\t"):
                tree.root_directory = line
            file.seek(0, 0)
            while True:
                line = file.readline().rstrip()
                if not line:
                    break

                elif line.count("\t") < 2:
                    continue

                name, size, date_time = line.split("\t", 2)
                if name.endswith("\\"):
                    tree.addFolder(name[:-1].replace("\\", "/"), date_time)
                else:
                    tree.addFile(name, int(size), date_time, name.endswith(".trunc$$"))

        # convert all archives into folders and update structure
        if convert_zip:
            folders = [folder for folder in tree.plain(False) if folder.is_zip]
            folders.sort(key=lambda folder: folder.long_name.count("/"), reverse=True)
            for folder in folders:
                parent = tree.root
                parent.a_num -= folder.a_num
                for directory in folder.long_name.split("/")[:-1]:
                    parent = parent.folders[directory]
                    parent.a_num -= folder.a_num + 1
                parent.r_num += 1
                parent.folders.pop(folder.name)
                parent.files.append(LSTFile(folder.name, parent.long_name, folder.a_size, folder.date))

        return tree

    @staticmethod
    def getTime(path):
        try:
            d = datetime.datetime.fromtimestamp(os.stat(path).st_mtime)
            return "%i.%.2i.%.2i\t%.2i:%.2i.%.2i" % (d.year, d.month, d.day, d.hour, d.minute, d.second)
        except OSError:
            return "1980.1.1\t0.0:00"


class LSTWriter(object):

    @staticmethod
    def write(tree, filename, encoding="utf-8"):
        """write tree into a text file"""
        with open(filename, "w", encoding=encoding) as lst:
            if tree.root_directory:
                lst.write(tree.root_directory.replace("/", "\\") + "\n")
            for folder in tree.plain()[1:]:
                lst.write(folder.text())


class LSTTree(object):

    def __init__(self):
        self.active_folder = self.root = LSTFolder("", "")
        self.truncated = 0
        self._root_directory = ""

    def setRoot(self, root):
        self._root_directory = root.replace("\\", "/").rstrip("/") + "/"

    def getRoot(self):
        return self._root_directory

    def addFolder(self, long_name, date_time):
        """create folder structure until given and mark it active"""
        folder = self.root
        directories = long_name.split("/")
        for i, directory in enumerate(directories, 1):
            if directory not in folder.folders:
                folder.folders[directory] = LSTFolder("/".join(directories[:i]), date_time)
            folder = folder.folders[directory]
        self.active_folder = folder

    def addFile(self, name, size, date_time, is_broken=False):
        """add file into active folder"""
        if is_broken:
            self.truncated += 1
        self.active_folder.files.append(LSTFile(name, self.active_folder.long_name, size, date_time))
        self.active_folder.r_size += size
        self.active_folder.r_num += 1
        self.root.a_size += size
        self.root.a_num += 1

        if self.active_folder.long_name:
            folder = self.root
            for directory in self.active_folder.long_name.split("/"):
                folder = folder.folders[directory]
                folder.a_size += size
                folder.a_num += 1

    def findFolder(self, long_name):
        """find LSTFolder based on long name"""
        if long_name:
            folder = self.root
            for directory in long_name.split("/"):
                folder = folder.folders[directory]
            return folder
        else:
            return self.root

    def plain(self, include_folder=True, include_files=True, from_folder=None):
        """get plain list of folders"""
        folders_left = [from_folder or self.root]
        folders = []
        while folders_left:
            folder = folders_left.pop()
            if include_folder:
                folders.append(folder)
            if include_files:
                folders.extend(folder.files)
            folders_left.extend(folder.folders.values())
        return folders

    root_directory = property(getRoot, setRoot)


class LSTFolder(object):

    r_num = a_num = r_size = a_size = depth = 0

    def __init__(self, long_name, date_time):
        self.long_name = long_name
        self.name = os.path.split(long_name)[1]
        self.folders = {}
        self.files = []
        self.is_zip = os.path.splitext(long_name)[1].lower() in (".zip", ".rar", ".7z", ".tar")
        self.date = date_time
        if long_name:
            self.depth = self.long_name.count("/") + 1

    def chunk(self, num):
        """obtain chunk of data for table. 0 - report, 1 - explorer"""
        depth = self.depth if num == 0 else "/"
        return [depth, self.name, self.long_name, self.r_size, self.a_size, self.r_num, self.a_num, self.date]

    def text(self):
        """return text for writing into lst file"""
        return "%s\\\t%i\t%s\n" % (self.long_name.replace("/", "\\"), self.a_size, self.date)


class LSTFile(object):

    r_num = a_num = 1
    depth = 0

    def __init__(self, name, folder_name, size, date_time):
        self.name = name
        self.long_name = "%s/%s" % (folder_name, name) if folder_name else name
        self.depth = self.long_name.count("/")
        self.r_size = self.a_size = size
        self.date = date_time

    def chunk(self, num):
        depth = self.depth if num == 0 else os.path.splitext(self.name)[1]
        return [depth, self.name, self.long_name, self.r_size, self.a_size, 1, 1, self.date]

    def text(self):
        return "%s\t%i\t%s\n" % (self.name, self.r_size, self.date)


class LSTAbstractFolder(object):

    r_num = a_num = r_size = a_size = depth = 0

    def __init__(self, long_name):
        self.long_name = long_name
        self.name = os.path.split(long_name)[1]
        if long_name:
            self.depth = self.long_name.count("/") + 1

    def chunk(self):
        """obtain chunk of data for table. 0 - report, 1 - explorer"""
        return [self.depth, self.name, self.long_name, self.r_size, self.a_size, self.r_num, self.a_num, "", 3]

    def add(self, size):
        self.r_size += size
        self.a_size += size
        self.a_num += 1
        self.r_num += 1

# table custom draw


class ReportDelegate(QItemDelegate):

    def __init__(self):
        QItemDelegate.__init__(self)
        self.limits = {0: 0, 3: 0, 4: 0, 5: 0, 6: 0}
        self.round = 3

    def setRound(self, round_value):
        self.round = round_value

    def paint(self, painter, option, index):

        data = index.data()
        num = index.column()
        if data is None:
            return

        try:
            value = data / self.limits[num]
        except ZeroDivisionError:
            value = 0
        except KeyError:
            QItemDelegate.paint(self, painter, option, index)
            return

        if option.state & QStyle.State_Selected:
            painter.setBrush(option.palette.highlight())
            painter.drawRect(option.rect)

        color = QColor(173, 216, 230)
        painter.setBrush(QBrush(color))
        painter.setPen(QPen(color))

        rect = option.rect
        rect = QRect(rect.x(), rect.y(), int(rect.width() * value), rect.height() - 1)
        painter.drawRect(rect)

        font = QFont()
        font.setPointSize(9)
        painter.setPen(Qt.black)
        painter.setFont(font)
        if num in (3, 4):
            if data > 2 ** 30:
                text = "%s G" % round(data / 2 ** 30, self.round)
            elif data > 2 ** 20:
                text = "%s M" % round(data / 2 ** 20, 1)
            elif data > 2 ** 10:
                text = "%s K" % round(data / 2 ** 10)
            else:
                text = "%s B" % data
        elif num in (5, 6) or data:
            text = str(data)
        else:
            text = "root"
        painter.drawText(option.rect, Qt.AlignCenter, text)


class ExplorerDelegate(ReportDelegate):

    def __init__(self):
        ReportDelegate.__init__(self)
        self.file_icon = QImage(__location__ + "/resources/icons/file.png")
        self.folder_icon = QImage(__location__ + "/resources/icons/folder.png")

    def paint(self, painter, option, index):
        data = index.data()

        if data is None:
            return
        if index.column():
            ReportDelegate.paint(self, painter, option, index)
        else:   # folder icon
            if option.state & QStyle.State_Selected:
                painter.setBrush(option.palette.highlight())
                painter.drawRect(option.rect)
            rect = option.rect
            rect = QRect(rect.x(), rect.y(), 20, 20)
            if data == "/":
                painter.drawImage(rect, self.folder_icon)
            else:
                painter.drawImage(rect, self.file_icon)
                rect = option.rect
                rect = QRect(rect.x() + 25, rect.y(), rect.width() - 25, 20)
                painter.drawText(rect, Qt.AlignLeft, data)


# map item custom draw


class Color(object):
    """scalable color class"""

    def __init__(self, *rgb):
        self.color = rgb[:3], rgb[3:]

    def get(self, scale=1.0):
        return QColor(*[int(c2 * (1.0 - scale) + c1 * scale) for c1, c2 in zip(*self.color)])


class MapItem(QGraphicsItem):

    folder_color = Color(254, 215, 0, 100, 100, 100)
    file_color = Color(0, 191, 254, 100, 100, 100)
    file_deep_color = Color(60, 100, 240, 215, 230, 250)
    other_color = Color(255, 0, 255, 0, 0, 0)
    active_color = Color(255, 69, 0, 0, 0, 0)

    def __init__(self, block, shape, scale, scale_max):
        QGraphicsItem.__init__(self)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.rect = QRect(*shape)
        self.object_type = block[-1]
        r_s = self.convertSize(block[3])
        a_s = self.convertSize(block[4])

        if scale_max:
            scale /= scale_max
        else:
            scale = 0

        if block[-1] == 0:
            color = self.file_color.get(scale)
            self.name = "   %s \n   AS: %s" % (block[1], a_s)

        elif block[-1] == 1:
            color = self.folder_color.get(scale)
            self.name = "   %s \n   AS: %s\n   AF: %s\n   RS: %s\n   RF: %s" % (block[1], a_s, block[6], r_s, block[5])

        elif block[-1] == 2:
            color = self.file_deep_color.get(scale)
            self.name = "   %s \n   AS: %s" % (block[1], a_s)

        else:
            color = self.other_color.get()
            self.name = "   %s \n   RS: %s\n   AS: %s\n   RF: %s" % (block[1], r_s, a_s, block[5])

        self.long_name = block[2]
        self.brush = QBrush()
        self.brush.setStyle(Qt.SolidPattern)
        self.brush.setColor(color)

        self.selected_brush = QBrush()
        self.selected_brush.setStyle(Qt.SolidPattern)
        self.selected_brush.setColor(self.active_color.get())

        self.pen = QPen()
        self.pen.setStyle(Qt.SolidLine)
        self.pen.setWidth(1)
        self.pen.setColor(QColor(20, 20, 20, 255))

    def convertSize(self, size):
        if size > 2 ** 30:
            return "%s Gb" % round(size / 2 ** 30, 1)
        elif size > 2 ** 20:
            return "%s Mb" % round(size / 2 ** 20, 1)
        elif size > 2 ** 10:
            return "%s Kb" % round(size / 2 ** 10, 1)
        else:
            return "%s B" % size

    def paint(self, painter, option, widget):
        painter.setPen(self.pen)
        if self.isSelected():
            painter.setBrush(self.selected_brush)
        else:
            painter.setBrush(self.brush)
        painter.drawRect(self.rect)
        painter.drawText(self.rect, Qt.AlignVCenter, self.name)

    def boundingRect(self):
        return QRectF(self.rect)


# UI


class TableBase(QWidget):

    UI_FILE = ""
    PREF_FILE = ""
    LABELS = ()
    WIDTH = 60, 350, 350, 75, 75, 60, 60, 150
    FILTER_TABLE = (4, 2 ** 30), (4, 2 ** 20), (6, 1), (4, 0), (3, 2 ** 30), (3, 2 ** 20), (5, 1)
    DELEGATE = QItemDelegate
    CHUNK_ID = 0

    def __init__(self):
        QWidget.__init__(self)
        self.ui = uic.loadUi(self.UI_FILE, self)

        # load options widget
        self.options = uic.loadUi(self.PREF_FILE)
        self.options.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowStaysOnTopHint)
        self.options.setWindowIcon(QIcon(__location__ + "/resources/icons/prefs.png"))

        # load data
        self.widgets = []
        self.tree = LSTTree()

        # table
        self.delegate = self.DELEGATE()
        self.ui.table_widget.setItemDelegate(self.delegate)
        self.ui.table_widget.keyPressEvent = self.tableKeyPressEvent

        # tree map
        self.ui.scene = QGraphicsScene()
        self.ui.scene.wheelEvent = self.sceneWheelEvent
        self.ui.scene.mouseDoubleClickEvent = self.sceneDoubleClickEvent
        self.ui.scene.keyPressEvent = self.sceneKeyPressEvent
        self.ui.map_widget.setScene(self.scene)
        self.brush = QBrush()
        self.pen = QPen()
        self.ui.map_widget.hide()

        # first load
        self.drawList()
        for i, width in enumerate(self.WIDTH):
            self.ui.table_widget.setColumnWidth(i, width)

        # signals
        for i in range(len(self.LABELS)):
            self.options.__getattribute__("chb_%i" % i).toggled.connect(self.toggleBar(i))
        self.options.spin_round.valueChanged.connect(self.delegate.setRound)
        self.options.spin_round.valueChanged.connect(self.ui.table_widget.reset)
        self.ui.btn_update.released.connect(self.filter)
        self.ui.chb_table.clicked.connect(self.ui.table_widget.setVisible)
        self.ui.chb_map.clicked.connect(self.toggleMap)
        self.ui.btn_options.released.connect(self.options.show)
        self.ui.cmb_filter.currentIndexChanged.connect(self.checkIndex)

        # context menus
        self.menu_table = QMenu()
        self.ui.table_widget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui.table_widget.customContextMenuRequested.connect(self.openMenuTable)

        self.action_copy.triggered.connect(self.copySelectedCell)
        self.menu_table.addAction(self.action_copy)
        self.ui.table_widget.addAction(self.action_copy)

        self.action_copy_full.triggered.connect(self.copyFullPath)
        self.menu_table.addAction(self.action_copy_full)
        self.ui.table_widget.addAction(self.action_copy_full)

        # check box ui
        self.ui.chb_table.paintEvent = self.checkboxPaintEvent(self.ui.chb_table)
        self.ui.chb_map.paintEvent = self.checkboxPaintEvent(self.ui.chb_map)

    def lastChunk(self, block):
        """append last block chunk"""
        raise NotImplementedError

    def updateTable(self):
        raise NotImplementedError

    def filter(self):
        """set rows visibility based on filter value"""
        filter_type, size = self.FILTER_TABLE[self.ui.cmb_filter.currentIndex()]
        size *= self.ui.spin_filter.value()
        table = self.ui.table_widget

        file_num = 0
        file_size = 0
        try:
            text = self.ui.line_filter.text().lower()
        except AttributeError:
            text = ""

        for i in range(table.rowCount()):
            item = table.item(i, filter_type)
            name = table.item(i, 2).text()
            if item.data(Qt.DisplayRole) >= size and text in name.lower():
                file_num += 1
                file_size += table.item(i, 3).data(Qt.DisplayRole)
                table.showRow(i)
            else:
                table.hideRow(i)
        self.ui.label_info.setText("%i/%i   |   %.3f GB" % (file_num, table.rowCount(), file_size / 2 ** 30))
        self.drawTree()

    def drawList(self, lst_list=(), limits=None):
        """draw list of LST objects"""
        table = self.ui.table_widget
        table.setUpdatesEnabled(False)
        table.setSortingEnabled(False)
        table.clear()
        table.setRowCount(len(lst_list))
        table.setColumnCount(len(self.LABELS))
        table.setHorizontalHeaderLabels(self.LABELS)
        for i, lst_unit in enumerate(lst_list):
            for j, lst_field in enumerate(lst_unit.chunk(self.CHUNK_ID)):
                item = QTableWidgetItem()
                item.setData(Qt.DisplayRole, lst_field)
                item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                table.setItem(i, j, item)
        for i in range(len(self.LABELS)):
            self.toggleBar(i)(self.options.__getattribute__("chb_%i" % i).isChecked())
        if limits:
            self.delegate.limits = limits
        self.filter()
        table.setSortingEnabled(True)
        table.setUpdatesEnabled(True)

    def drawTree(self):
        raise NotImplementedError

    def load(self, tree):
        raise NotImplementedError

    def generateTreeData(self):
        """read data from table. represent all depth in linear"""
        table = self.ui.table_widget
        p = self.options.cmb_map_show.currentIndex() + 3
        data = []
        for i in range(table.rowCount()):
            if table.isRowHidden(i):
                continue
            block = [table.item(i, j).data(Qt.DisplayRole) for j in range(7)]
            self.lastChunk(block)
            if block[p]:
                data.append(block)
        return data

    def filterTreeData(self, data):
        """filter given array"""
        p = self.options.cmb_map_show.currentIndex() + 3
        data.sort(key=lambda b: b[p], reverse=True)
        limit = self.options.spin_limit.value()
        if len(data) > limit:
            rel_size = a_size = rel_file = a_file = 0
            extra_data = data[limit:]
            data = data[:limit]
            for data_block in extra_data:
                rel_size += data_block[3]
                a_size += data_block[4]
                rel_file += data_block[5]
                a_file += data_block[6]
            data.append([0, "", "", rel_size, a_size, rel_file, a_file, 3])
            data.sort(key=lambda b: b[p], reverse=True)
        return data

    def selectRowsByNames(self, selected):
        """select all row with names"""
        if not selected:
            return
        table = self.ui.table_widget
        table.clearSelection()
        table.setSelectionMode(QAbstractItemView.MultiSelection)
        for i in range(table.rowCount()):
            if table.item(i, 2).text() in selected:
                table.selectRow(i)
        table.setSelectionMode(QAbstractItemView.SingleSelection)

    def findLimits(self, data):
        """find limits of given array"""
        return {
            0: max(data, key=lambda x: x.depth).depth,
            3: max(data, key=lambda x: x.r_size).r_size,
            4: max(data, key=lambda x: x.a_size).a_size,
            5: max(data, key=lambda x: x.r_num).r_num,
            6: max(data, key=lambda x: x.a_num).a_num
        }

    def copyToClipboard(self, text):
        """copy given text"""
        cb = app.clipboard()
        cb.clear(mode=cb.Clipboard)
        cb.setText(text, mode=cb.Clipboard)

    def copyFullPath(self):
        table = self.ui.table_widget
        if table.currentRow() != -1:
            self.copyToClipboard(self.tree.root_directory + table.item(table.currentRow(), 2).text())

    def copySelectedCell(self):
        table = self.ui.table_widget
        if table.currentRow() != -1 or table.currentColumn() != -1:
            self.copyToClipboard(table.item(table.currentRow(), table.currentColumn()).text())

    # events

    def sceneWheelEvent(self, event):
        factor = 1.41 ** ((event.delta() * .5) / 240.0)
        self.ui.map_widget.scale(factor, factor)

    def checkboxPaintEvent(self, check_box):

        def paint(event):
            painter = QPainter()
            painter.begin(check_box)
            if check_box.isChecked():
                painter.setPen(QColor(173, 216, 230))
                painter.setBrush(QColor(173, 216, 230))
                painter.drawRect(0, 0, check_box.width(), check_box.height())
            painter.drawPixmap(2, 2, 20, 20, check_box.icon().pixmap(20, 20))
            painter.end()

        return paint

    def openMenuTable(self, position):
        self.menu_table.exec(self.ui.table_widget.mapToGlobal(position))

    # toggles

    def toggleBar(self, index):
        """toggle column visibility"""
        def toggle(value):
            if value:
                self.ui.table_widget.showColumn(index)
            else:
                self.ui.table_widget.hideColumn(index)
        return toggle

    def checkIndex(self, index):
        if index != 3:
            return
        else:
            self.ui.cmb_filter.setCurrentIndex(0)

    def toggleMap(self, value):
        if value:
            self.ui.map_widget.show()
            self.drawTree()
        else:
            self.ui.map_widget.hide()


class Report(TableBase):

    LABELS = "depth", "name", "path", "R size", "A size", "R files", "A files", "date"
    UI_FILE = __location__ + "/resources/ui/table_report.ui"
    PREF_FILE = __location__ + "/resources/ui/options_report.ui"
    DELEGATE = ReportDelegate
    CHUNK_ID = 0
    reveal_in_explorer = pyqtSignal(str)
    reveal_file_in_explorer = pyqtSignal(str)

    def __init__(self):
        TableBase.__init__(self)
        self.options.setFixedSize(331, 581)
        self.folders = []
        self.files = []

        # signals
        self.ui.chb_root.clicked.connect(self.updateTable)
        self.ui.table_widget.doubleClicked.connect(self.revealInExplorer)
        self.ui.btn_explorer.released.connect(self.revealInExplorer)
        self.options.rad_folders.clicked.connect(self.updateTable)
        self.options.rad_files.clicked.connect(self.updateTable)
        self.options.line_root.editingFinished.connect(self.changeRoot)

        # context
        self.action_explorer.triggered.connect(self.revealInExplorer)
        self.menu_table.addAction(self.action_explorer)
        self.ui.table_widget.addAction(self.action_explorer)
        self.ui.chb_root.paintEvent = self.checkboxPaintEvent(self.ui.chb_root)

    def load(self, tree):
        """load new lst file"""
        self.tree = tree
        self.folders = tree.plain(True, False)
        self.files = tree.plain(False, True)
        if self.options.text_report.toPlainText():
            self.options.text_report.append("---")
        self.options.line_root.setText(tree.root_directory)
        self.options.text_report.append("root: %s" % tree.root_directory)
        self.options.text_report.append("size: %s GB" % round(self.folders[0].a_size / 2 ** 30, 1))
        self.options.text_report.append("folders: %i" % len(self.folders))
        self.options.text_report.append("files: %i" % len(self.files))
        self.options.text_report.append("truncated: %i" % tree.truncated)
        self.updateTable()

    def updateTable(self):
        """build table to draw"""
        if self.options.rad_folders.isChecked():
            data = self.folders if self.ui.chb_root.isChecked() else self.folders[1:]
        else:
            data = self.files
        if not data:
            return
        self.drawList(data, self.findLimits(data))

    def lastChunk(self, block):
        block.append(self.options.rad_folders.isChecked())

    def drawTree(self):
        """draw map tree. executed right after filter and doesn't draw if widget is hidden"""
        if not self.ui.map_widget.isVisible():
            return
        self.ui.scene.clear()

        data = self.filterTreeData(self.generateTreeData())
        if not data:
            return
        p = self.options.cmb_map_show.currentIndex() + 3
        shapes = Square.plot([v[p] for v in data], self.options.spin_size.value())
        max_depth = max(data, key=lambda b: b[0])[0]
        for block, shape in zip(data, shapes):
            shape = [round(shape[letter]) for letter in ("x", "y", "dx", "dy")]
            self.scene.addItem(MapItem(block, shape, block[0], max_depth))

    def revealInExplorer(self):
        """open selected folder in explorer"""
        table = self.ui.table_widget
        if table.currentRow() != -1:
            long_name = table.item(table.currentRow(), 2).text()
            if self.options.rad_folders.isChecked():
                self.reveal_in_explorer.emit(long_name)
            else:
                self.reveal_file_in_explorer.emit(long_name)

    def changeRoot(self):
        self.tree.root_directory = self.options.line_root.text()
        self.options.line_root.setText(self.tree.root_directory)

    # navigation events

    def sceneDoubleClickEvent(self, *args):
        self.selectRowsByNames({item.long_name for item in self.ui.scene.selectedItems()})
        self.revealInExplorer()

    def sceneKeyPressEvent(self, event):
        if event.key() == Qt.Key_Return:
            self.sceneDoubleClickEvent(self)
        else:
            QGraphicsScene.keyPressEvent(self.ui.scene, event)

    def tableKeyPressEvent(self, event):
        if event.key() == Qt.Key_Return:
            self.revealInExplorer()
        elif event.key() in (Qt.Key_X, 1063) and event.modifiers() == Qt.ControlModifier:
            self.copyFullPath()
        else:
            QTableWidget.keyPressEvent(self.ui.table_widget, event)


class Explorer(TableBase):

    LABELS = "type", "name", "path", "R size", "A size", "R files", "A files", "date"
    UI_FILE = __location__ + "/resources/ui/table_explorer.ui"
    PREF_FILE = __location__ + "/resources/ui/options_explorer.ui"
    DELEGATE = ExplorerDelegate
    CHUNK_ID = 1

    def __init__(self):
        TableBase.__init__(self)
        self.options.setFixedSize(331, 371)
        self.global_limits = {0: 0, 3: 0, 4: 0, 5: 0, 6: 0}

        # signals
        self.ui.btn_deep.released.connect(self.drawTreeDeep)
        self.ui.btn_backward.released.connect(self.backward)
        self.ui.btn_forward.released.connect(self.forward)
        self.ui.table_widget.doubleClicked.connect(self.forward)
        self.ui.chb_local.toggled.connect(self.updateTable)
        self.ui.chb_update.clicked.connect(self.drawTree)

        # context
        self.action_forward.triggered.connect(self.forward)
        self.menu_table.addAction(self.action_forward)
        self.ui.table_widget.addAction(self.action_forward)

        self.action_backward.triggered.connect(self.backward)
        self.menu_table.addAction(self.action_backward)
        self.ui.table_widget.addAction(self.action_backward)

        self.ui.chb_local.paintEvent = self.checkboxPaintEvent(self.ui.chb_local)

    def load(self, tree):
        """load new lst file"""
        self.tree = tree
        self.global_limits = self.findLimits(self.tree.plain()[1:])
        self.revealInExplorer("")

    def lastChunk(self, block):
        block.append(block[0] == "/")

    def drawTree(self):
        """draw map tree. executed right after filter and doesn't draw if widget is hidden"""
        if not self.ui.chb_update.isChecked() or not self.ui.map_widget.isVisible():
            return

        self.ui.scene.clear()
        p = self.options.cmb_map_show.currentIndex() + 3
        data = self.generateTreeData()
        folders = self.filterTreeData([block for block in data if block[-1] == 1])
        files = self.filterTreeData([block for block in data if block[-1] == 0])
        data = folders + files
        if not data:
            return
        p = self.options.cmb_map_show.currentIndex() + 3
        shapes = Square.plot([v[p] for v in data], self.options.spin_size.value())
        max_size = max(data, key=lambda b: b[p])[p]
        for block, shape in zip(data, shapes):
            shape = [round(shape[letter]) for letter in ("x", "y", "dx", "dy")]
            self.scene.addItem(MapItem(block, shape, block[p], max_size))

    def drawTreeDeep(self):
        """draw tree map showing all depth. one time activate"""
        if not self.ui.map_widget.isVisible():
            return

        self.ui.scene.clear()
        try:
            folder = self.tree.findFolder(self.ui.line_folder.text())
        except KeyError:
            return
        self.ui.chb_update.setChecked(False)

        # find files to draw
        files_ok = self.tree.plain(False, True, folder)
        if self.options.rad_items.isChecked():
            files_ok = sorted(files_ok, key=lambda f: f.a_size, reverse=True)[:self.options.spin_limit.value()]
        else:
            cut_size = self.options.spin_limit_size.value() * 2 ** 20
            files_ok = [file for file in files_ok if file.a_size >= cut_size]
        if not files_ok:
            return
        max_depth = max(files_ok, key=lambda x: x.depth).depth
        files_ok = {file.long_name for file in files_ok}

        # draw
        folders_draw = [(folder, (0, 0, self.options.spin_size.value(), self.options.spin_size.value()))]
        while folders_draw:
            folder, geometry = folders_draw.pop()
            nodes = [node for node in folder.folders.values() if node.a_size]

            # add abstract folder for small files
            abstract_folder = LSTAbstractFolder(folder.long_name)
            for node in folder.files:
                if not node.a_size:
                    continue
                if node.long_name in files_ok:
                    nodes.append(node)
                else:
                    abstract_folder.add(node.a_size)
            if abstract_folder.a_size:
                nodes.append(abstract_folder)

            # sort and create shapes
            nodes.sort(key=lambda n: n.a_size)
            shapes = Square.plot([n.a_size for n in nodes], geometry[2], geometry[3])
            include_small = self.options.chb_deep_small.isChecked()

            # add folder to queue, draw file
            for node, shape in zip(nodes, shapes):
                shape = geometry[0] + shape["x"], geometry[1] + shape["y"], shape["dx"], shape["dy"]
                if isinstance(node, LSTFolder):
                    folders_draw.append((node, shape))
                elif isinstance(node, LSTFile):
                    block = node.chunk(0)
                    block.append(2)
                    self.scene.addItem(MapItem(block, shape, block[0], max_depth))
                elif include_small:
                    self.scene.addItem(MapItem(node.chunk(), shape, folder.depth + 1, max_depth))

    def revealInExplorer(self, long_name):
        """open folder in explorer"""
        try:
            folder = self.tree.findFolder(long_name)
        except KeyError:
            return
        self.ui.line_folder.setText(folder.long_name)
        lst_list = list(folder.folders.values()) + folder.files
        if lst_list:
            limits = self.findLimits(lst_list) if self.ui.chb_local.isChecked() else self.global_limits
            self.drawList(lst_list, limits)
        else:
            self.drawList()

    def revealFileInExplorer(self, long_name):
        """open file in explorer"""
        self.revealInExplorer(os.path.dirname(long_name))
        self.selectRowsByNames({long_name})

    def forward(self):
        """open folder next in hierarchy"""
        table = self.ui.table_widget
        if table.currentRow() != -1:
            self.revealInExplorer(table.item(table.currentRow(), 2).text())

    def backward(self):
        """open folder previous in hierarchy"""
        long_name = self.ui.line_folder.text()
        if "/" not in long_name:
            self.revealInExplorer("")
        else:
            self.revealInExplorer(long_name.rsplit("/", 1)[0])

    def updateTable(self):
        """build table again"""
        self.revealInExplorer(self.ui.line_folder.text())

    # navigation events

    def sceneDoubleClickEvent(self, *args):
        selected = self.ui.scene.selectedItems()
        if not selected:
            return
        node = selected[0]
        if node.object_type in (0, 2):
            self.revealFileInExplorer(node.long_name)
        else:
            self.revealInExplorer(node.long_name)

    def sceneKeyPressEvent(self, event):
        if event.key() == Qt.Key_Return:
            self.sceneDoubleClickEvent(self)
        elif event.key() == Qt.Key_Backspace:
            self.backward()
        else:
            QGraphicsScene.keyPressEvent(self.ui.scene, event)

    def tableKeyPressEvent(self, event):
        if event.key() == Qt.Key_Return:
            self.forward()
        elif event.key() == Qt.Key_Backspace:
            self.backward()
        elif event.key() in (Qt.Key_X, 1063) and event.modifiers() == Qt.ControlModifier:
            self.copyFullPath()
        else:
            QTableWidget.keyPressEvent(self.ui.table_widget, event)


class MainWindow(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)
        self.help = uic.loadUi(__location__ + "/resources/ui/help.ui")
        self.help.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowStaysOnTopHint)
        self.help.setWindowIcon(QIcon(__location__ + "/resources/icons/info.png"))
        self.help.setFixedSize(331, 371)

        self.ui = uic.loadUi(__location__ + "/resources/ui/MainUI.ui")
        self.setWindowIcon(QIcon(__location__ + "/resources/icons/icon.png"))
        self.setWindowTitle("LST reader %i" % version)
        self.setCentralWidget(self.ui)
        self.setAcceptDrops(True)
        self.tree = LSTTree()
        self.table_report = Report()
        self.table_explorer = Explorer()

        # signals
        self.ui.split_tab.addWidget(self.table_report)
        self.ui.split_tab.addWidget(self.table_explorer)
        self.ui.btn_dir._menu = QMenu()
        self.ui.btn_dir._menu.addAction("directory", self.inputDir)
        self.ui.btn_dir._menu.addAction("LST file", self.inputFile)
        self.ui.btn_dir.setMenu(self.ui.btn_dir._menu)
        self.ui.btn_save.released.connect(self.save)
        self.ui.btn_load.released.connect(self.load)
        self.ui.chb_report.clicked.connect(self.table_report.setVisible)
        self.ui.chb_explorer.clicked.connect(self.table_explorer.setVisible)
        self.table_report.reveal_in_explorer.connect(self.table_explorer.revealInExplorer)
        self.table_report.reveal_file_in_explorer.connect(self.table_explorer.revealFileInExplorer)
        self.ui.btn_help.released.connect(self.help.show)
        self.ui.btn_color.released.connect(self.load_temp)

        # context
        self.ui.action_open.triggered.connect(self.inputFile)
        self.ui.addAction(self.ui.action_open)

        self.ui.action_new.triggered.connect(self.inputDir)
        self.ui.addAction(self.ui.action_new)

        self.ui.action_load.triggered.connect(self.load)
        self.ui.addAction(self.ui.action_load)

        self.ui.action_save.triggered.connect(self.save)
        self.ui.addAction(self.ui.action_save)

        # finish
        self.load_temp()
        self.show()

    def load(self):
        file_name = self.ui.line_in.text()
        try:
            if os.path.isfile(file_name):
                self.tree = LSTReader.fromFile(file_name, self.ui.cmb_encode.currentText(), self.ui.chb_zip.isChecked())
            elif os.path.isdir(file_name):
                self.tree = LSTReader.fromDirectory(file_name)
            else:
                raise Exception("input is neither file nor directory")
            self.table_explorer.load(self.tree)
            self.table_report.load(self.tree)

        except Exception as e:
            print("couldn't load:")
            print(e)

    def save(self):
        """save tree into a file"""
        try:
            file_name = os.path.splitext(self.ui.line_in.text())[0] + ".lst"
            file = QFileDialog().getSaveFileName(self, caption="Save LST", directory=file_name)[0]
            if not file:
                return
            file = os.path.splitext(file)[0] + ".lst"
            LSTWriter.write(self.tree, file, "utf-8")
        except Exception as e:
            print("couldn't save:")
            print(e)

    def inputDir(self):
        """select directory"""
        self.ui.line_in.setText(QFileDialog().getExistingDirectory(self, caption="Select Root"))

    def inputFile(self):
        """select lst file"""
        self.ui.line_in.setText(QFileDialog().getOpenFileName(self, caption="Select LST", filter="LST file (*.lst)")[0])

    def dragEnterEvent(self, event):
        event.accept()

    def dropEvent(self, event):
        text = event.mimeData().text()
        if "file:///" in text:
            self.ui.line_in.setText(text.replace("file:///", ""))

    def load_temp(self):
        """temp function"""
        try:
            color_dict = eval(open(__location__ + "/resources/colors.txt", "r").read())
            MapItem.folder_color = Color(*color_dict["folder"])
            MapItem.file_color = Color(*color_dict["file"])
            MapItem.other_color = Color(*color_dict["other"])
            MapItem.active_color = Color(*color_dict["active"])
            MapItem.file_deep_color = Color(*color_dict["file_deep"])
            self.table_report.drawTree()
            self.table_explorer.drawTree()
        except Exception as e:
            print(e)


if __name__ == "__main__":
    import sys
    app = QApplication([])
    a = MainWindow()
    sys.exit(app.exec())
