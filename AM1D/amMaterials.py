import os
import collections
import bpy
import bmesh
import mathutils
from .amTools import *


class RasterList(object):

    headers = "MAT", "TEXTURE/NODE", "RASTER", "SIZE", "STATUS"
    sort_by = BlenderParam("raster_scan_index")

    def __init__(self):
        self.total_size = self.total_miss =0
        self.images = {image.name for image in bpy.data.images}
        self.blocks = []
        self.length = [0] * 5

    def nextImage(self, image, material_name=""):
        """proceed next image"""

        if not image:
            status = "-----EMPTY"
            path = ""
            image_name = ""
            size = 0
        else:
            path = bpy.path.abspath(image.filepath)
            image_name = image.name
            if image.packed_file and os.path.isfile(path):
                status = "ok both"
                size = image.packed_file.size
            elif image.packed_file:
                status = "ok packed"
                size = image.packed_file.size
            elif os.path.isfile(path):
                status = "ok hdd"
                size = os.path.getsize(path)
            else:
                status = "-----LOST"
                size = 0

        if self.images:
            self.images.discard(image_name)

        result = [
            material_name,
            image_name,
            os.path.split(path)[1],
            "%.2f" % (size / 2 ** 20),
            status]

        self.total_size += size
        if status == "-----LOST":
            self.total_miss += 1

        self.blocks.append(result)

    def build(self):
        number = int(self.sort_by())
        if number == 3:
            self.blocks.sort(key=lambda v: float(v[3]), reverse=True)
        else:
            self.blocks.sort(key=lambda v: v[number])
        self.blocks.insert(0, [self.count(0), self.count(1), self.count(2), "=%iMb" % (self.total_size / 2 ** 20),
                               "=%i" % self.total_miss])
        self.blocks.insert(0, self.headers)
        for line in self.blocks:
            for i in range(5):
                self.length[i] = max(self.length[i], len(line[i]))

    def count(self, index=0):
        unique = set()
        for block in self.blocks:
            unique.add(block[index])
        unique.discard("")
        return str("=%i" % len(unique))


class RasterScan(bpy.types.Operator):
    bl_idname = "mesh.am1d_raster_scan"
    bl_label = "Raster Scan"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "Scan scene raster images/nodes/materials structure."


    @staticmethod
    def count(array, index=0):
        unique = set()
        for block in array:
            unique.add(block[index])
        unique.discard("")
        return str("=%i" % len(unique))

    def execute(self, context):

        scan = RasterList()
        for material in bpy.data.materials:
            if not material.node_tree:
                continue
            for image in material.node_tree.nodes:
                if image.type == "TEX_IMAGE":
                    scan.nextImage(image.image, material.name)

        for image_name in tuple(scan.images):
            scan.nextImage(bpy.data.images[image_name])
        scan.build()

        if "RASTERSCAN.txt" in bpy.data.texts:
            text_block = bpy.data.texts["RASTERSCAN.txt"]
        else:
            text_block = bpy.data.texts.new(name="RASTERSCAN.txt")
        text_block.clear()

        for line in scan.blocks:
            line = [line[i] + " " * (scan.length[i] - len(line[i])) for i in range(5)]
            text_block.write("  |  ".join(line) + "\n")
        return {"FINISHED"}


class MatsEqualizeBetween(bpy.types.Operator):

    bl_idname = "mesh.am1d_mats_equalize_between"
    bl_label = "Mats Equalize Between"
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def addMaterials(objs, change_active=True):
        obj_act = bpy.context.scene.objects.active

        mat_info = collections.namedtuple('mat_info', 'mat slot index')
        need_mats = []

        # 1) Берём полный список материалов активного объекта
        i_base = 0
        slots = obj_act.material_slots
        for i, slot in enumerate(slots):
            mat = slot.material
            cell = mat_info(mat, slot, i_base)
            need_mats.append(cell)
            i_base += 1

        # 2) Дополняем его нехватающими материалами пассивных объектов
        if change_active:
            for o in objs:
                if o == obj_act:
                    continue

                slots = bpy.data.objects[o.name].material_slots
                for i, slot in enumerate(slots):
                    mat = slot.material
                    mats_ = [m.mat for m in need_mats]
                    if mat not in mats_:
                        bpy.ops.object.material_slot_add()
                        new_slot = bpy.data.objects[obj_act.name].material_slots[-1]
                        new_slot.material = bpy.data.materials[mat.name]
                        cell = mat_info(mat, new_slot, i_base)
                        need_mats.append(cell)
                        i_base += 1

        # 3) Назначаем этот список всем выделенным объектам таким образом,
        #    чтобы внешне они выглядели так же

        slots = bpy.data.objects[obj_act.name].material_slots
        for o in objs:
            if o == obj_act:
                continue

            bpy.context.scene.objects.active = o
            mats_ = [s.material.name for s in bpy.data.objects[o.name].material_slots]
            for i, slot in enumerate(slots):
                mat = slot.material.name
                if mat not in mats_:
                    bpy.ops.object.material_slot_add()
                    new_slot = bpy.data.objects[o.name].material_slots[-1]
                    new_slot.material = bpy.data.materials[mat]

        for o in objs:
            bpy.context.scene.objects.active = o
            len_need_mats = len(need_mats)
            for j in range(len_need_mats):
                cell = need_mats[j]
                slot_name = cell.mat.name
                slots = [s.name for s in bpy.data.objects[o.name].material_slots]
                for i, slot_ in enumerate(slots):
                    if slot_ == slot_name:
                        idx_move = cell.index - i
                        bpy.context.object.active_material_index = i
                        if idx_move > 0:
                            for j in range(idx_move):
                                bpy.ops.object.material_slot_move(direction="DOWN")
                        elif idx_move < 0:
                            for j in range(-idx_move):
                                bpy.ops.object.material_slot_move(direction="UP")

        bpy.context.scene.objects.active = obj_act

    def execute(self, context):
        self.addMaterials(context.selected_objects)
        return {"FINISHED"}


class MatsEqualizeAdd(bpy.types.Operator):
    bl_idname = "mesh.am1d_mats_equalize_add"
    bl_label = "Mats Equalize Add"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for mesh in context.selected_objects:
            if mesh != context.scene.objects.active:
                MatsEqualizeBetween.addMaterials([mesh], False)
        return {"FINISHED"}


class MatsColorize(bpy.types.Operator):
    bl_idname = "mesh.am1d_mats_colorize"
    bl_label = "Mats Colorize"
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def rgb_to_hsv(r, g, b):
        maxc = max(r, g, b)
        minc = min(r, g, b)
        v = maxc
        if minc == maxc:
            return 0.0, 0.0, v
        s = (maxc - minc) / maxc
        rc = (maxc - r) / (maxc - minc)
        gc = (maxc - g) / (maxc - minc)
        bc = (maxc - b) / (maxc - minc)
        if r == maxc:
            h = bc - gc
        elif g == maxc:
            h = 2.0 + rc - bc
        else:
            h = 4.0 + gc - rc
        h = (h / 6.0) % 1.0
        return h, s, v

    @staticmethod
    def hsv_to_rgb(h, s, v):
        if s == 0.0:
            return v, v, v
        i = int(h * 6.0)  # XXX assume int() truncates!
        f = (h * 6.0) - i
        p = v * (1.0 - s)
        q = v * (1.0 - s * f)
        t = v * (1.0 - s * (1.0 - f))
        i = i % 6
        if i == 0:
            return v, t, p
        if i == 1:
            return q, v, p
        if i == 2:
            return p, v, t
        if i == 3:
            return p, q, v
        if i == 4:
            return t, p, v
        if i == 5:
            return v, p, q
        # Cannot get here

    @classmethod
    def get_hsv_circle(cls, color_base, number_colors):
        if number_colors <= 0:
            number_colors = 1

        # hue
        h_min = bpy.context.scene.am_settings.mats_colorize_h_min
        h_max = bpy.context.scene.am_settings.mats_colorize_h_max
        if bpy.context.scene.am_settings.mats_colorize_h_reversed:
            h_min, h_max = h_max, h_min
        h_full = int(bpy.context.scene.am_settings.mats_colorize_h_full)

        # saturation
        s_min = bpy.context.scene.am_settings.mats_colorize_s_min
        s_max = bpy.context.scene.am_settings.mats_colorize_s_max
        if bpy.context.scene.am_settings.mats_colorize_s_reversed:
            s_min, s_max = s_max, s_min

        # value
        v_min = bpy.context.scene.am_settings.mats_colorize_v_min
        v_max = bpy.context.scene.am_settings.mats_colorize_v_max
        if bpy.context.scene.am_settings.mats_colorize_v_reversed:
            v_min, v_max = v_max, v_min

        color_parent = cls.rgb_to_hsv(*color_base)
        for i in range(number_colors):

            # hue
            if not bpy.context.scene.am_settings.mats_colorize_h:
                h = color_parent[0]
            else:
                h = i / (number_colors - h_full)
                if bpy.context.scene.am_settings.mats_colorize_h_other_way:
                    if h_max > h_min:
                        h = (h_min - (1.0 - (h_max - h_min)) * h) % 1
                    else:
                        h = (h_min + (1.0 + (h_max - h_min)) * h) % 1
                else:
                    h = h_min + (h_max - h_min) * h

            # saturation
            if not bpy.context.scene.am_settings.mats_colorize_s:
                s = color_parent[1]
            else:
                s = i / (number_colors - 1)
                s = s_min + (s_max - s_min) * s

            # value
            if not bpy.context.scene.am_settings.mats_colorize_v:
                v = color_parent[2]
            else:
                v = i / (number_colors - 1)
                v = v_min + (v_max - v_min) * v

            # get color
            color = cls.hsv_to_rgb(h, s, v)
            yield color

    def execute(self, context):
        obj = bpy.context.active_object
        materials = obj.material_slots
        number_colors = len(materials)
        if number_colors == 0:
            return {"FINISHED"}

        bpy.ops.object.mode_set(mode="EDIT")

        color = self.get_hsv_circle(color_base=obj.active_material.diffuse_color, number_colors=number_colors)
        material_index = obj.active_material_index
        material_name = obj.active_material.name
        j = 0

        for i in range(number_colors):
            bpy.ops.mesh.select_all(action="DESELECT")
            obj.active_material_index = obj.data.materials.find(materials[i].material.name)
            if obj.active_material:
                obj.active_material.diffuse_color = next(color)
                bpy.ops.object.material_slot_select()
                if obj.active_material_index == material_index:
                    continue
                elif context.scene.am_settings.mats_colorize_name == "prefix":
                    obj.active_material.name = "%s_%s" % (material_name, obj.active_material.name)
                elif context.scene.am_settings.mats_colorize_name == "rename":
                    obj.active_material.name = "%s_%.3i" % (material_name, j)
                    j += 1

        bpy.ops.mesh.select_all(action="DESELECT")
        bpy.ops.object.mode_set(mode="OBJECT")
        self.report({'INFO'}, "%i materials" % number_colors)
        return {"FINISHED"}


class FuncloneFromActive(bpy.types.Operator):
    bl_idname = "mesh.am1d_funclone"
    bl_label = "Mats Funclone From Active"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "Unclone materials, keeping materials of an active object as main"

    @staticmethod
    def parseMaterial(material):
        """extract name and number"""
        name, _, number = material.name.rpartition(".")
        if "." not in material.name or not number.isnumeric():
            name = material.name
            number = ""
        return name, number

    @classmethod
    def compareMaterials(cls, material_1, material_2):
        number_1 = cls.parseMaterial(material_1)[1]
        number_2 = cls.parseMaterial(material_2)[1]
        if not number_1.isnumeric():
            return material_1
        elif not number_2.isnumeric():
            return material_2
        elif int(number_1) < int(number_2):
            return material_1
        else:
            return material_2

    @staticmethod
    def checkMaterial(material):
        for obj in bpy.data.objects:
            for slot in obj.material_slots:
                if slot.material == material:
                    return False
        return True

    def execute(self, context):
        materials = {}

        for slot in context.active_object.material_slots:
            material = slot.material

            name = self.parseMaterial(material)[0]
            if name not in materials:
                materials[name] = material
            else:
                materials[name] = self.compareMaterials(materials[name], material)

        for mesh in context.scene.objects:
            if mesh.type != "MESH":
                continue
            for slot in mesh.material_slots:
                try:
                    slot.material = materials[self.parseMaterial(slot.material)[0]]
                except KeyError:
                    pass

        for material in bpy.data.materials:
            if self.checkMaterial(material):
                material.name += "..."

        for material in materials.values():
            material.name = self.parseMaterial(material)[0]
        return {"FINISHED"}


class MatsPlane(bpy.types.Operator):
    bl_idname = "mesh.am1d_mats_plane"
    bl_label = "Mats Showplane"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "Create polygonal material pallete from an active object"

    def execute(self, context):

        materials = collections.defaultdict(list)
        offset = collections.defaultdict(int)
        active = context.active_object
        x = 3

        # generate list of materials
        h = 0
        for slot in context.active_object.material_slots:
            material = slot.material
            try:
                name, number = material.name.rsplit(".", 1)
                number = int(number)
            except ValueError:
                number = -1
                name = material.name
            materials[name.lower()].append((number, material))
            h += 1

        h = max(h, 199) // 100 * 10
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(context.active_object.data)
        for face in bm.faces:  # remove all old faces
            face.hide = False
            face.select = True
        bpy.ops.mesh.delete(type="FACE")

        # assing new materials
        previous_face = None
        j = 0
        for i, name in enumerate(sorted(materials)):
            material_list = materials[name]
            column, pos_y = divmod(i, h)
            pos_x = offset[column]
            offset[column + 1] = max(offset[column + 1], pos_x + len(material_list) + .5)
            for material in sorted(material_list, key=lambda mt: mt[0]):
                vertex_0 = bm.verts.new(mathutils.Vector([pos_x * x, pos_y * -x, 0]))
                vertex_1 = bm.verts.new(mathutils.Vector([(pos_x + 1) * x, pos_y * -x, 0]))
                vertex_2 = bm.verts.new(mathutils.Vector([(pos_x + 1) * x, (pos_y + 1) * -x, 0]))
                vertex_3 = bm.verts.new(mathutils.Vector([pos_x * x, (pos_y + 1) * -x, 0]))
                pos_x += 1

                face = bm.faces.new([vertex_3, vertex_2, vertex_1, vertex_0])
                if previous_face:
                    previous_face.select = False
                face.select = True
                previous_face = face

                context.active_object.data.materials[j] = material[1]
                face.material_index = j
                j += 1

        bmesh.update_edit_mesh(context.active_object.data)
        bpy.ops.object.mode_set(mode="OBJECT")
        return {"FINISHED"}


class MatsTexlist(bpy.types.Operator):
    bl_idname = "mesh.am1d_mats_texlist"
    bl_label = "Matlist Textures Compare"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "Compare scene material and texture names"
    file_name = "MATERIALS"

    def execute(self, context):
        data = {"Coincident": [], "Multiple": [], "None": [], "Mismatch": []}
        for node in context.scene.objects:
            for slot in node.material_slots:
                try:
                    mats = [node.image.name for node in slot.material.node_tree.nodes if node.type == "TEX_IMAGE"]
                    # has no textures
                    if not mats:
                        data["None"].append(slot.name)

                    # has multiple
                    elif len(mats) > 1:
                        for file in mats:
                            data["Multiple"].append("%s == %s" % (slot.name, file))
                        data["Multiple"].append("")

                    # one similar
                    elif os.path.splitext(mats[0])[0] == slot.name:
                        data["Coincident"].append("%s == %s" % (slot.name, mats[0]))

                    # one different
                    else:
                        data["Mismatch"].append("%s == %s" % (slot.name, mats[0]))
                except:
                    pass

        if "MATLIST.txt" in bpy.data.texts:
            text_block = bpy.data.texts["MATLIST.txt"]
        else:
            text_block = bpy.data.texts.new(name="MATLIST.txt")
        text_block.clear()

        for name in ("Mismatch", "Multiple", "None", "Coincident"):
            text_block.write("_______________________" + name + "\n")
            for string in data[name]:
                text_block.write(string + "\n")
            text_block.write("\n\n")

        return {"FINISHED"}


class SelectedMaterials(bpy.types.Operator):
    """get materials from selected nodes"""

    bl_idname = "mesh.am1d_get_materials_selected"
    bl_label = "Matlist Selected"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        materials = []
        for node in bpy.data.objects:
            if not node.select:
                continue
            for material in node.material_slots:
                materials.append(material.name)

        materials.sort(key=lambda m: m.lower())

        if "MATLIST.txt" in bpy.data.texts:
            text_block = bpy.data.texts["MATLIST.txt"]
        else:
            text_block = bpy.data.texts.new(name="MATLIST.txt")
        text_block.clear()

        for material in materials:
            text_block.write(material + "\n")

        return {"FINISHED"}


class AllMaterials(bpy.types.Operator):
    """get list of all materials"""

    bl_idname = "mesh.am1d_get_materials_all"
    bl_label = "Matlist Scene"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "Read materials from scene"

    def execute(self, context):

        materials = [m.name for m in bpy.data.materials]
        materials.sort(key=lambda m: m.lower())

        if "MATLIST.txt" in bpy.data.texts:
            text_block = bpy.data.texts["MATLIST.txt"]
        else:
            text_block = bpy.data.texts.new(name="MATLIST.txt")
        text_block.clear()

        for material in materials:
            text_block.write(material + "\n")

        return {"FINISHED"}
