import math
import collections
import mathutils
import bpy
import bmesh
from .amTools import *


def crossSplit(func):
    """decorator for splitting edges in 2D/3D"""

    def run(self, context):

        obj = bpy.context.object
        me = obj.data
        bm = bmesh.from_edit_mesh(me)

        b_edges = tuple(bm.edges)
        edges = [b_edges[e.index] for e in bm.edges if e.select]
        if len(b_edges) < 2:
            bpy.context.tool_settings.mesh_select_mode = True, True, True
            return {"FINISHED"}

        edges_split = [[] for e in edges]

        # intersect all edges
        for i, edge_1 in enumerate(edges):
            for j, edge_2 in enumerate(edges[i + 1:], start=i + 1):

                point = func(self, edge_1.verts[0].co, edge_1.verts[1].co, edge_2.verts[0].co, edge_2.verts[1].co,
                             context)

                if point:
                    for position, point_data in zip((i, j), point):
                        if 0 < point_data < 1:
                            edges_split[position].append(point_data)

        # set points
        for edge, percents in zip(edges, edges_split):

            vector = edge.verts[1].co - edge.verts[0].co
            point = edge.verts[0].co

            geom_split = bmesh.ops.bisect_edges(bm, edges=[edge], cuts=len(percents))["geom_split"]
            vertices = [v for v in geom_split if isinstance(v, bmesh.types.BMVert)]
            vertices.sort(key=lambda v: (point - v.co).length)
            percents.sort()
            for vertex, percent in zip(vertices, percents):
                vertex.co = point + vector * percent

        bmesh.update_edit_mesh(me)
        bpy.context.tool_settings.mesh_select_mode = True, True, True
        return {"FINISHED"}

    return run


class CornerCross(bpy.types.Operator):
    """split edges in 2D projection on Z"""

    bl_idname = "mesh.am1d_corner_cross"
    bl_label = "Corner Cross"
    bl_description = "Slice intersecting edges from Z projection"
    bl_options = {'REGISTER', 'UNDO'}

    @crossSplit
    def execute(self, v1, v2, v3, v4, context):
        """find projection in 2D space"""

        v1 = mathutils.Vector((v1.x, v1.y))
        v2 = mathutils.Vector((v2.x, v2.y))
        v3 = mathutils.Vector((v3.x, v3.y))
        v4 = mathutils.Vector((v4.x, v4.y))

        point = mathutils.geometry.intersect_line_line_2d(v1, v2, v3, v4)
        if point is None:
            if context.scene.am_settings.overlap:
                return None
            else:
                return self.findNonOverlap((v1, v2), (v3, v4))

        try:
            percent_1 = (point - v1).length / (v2 - v1).length
        except ZeroDivisionError:
            percent_1 = 0

        try:
            percent_2 = (point - v3).length / (v4 - v3).length
        except ZeroDivisionError:
            percent_2 = 0

        return percent_1, percent_2

    @staticmethod
    def findNonOverlap(line1, line2):
        """find projection without overlap"""

        xdiff = (line1[0].x - line1[1].x, line2[0].x - line2[1].x)
        ydiff = (line1[0].y - line1[1].y, line2[0].y - line2[1].y)

        def det(a, b):
            return a[0] * b[1] - a[1] * b[0]

        div = det(xdiff, ydiff)
        if div == 0:
            return None

        d = det(*line1), det(*line2)
        x = det(d, xdiff) / div
        y = det(d, ydiff) / div

        point = mathutils.Vector((x, y))
        v1 = line1[1] - line1[0]

        try:
            param_1 = (point - line1[0]) * v1 / v1.length / v1.length
        except ZeroDivisionError:
            param_1 = 0

        v2 = line2[1] - line2[0]
        try:
            param_2 = (point - line2[0]) * v2 / v2.length / v2.length
        except ZeroDivisionError:
            param_2 = 0

        return param_1, param_2


class ExtendCross(bpy.types.Operator):
    """split edges in 3D"""

    bl_idname = "mesh.am1d_extend_cross"
    bl_label = "Extend Cross"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "Slice intersecting edges in 3D"

    @crossSplit
    def execute(self, v1, v2, v3, v4, context):
        """find intersection in 3D"""
        try:
            p1, p2 = mathutils.geometry.intersect_line_line(v1, v2, v3, v4)
        except TypeError:
            return None

        vector_1 = v2 - v1
        try:
            percent_1 = (p1 - v1) * vector_1 / vector_1.length / vector_1.length
        except ZeroDivisionError:
            percent_1 = 0

        vector_2 = v4 - v3
        try:
            percent_2 = (p2 - v3) * vector_2 / vector_2.length / vector_2.length
        except ZeroDivisionError:
            percent_2 = 0

        if context.scene.am_settings.overlap and not (0 < percent_1 < 1 and 0 < percent_2 < 1):
            return None
        return percent_1, percent_2


class StoreFacesOperator(bpy.types.Operator):
    bl_idname = "mesh.am1d_project_edges_store"
    bl_label = "Project Edges Store"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "Store selected Faces to project to"

    def execute(self, context):
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(context.active_object.data)
        ProjectEdgesOperator.face_indices = {f.index for f in bm.faces if f.select}
        if not len(ProjectEdgesOperator.face_indices):
            ProjectEdgesOperator.face_indices = set(range(len(bm.faces)))
        for f in bm.faces:
            f.select = False
        ProjectEdgesOperator.target_mesh = context.active_object.name
        bpy.context.tool_settings.mesh_select_mode = [False, True, False]
        return {"FINISHED"}


class ProjectEdgesOperator(bpy.types.Operator):
    bl_idname = "mesh.am1d_project_edges"
    bl_label = "Project Edges"
    bl_options = {'REGISTER', 'UNDO'}
    bl_description = "Project selected edges to stored faces"
    face_indices = set()  # faces to preserve
    target_mesh = ""

    def execute(self, context):

        def project(function):

            def run(vertex_1, vertex_2, reverse=False):
                p_1 = target_mesh.matrix_world.inverted() * (main_mesh.matrix_world * vertex_1.co)
                p_2 = target_mesh.matrix_world.inverted() * (main_mesh.matrix_world * vertex_2.co)
                vector = p_2 - p_1
                if edge_vector:  # project in one direction
                    vector = edge_vector if edge_vector.angle(vector) < math.pi / 2 else -edge_vector
                if reverse:
                    vector = -vector
                result, location = function(p_2, vector)
                if result:
                    location = main_mesh.matrix_world.inverted() * (target_mesh.matrix_world * location)
                    vectors.append((location, vertex_2))

            return run

        @project
        def projectOnMesh(*args):
            return target_mesh.ray_cast(*args)[:2]

        @project
        def projectOnPlane(p_2, vector):
            location = mathutils.geometry.intersect_ray_tri(*plane, vector, p_2, False)
            if location:
                return True, location
            else:
                return False, None

        # duplicate object and remove all faces
        main_mesh = context.active_object
        if self.target_mesh in context.scene.objects:
            source_mesh = context.scene.objects[self.target_mesh] or main_mesh
        else:
            source_mesh = main_mesh
        bpy.ops.object.mode_set(mode="OBJECT")
        target_mesh = source_mesh.copy()
        target_mesh.data = source_mesh.data.copy()
        context.scene.objects.link(target_mesh)
        context.scene.objects.active = target_mesh
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(context.active_object.data)
        bm.faces.ensure_lookup_table()
        for f in bm.faces:
            f.hide = False
            f.select = f.index not in self.face_indices
        bpy.ops.mesh.delete(type="FACE")
        bmesh.update_edit_mesh(target_mesh.data, True)
        bpy.ops.object.mode_set(mode="OBJECT")

        # project edges
        context.scene.objects.active = main_mesh
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(context.active_object.data)

        # find main vector. only for active edge vector
        edge_vector = None
        if bpy.context.scene.am_settings.project_edges_active:
            for edge in reversed(bm.select_history):
                if isinstance(edge, bmesh.types.BMEdge):
                    v_1 = target_mesh.matrix_world.inverted() * (main_mesh.matrix_world * edge.verts[0].co)
                    v_2 = target_mesh.matrix_world.inverted() * (main_mesh.matrix_world * edge.verts[1].co)
                    edge_vector = v_2 - v_1
                    break

        # generate plane for 1 face selected
        plane = None
        project_function = projectOnMesh
        if len(self.face_indices) == 1:
            project_function = projectOnPlane
            face_id = list(self.face_indices).pop()
            plane = [source_mesh.data.vertices[v].co for v in source_mesh.data.polygons[face_id].vertices[:3]]

        # project points
        for e in bm.edges:
            if not e.select:
                continue
            vectors = []
            project_function(*e.verts)
            project_function(*e.verts, True)
            project_function(*reversed(e.verts))
            project_function(*reversed(e.verts), True)
            if vectors:
                point, vertex = min(vectors, key=lambda v: (v[0] - v[1].co).length)
                vertex.co = point

        bmesh.update_edit_mesh(main_mesh.data, True)
        bpy.data.objects.remove(target_mesh, True)
        return {"FINISHED"}


class RotatePatch(bpy.types.Operator):
    side = bpy.props.IntProperty(default=2, min=0, max=2)
    bl_idname = "mesh.am1d_rotate_patch"
    bl_label = "Spin Profile"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        self.applyRotation(context.scene.am_settings.rotate_patch_number, context.scene.am_settings.rotate_patch_closed)
        return {"FINISHED"}

    def rotateVector(self, vector, angle):
        rotor = mathutils.Euler((0.0, 0.0, 0.0), 'XYZ')
        rotor[self.side] = angle
        vector_2 = vector.normalized()
        vector_2.rotate(rotor)
        return vector_2 * vector.length / math.cos(angle)

    @staticmethod
    def buildChain(edges):

        def checkEdges(link_edges):
            num = 0
            for edge in link_edges:
                num += edge in edges_set
            return num

        vertices = set()
        edges_set = set(edges)

        for e in edges:
            vertices.add(e.verts[0])
            vertices.add(e.verts[1])

        visited_edges = set()
        border = [v for v in vertices if checkEdges(v.link_edges) == 1]
        if len(border) == 2:
            is_closed = False
            start, end = border

        else:
            is_closed = True
            start, end = edges[0].verts
            visited_edges.add(edges[0])

        current = start
        path = [start]

        while current != end:
            next_edges = [e for e in current.link_edges if e in edges_set and e not in visited_edges]
            if not next_edges:  # can't build path
                is_closed = False
                break
            edge = next_edges[0]
            visited_edges.add(edge)
            current = edge.other_vert(current)
            path.append(current)

        return is_closed, path

    def applyRotation(self, rotation_number=4, always_closed=False):
        """side 0 - x, 1 - y, 2 - z"""
        angle = math.radians(360. / rotation_number)

        active = bpy.context.scene.objects.active
        bpy.ops.object.mode_set(mode='EDIT')
        bm = bmesh.from_edit_mesh(active.data)
        matrix = active.matrix_world
        matrix_back = matrix.inverted()

        is_closed, path = self.buildChain([e for e in bm.edges if e.select] or bm.edges)
        is_closed = (always_closed or is_closed) and len(path) != 2
        arrays = []

        # create vertex cloud
        for vertex in path:
            # find new position
            vector = matrix * vertex.co - bpy.context.scene.cursor_location
            v = vector[self.side]
            vector[self.side] = 0
            vector = self.rotateVector(vector, angle / 2)
            vector[self.side] = v

            array = [bm.verts.new(matrix_back * (bpy.context.scene.cursor_location + vector))]
            arrays.append(array)
            for i in range(1, rotation_number):
                rotor = mathutils.Euler((0.0, 0.0, 0.0), 'XYZ')
                rotor[self.side] = angle
                vector.rotate(rotor)
                array.append(bm.verts.new(matrix_back * (bpy.context.scene.cursor_location + vector)))

        for i in range(len(arrays) - 1 + is_closed):
            for j in range(rotation_number):
                i_next = (i + 1) % len(arrays)
                j_next = (j + 1) % rotation_number
                bm.faces.new((arrays[i][j], arrays[i_next][j], arrays[i_next][j_next], arrays[i][j_next]))

        bmesh.update_edit_mesh(active.data)
        bpy.ops.object.mode_set(mode='OBJECT')


class SurfaceSew(bpy.types.Operator):
    bl_idname = "mesh.am1d_surface_sew"
    bl_label = "Surface sew"
    bl_description = "Transform vertices of one surface to closest vertices of another surface"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        result = self.operator(context, True)
        self.report({"INFO"}, "%i found, %i moved" % result)
        return {"FINISHED"}

    @staticmethod
    def operator(context, move_vertex=True):
        # active object data
        active_object = context.object
        matrix_a = active_object.matrix_world
        matrix_a_rev = matrix_a.inverted()
        vertex_moved = vertex_count = 0
        is_radius = context.scene.am_settings.surface_sew_is_radius
        radius = context.scene.am_settings.surface_sew_radius
        meshes = [mesh for mesh in bpy.data.objects if mesh.type == "MESH" and mesh.select and mesh != active_object]
        if not meshes:
            return 0, 0

        for mesh in meshes:
            matrix_m = mesh.matrix_world
            matrix_m_rev = matrix_m.inverted()
            vertices = [matrix_m_rev * (matrix_a * v.co) for v in active_object.data.vertices]

            context.scene.objects.active = mesh
            bpy.ops.object.mode_set(mode='EDIT')
            bm = bmesh.from_edit_mesh(mesh.data)

            if context.scene.am_settings.surface_sew_selected:
                skip = {vertex.index for vertex in bm.verts if not vertex.select}
            else:
                skip = set()

            for face in bm.faces:  # bug fix of unselectable vertices
                face.select = False

            for vertex in bm.verts:

                # selected only and boundary
                if vertex.index in skip or (context.scene.am_settings.surface_sew_boundary and not vertex.is_boundary):
                    continue

                # quick method. find closest face
                if context.scene.am_settings.surface_sew_quick:
                    face_id = active_object.closest_point_on_mesh(matrix_a_rev * (matrix_m * vertex.co))[-1]
                    if face_id == -1:
                        continue
                    points = [vertices[v] for v in active_object.data.polygons[face_id].vertices]
                # check all vertices
                else:
                    points = vertices

                # now find best match
                point = min(points, key=lambda v: (v - vertex.co).length)
                length = (point - vertex.co).length
                if is_radius and length > radius:
                    continue

                vertex_count += 1
                if context.scene.am_settings.surface_sew_select_found:
                    vertex.select = True

                if length < 1e-3 and move_vertex:
                    continue
                vertex_moved += 1
                vertex.select = True
                if move_vertex:
                    vertex.co = point

            bmesh.update_edit_mesh(mesh.data)
            bpy.ops.object.mode_set(mode='OBJECT')

        return vertex_count, vertex_moved


class SurfaceSewSelectByRadius(bpy.types.Operator):
    bl_idname = "mesh.am1d_surface_sew_select_by_radius"
    bl_label = "Select By Radius"
    bl_description = "Select vertices instead transform"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        result = SurfaceSew.operator(context, False)
        self.report({"INFO"}, "%i found" % result[0])
        return {"FINISHED"}


class PokeLoops(bpy.types.Operator):
    bl_idname = "mesh.am1d_poke_loops"
    bl_label = "PP Poke Loops"
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def getEdgesGroups(edges):
        edges = set(edges)
        vertices = {}
        i = 0
        while edges:
            e = edges.pop()
            edges_next = [e]
            while edges_next:
                e = edges_next.pop(-1)
                for v in e.verts:
                    vertices[v] = i
                    for e_n in v.link_edges:
                        if e_n in edges:
                            edges.remove(e_n)
                            edges_next.append(e_n)
            i += 1

        return vertices

    def execute(self, context):

        def cameraSpace(point):
            return view_matrix.inverted() * (matrix_world * point)

        def meshSpace(point):
            return matrix_world.inverted() * (view_matrix * point)

        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        view_matrix = view3d()[1].view_matrix.transposed()
        matrix_world = context.active_object.matrix_world
        bm = bmesh.from_edit_mesh(context.active_object.data)
        edges = [e for e in bm.edges if e.select]
        vertices = self.getEdgesGroups(edges)
        radius = context.scene.am_settings.poke_loops_radius
        once = context.scene.am_settings.poke_loops_once
        group = context.scene.am_settings.poke_loops_group
        project = context.scene.am_settings.poke_loops_project
        is_merge = context.scene.am_settings.poke_loops_merge
        threshold = context.scene.am_settings.poke_loops_threshold

        data = collections.defaultdict(list)
        used_vertices = set()

        edges_before = sum([e.calc_length() for e in bm.edges])

        # generate split params
        for e in edges:
            start = cameraSpace(e.verts[0].co)
            vector = cameraSpace(e.verts[1].co) - start
            if not vector.length:
                continue
            for v in vertices:
                if v in used_vertices or v in e.verts:
                    continue
                if group and vertices[v] == vertices[e.verts[0]]:  # check for the same split group
                    continue
                vertex = cameraSpace(v.co)
                p = (vertex - start) * vector / (vector.length ** 2)

                if not 0 < p < 1:
                    continue

                point = start + vector * p
                vector_z = point - vertex
                vector_z.z = 0
                if not radius or radius > vector_z.length:
                    data[e].append((p, v.index))
                    if once:
                        used_vertices.add(v)

        # make split
        PokeLoopsDebug.latest = []
        for edge, percents in data.items():
            start_local = edge.verts[0].co
            start = cameraSpace(start_local)
            vector = cameraSpace(edge.verts[1].co) - start

            geom_split = bmesh.ops.bisect_edges(bm, edges=[edge], cuts=len(percents))["geom_split"]
            vertices = [v for v in geom_split if isinstance(v, bmesh.types.BMVert)]
            vertices.sort(key=lambda v: (start_local - v.co).length)
            percents.sort(key=lambda v: v[0])
            bm.verts.ensure_lookup_table()

            for vertex, (percent, index) in zip(vertices, percents):
                point = start + vector * percent
                vertex.co = meshSpace(point)
                vertex.select = True

                old_point = cameraSpace(bm.verts[index].co)
                if project == "initial":  # project initial vertices
                    old_point.x = point.x
                    old_point.y = point.y
                    bm.verts[index].co = meshSpace(old_point)

                elif project == "generated":
                    point.x = old_point.x
                    point.y = old_point.y
                    vertex.co = meshSpace(point)

                PokeLoopsDebug.latest.append((vertex.index, percent, vertex.co, point, vector))

        if is_merge:
            bpy.ops.mesh.remove_doubles(threshold=threshold)
        bmesh.update_edit_mesh(context.active_object.data)
        bm.edges.ensure_lookup_table()
        edges_after = sum([e.calc_length() for e in bm.edges])
        self.report({"INFO"}, "Diff = %.3f" % (edges_before - edges_after))

        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        bpy.ops.mesh.select_mode(type="VERT")
        return {"FINISHED"}


class PokeLoopsDebug(bpy.types.Operator):

    latest = ""
    file_name = "poke loops log"
    bl_idname = "mesh.am1d_poke_loops_log"
    bl_label = "Poke Loops Debug"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        if self.file_name in bpy.data.texts:
            text_block = bpy.data.texts[self.file_name]
        else:
            text_block = bpy.data.texts.new(name=self.file_name)
        text_block.write(str(self.latest) + "\n")
        return {"FINISHED"}


class SurfaceLevelset(bpy.types.Operator):
    bl_idname = "mesh.am1d_surface_levelset"
    bl_label = "Surface Levelset"
    bl_description = "Project surface vertices Z levels to closest surface points"
    bl_options = {'REGISTER', 'UNDO'}

    def projectPoints(self, reference, vertices, matrix):
        """project list of vertices on reference mesh"""
        reference_matrix = reference.matrix_world
        reference_inverse_matrix = reference_matrix.inverted()
        inverse_matrix = matrix.inverted()
        for vertex in vertices:
            reference_vertex = reference_inverse_matrix * (matrix * vertex.co)  # translate into ref space
            result, *points = reference.closest_point_on_mesh(reference_vertex)
            if not result:
                continue
            point = inverse_matrix * (reference_matrix * points[0])
            vertex.co[2] = point[2]

    def execute(self, context):
        reference = context.scene.objects.active
        if not reference:
            return {"FINISHED"}

        selected_meshes = [m for m in context.selected_objects if m != reference and m.type == "MESH"]
        if not selected_meshes:
            return {"FINISHED"}
        if context.scene.am_settings.surface_levelset:  # selected vertices
            for mesh in selected_meshes:
                context.scene.objects.active = mesh
                bpy.ops.object.mode_set(mode='EDIT')
                bm = bmesh.from_edit_mesh(mesh.data)
                vertex_array = [vertex for vertex in bm.verts if vertex.select]
                self.projectPoints(reference, vertex_array, mesh.matrix_world)
                bmesh.update_edit_mesh(mesh.data)
                bpy.ops.object.mode_set(mode='OBJECT')
            context.scene.objects.active = reference

        else:
            for mesh in selected_meshes:
                self.projectPoints(reference, mesh.data.vertices, mesh.matrix_world)

        return {"FINISHED"}


class Dissect(bpy.types.Operator):
    """split mesh by 2 selected vertices and connect them"""

    bl_idname = "mesh.am1d_dissect"
    bl_label = "Dissect"
    bl_description = "Join 2 wiremesh contours by 2 edges"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        obj = bpy.context.object
        me = obj.data
        bm = bmesh.from_edit_mesh(me)
        edges = [v for v in bm.edges if v.select]

        if len(edges) != 2:
            self.report({"INFO"}, "select 2 edges")
            return {"FINISHED"}
        for v in edges:
            if len(v.link_faces):
                self.report({"INFO"}, "vertices connected to face")
                return {"FINISHED"}

        other = list(edges[0].verts) + list(edges[1].verts)
        for vertex in other:
            vertex.select = False
        points = [mathutils.Vector((v.co.x, v.co.y)) for v in other]
        new_vertex = [bm.verts.new(v.co) for v in other]
        bm.edges.new((new_vertex[0], new_vertex[1]))
        bm.edges.new((new_vertex[2], new_vertex[3]))
        if mathutils.geometry.intersect_line_line_2d(points[0], points[2], points[1], points[3]):
            bm.edges.new((other[0], other[3]))
            bm.edges.new((other[1], other[2]))
            bm.edges.new((new_vertex[0], new_vertex[3]))
            bm.edges.new((new_vertex[1], new_vertex[2]))
            new_vertex = new_vertex[0], new_vertex[3], new_vertex[2], new_vertex[1]
        else:
            bm.edges.new((other[0], other[2]))
            bm.edges.new((other[1], other[3]))
            bm.edges.new((new_vertex[0], new_vertex[2]))
            bm.edges.new((new_vertex[1], new_vertex[3]))
            new_vertex = new_vertex[0], new_vertex[2], new_vertex[3], new_vertex[1]
        bm.faces.new(new_vertex).select = True
        bpy.ops.object.material_slot_assign()
        bmesh.ops.delete(bm, geom=edges, context=2)
        bmesh.update_edit_mesh(me)
        return {"FINISHED"}


class MapCut(bpy.types.Operator):
    bl_idname = "mesh.am1d_map_cut"
    bl_label = "Map Cut"
    bl_description = "Knife project multiple objects"
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def createProjectPlane(context, plane_mesh):
        context.scene.objects.active = plane_mesh
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(plane_mesh.data)
        error = len(bm.faces)
        for vertex in bm.verts:
            if len(vertex.link_edges) != 2:
                error = True
        bpy.ops.object.mode_set(mode="OBJECT")
        if error:
            return None

        target_mesh = plane_mesh.copy()
        target_mesh.data = plane_mesh.data.copy()
        context.scene.objects.link(target_mesh)
        context.scene.objects.active = target_mesh
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(target_mesh.data)
        left_edges = set(bm.edges)

        while left_edges:
            edge = left_edges.pop()
            end_vertex, vertex = edge.verts
            new_face = [vertex]
            while vertex != end_vertex:
                edge = vertex.link_edges[0] if vertex.link_edges[0] in left_edges else vertex.link_edges[1]
                left_edges.discard(edge)
                vertex = edge.other_vert(vertex)
                new_face.append(vertex)
            bm.faces.new(new_face)

        bmesh.update_edit_mesh(target_mesh.data)
        bpy.ops.object.mode_set(mode="OBJECT")
        return target_mesh

    @staticmethod
    def splitMesh(context, mesh, project_plane, camera_vector):
        plane_matrix = project_plane.matrix_world.inverted()
        mesh_matrix = mesh.matrix_world
        context.scene.objects.active = mesh
        bm = bmesh.from_edit_mesh(mesh.data)
        for face in bm.faces:
            if face.select:
                continue
            select = True
            for vertex in face.verts:
                center = mesh_matrix * vertex.co
                # plane project local coordinates
                aim_point = plane_matrix * (center + camera_vector)
                center = plane_matrix * center
                aim_vector = aim_point - center
                result = project_plane.ray_cast(center, aim_vector)[0] or project_plane.ray_cast(center, -aim_vector)[0]
                select = select and result
            face.select = select

    def execute(self, context):
        region, rv3d, v3d, area = view3d()
        project_vector = mathutils.Vector((0, 0, 1)) * rv3d.view_matrix - mathutils.Vector() * rv3d.view_matrix

        cut_plane = context.active_object
        meshes_to_cut = [mesh for mesh in context.selected_objects if mesh != cut_plane and mesh.type == "MESH"]
        project_plane = self.createProjectPlane(context, cut_plane)
        if not project_plane:
            self.report({"INFO"}, "input contour error")
            return {"FINISHED"}
        new_meshes = []
        for mesh in meshes_to_cut:
            for node in context.scene.objects:  # deselect all
                node.select = False
            context.scene.objects.active = mesh
            cut_plane.select = True
            override = {
                "scene": context.scene,
                "region": region,
                "area": area,
                "space": v3d,
                "active_object": context.object,
                "window": context.window,
                "screen": context.screen,
                "selected_objects": context.selected_objects,
                "edit_object": context.object
            }
            bpy.ops.object.mode_set(mode="EDIT")
            bpy.ops.wm.redraw_timer(type="DRAW_WIN_SWAP", iterations=1)
            bpy.ops.mesh.knife_project(override, cut_through=True)
            self.splitMesh(context, mesh, project_plane, project_vector)
            try:
                bpy.ops.mesh.separate(type="SELECTED")
                new_mesh = [m for m in context.selected_objects if m != mesh and m != cut_plane]
                new_meshes.extend(new_mesh)
            except:
                pass
            bpy.ops.object.mode_set(mode="OBJECT")
        for node in context.scene.objects:  # deselect all
            node.select = False
        project_plane.select = True
        bpy.ops.object.delete()
        for mesh in new_meshes:
            mesh.select = True
        return {"FINISHED"}


class BevelFromSeam(bpy.types.Operator):
    bl_idname = "mesh.am1d_bevel_from_seam"
    bl_label = "WW write Bevel from Seam ЦЦ цц"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        context.active_object.select = True
        for mesh in context.selected_objects:
            if mesh.type != "MESH":
                continue
            context.scene.objects.active = mesh
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='EDGE')
            bm = bmesh.from_edit_mesh(mesh.data)
            for edge in bm.edges:
                edge.select = edge.seam
            bpy.ops.transform.edge_bevelweight(value=1)
            bpy.ops.mesh.select_all(action='INVERT')
            bpy.ops.transform.edge_bevelweight(value=-1.5)
            bpy.ops.mesh.select_all(action='INVERT')
            bm.free()
            bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='EDIT')
        return {"FINISHED"}


class SeamFromBevel(bpy.types.Operator):
    bl_idname = "mesh.am1d_seam_from_bevel"
    bl_label = "WS write Seam from Bevel"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.object.mode_set(mode='OBJECT')
        for mesh in context.selected_objects:
            if mesh.type != "MESH":
                continue
            context.scene.objects.active = mesh
            mesh.data.use_customdata_edge_bevel = True
            edge_weights = [e.bevel_weight for e in mesh.data.edges]
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='EDGE')
            bm = bmesh.from_edit_mesh(mesh.data)
            for edge, weight in zip(bm.edges, edge_weights):
                edge.select = weight > 0
            bpy.ops.mesh.mark_seam(clear=False)
            bpy.ops.mesh.select_all(action='INVERT')
            bpy.ops.mesh.mark_seam(clear=True)
            bpy.ops.mesh.select_all(action='INVERT')
            bm.free()
            bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.mode_set(mode='EDIT')
        return {"FINISHED"}
