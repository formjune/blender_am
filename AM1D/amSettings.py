import bpy


class Settings(bpy.types.PropertyGroup):
    overlap = bpy.props.BoolProperty(default=False, description="Limit slicing to overlapping edges")
    close_relative = bpy.props.BoolProperty(default=True)
    from_active = bpy.props.BoolProperty(default=False)
    select_absolute_scale = bpy.props.BoolProperty(default=False)
    select_relative_type = bpy.props.EnumProperty(
        items=[
            ("=", "=", ""),
            (">=", ">=", ""),
            (">", ">", ""),
            ("<=", "<=", ""),
            (" <", "<", "")
        ])
    islands_distance = bpy.props.FloatProperty(default=0.1, min=0, precision=3, step=5)
    islands_split = bpy.props.IntProperty(default=3, min=1, step=1)
    islands_sort = bpy.props.EnumProperty(
        items=[
            ("space", "space", ""),
            ("name", "name", ""),
            ("verts", "verts", "")
        ])
    islands_mode = bpy.props.EnumProperty(
        items=[
            ("table", "table", ""),
            ("compact", "compact", ""),
            ("area", "area", "")
        ])
    islands_origin = bpy.props.BoolProperty(default=False)
    islands_group = bpy.props.BoolProperty(default=True)
    slope_length = bpy.props.FloatProperty(default=1, min=0.1, step=10, precision=3)
    slope_mode = bpy.props.EnumProperty(
        items=[
            ("vertical", "vertical", ""),
            ("horizontal", "horizontal", ""),
            ("constant", "constant", "")
        ])
    slope_center = bpy.props.BoolProperty(default=True)
    slope_separated = bpy.props.BoolProperty(default=False)
    far_origin = bpy.props.FloatProperty(default=2, min=0.1, step=10, precision=3)
    surface_levelset = bpy.props.BoolProperty(default=False, description="Limit projection to selected vertices")
    isolines_step = bpy.props.FloatProperty(default=.1, min=0.01, step=10, precision=3)
    isolines_main = bpy.props.FloatProperty(default=1, min=0.01, step=10, precision=3)
    obj_split_input = bpy.props.StringProperty(subtype="FILE_PATH")
    obj_split_output = bpy.props.StringProperty(subtype="FILE_PATH")
    obj_split_count = bpy.props.IntProperty(default=5, min=1, step=1)
    obj_split_remove_uvs = bpy.props.BoolProperty(default=False)
    obj_split_remove_normals = bpy.props.BoolProperty(default=False)
    surface_sew_selected = bpy.props.BoolProperty(default=False, description="Limit operation to selected vertices")
    surface_sew_is_radius = bpy.props.BoolProperty(default=True, description="Limit operation to given radius")
    surface_sew_boundary = bpy.props.BoolProperty(default=False, description="Limit operation to boundary vertices")
    surface_sew_radius = bpy.props.FloatProperty(default=1, min=0, step=10, precision=4)
    surface_sew_quick = bpy.props.BoolProperty(default=True)
    surface_sew_select_found = bpy.props.BoolProperty(name="", default=False)
    rotate_patch_number = bpy.props.IntProperty(default=8, min=3, step=1)
    rotate_patch_closed = bpy.props.BoolProperty(default=False)
    fencer_dist = bpy.props.FloatProperty(default=1, min=1e-3, step=1e2, precision=3)
    fencer_no_z = bpy.props.BoolProperty(default=False)
    fencer_center = bpy.props.EnumProperty(
        items=[
            ("pivot 3d", "pivot 3d", ""),
            ("origin", "origin", "")
        ])
    fencer_aim = bpy.props.EnumProperty(
        items=[
            ("x", "x", ""),
            ("y", "y", ""),
            ("z", "z", ""),
            ("-x", "-x", ""),
            ("-y", "-y", ""),
            ("-z", "-z", "")
        ],
        default="x")
    fencer_up = bpy.props.EnumProperty(
        items=[
            ("x", "x", ""),
            ("y", "y", ""),
            ("z", "z", ""),
            ("-x", "-x", ""),
            ("-y", "-y", ""),
            ("-z", "-z", "")
        ],
        default="z")
    project_edges_active = bpy.props.BoolProperty(default=False)
    invert_visibility = bpy.props.StringProperty(default="RESTRICT_VIEW_OFF")
    invert_l_visibility = bpy.props.StringProperty(default="RESTRICT_VIEW_OFF")
    convert_render = bpy.props.StringProperty(default="RESTRICT_VIEW_OFF")

    # poke loops
    poke_loops_radius = bpy.props.FloatProperty(default=.1, min=0, step=100, precision=4)
    poke_loops_once = bpy.props.BoolProperty(default=True)
    poke_loops_group = bpy.props.BoolProperty(default=False)
    poke_loops_project = bpy.props.EnumProperty(
        items=[
            ("none", "no project", " no vertices movement after generation"),
            ("initial", "linear", "project initial vertices"),
            ("generated", "radial", "project generated to initial vertices")
        ],
        default="initial")
    poke_loops_merge = bpy.props.BoolProperty(default=True)
    poke_loops_threshold = bpy.props.FloatProperty(default=.01, min=0, precision=3, step=10)

    # bricker
    bricker_offset = bpy.props.FloatProperty(default=0, min=0, precision=3, step=10)
    bricker_thickness = bpy.props.FloatProperty(default=.05, min=0, precision=3, step=10)
    bricker_height = bpy.props.FloatProperty(default=.07, min=0, precision=3, step=10)
    bricker_step = bpy.props.FloatProperty(default=.15, min=1e-3, precision=3, step=10)
    bricker_double = bpy.props.BoolProperty(default=False)
    bricker_clean = bpy.props.BoolProperty(default=True)
    bricker_modifier = bpy.props.BoolProperty(default=True)

    # scribe
    scribe_use_cursor = bpy.props.BoolProperty(default=True)
    scribe_use_offset = bpy.props.BoolProperty(default=True)
    scribe_use_obname = bpy.props.BoolProperty(default=True)
    scribe_scale = bpy.props.FloatProperty(default=1, min=0, precision=3, step=10)
    scribe_offset = bpy.props.FloatProperty(default=.5, min=0, precision=3, step=10)
    scribe_font = bpy.props.StringProperty(subtype="FILE_PATH")
    scribe_font_editor = bpy.props.StringProperty(subtype="FILE_PATH")

    # svg
    svg_input = bpy.props.StringProperty(subtype="FILE_PATH")
    svg_output = bpy.props.StringProperty(subtype="FILE_PATH")
    svg_qrc = bpy.props.StringProperty(default="RASTER_LIB")
    svg_size = bpy.props.FloatProperty(name="svg_size", default=2, min=0.1, step=10, precision=1)
    svg_crop_abs = bpy.props.BoolProperty(name="", default=False)
    svg_label = bpy.props.BoolProperty(name="", default=False)

    # lst
    lst_input = bpy.props.StringProperty(subtype="FILE_PATH")
    lst_output = bpy.props.StringProperty(subtype="FILE_PATH")
    lst_create = bpy.props.BoolProperty(default=True)
    lst_zip = bpy.props.BoolProperty(default=False)
    lst_show = bpy.props.EnumProperty(
        items=[
            ("relative", "relative", ""),
            ("absolute", "absolute", "")
        ])
    lst_size = bpy.props.FloatProperty(default=.01, min=0, step=10, precision=3)

    #
    mats_colorize_h = bpy.props.BoolProperty(default=True)
    mats_colorize_h_reversed = bpy.props.BoolProperty(default=False)
    mats_colorize_h_full = bpy.props.BoolProperty(default=False)
    mats_colorize_h_other_way = bpy.props.BoolProperty(default=False)
    mats_colorize_h_min = bpy.props.FloatProperty(default=0, min=0, max=1, precision=3, step=10)
    mats_colorize_h_max = bpy.props.FloatProperty(default=1, min=0, max=1, precision=3, step=10)
    mats_colorize_s = bpy.props.BoolProperty(default=True)
    mats_colorize_s_reversed = bpy.props.BoolProperty(default=False)
    mats_colorize_s_min = bpy.props.FloatProperty(default=0, min=0, max=1, precision=3, step=10)
    mats_colorize_s_max = bpy.props.FloatProperty(default=1, min=0, max=1, precision=3, step=10)
    mats_colorize_v = bpy.props.BoolProperty(default=True)
    mats_colorize_v_reversed = bpy.props.BoolProperty(default=False)
    mats_colorize_v_min = bpy.props.FloatProperty(default=0, min=0, max=1, precision=3, step=10)
    mats_colorize_v_max = bpy.props.FloatProperty(default=1, min=0, max=1, precision=3, step=10)
    mats_colorize_name = bpy.props.EnumProperty(
        items=[
            ("none", "none", ""),
            ("rename", "rename", ""),
            ("prefix", "prefix", "")
        ])
    worm_edges_width = bpy.props.FloatProperty(default=1, min=0, precision=3, step=10)
    worm_edges_height = bpy.props.FloatProperty(default=1, min=0, precision=3, step=10)
    raster_scan_index = bpy.props.EnumProperty(
        items=[
            ("0", "MAT", ""),
            ("1", "TEXTURE/NODE", ""),
            ("2", "RASTER", ""),
            ("3", "SIZE", ""),
            ("4", "STATUS", "")],
        default="3")


class CollapseSettings(bpy.types.PropertyGroup):
    cbs_files = bpy.props.BoolProperty(default=False)
    cbs_mesh_tools = bpy.props.BoolProperty(default=False)
    cbs_materials = bpy.props.BoolProperty(default=False)
    cbs_objects = bpy.props.BoolProperty(default=False)
    cbs_select_scale = bpy.props.BoolProperty(default=False)
    cbs_islands_sort = bpy.props.BoolProperty(default=False)
    cbs_connect_origins = bpy.props.BoolProperty(default=False)
    cbs_build_slope = bpy.props.BoolProperty(default=False)
    cbs_far_origin = bpy.props.BoolProperty(default=False)
    cbs_rotate_patch = bpy.props.BoolProperty(default=False)
    cbs_build_isolines = bpy.props.BoolProperty(default=False)
    cbs_obj_split = bpy.props.BoolProperty(default=False)
    cbs_surface_sew = bpy.props.BoolProperty(default=False)
    cbs_lst = bpy.props.BoolProperty(default=False)
    cbs_fencer = bpy.props.BoolProperty(default=False)
    cbs_project_edges = bpy.props.BoolProperty(default=False)
    cbs_extend_cross = bpy.props.BoolProperty(default=False)
    cbs_instances = bpy.props.BoolProperty(default=False)
    cbs_origins = bpy.props.BoolProperty(default=False)
    cbs_states_and_search = bpy.props.BoolProperty(default=False)
    cbs_builder = bpy.props.BoolProperty(default=False)
    cbs_raster_scan = bpy.props.BoolProperty(default=False)
    cbs_poke_loops = bpy.props.BoolProperty(default=False)
    cbs_bricker = bpy.props.BoolProperty(default=False)
    cbs_scribe = bpy.props.BoolProperty(default=False)
    cbs_scribe_editor = bpy.props.BoolProperty(default=False)
    cbs_worm_edges = bpy.props.BoolProperty(default=False)
    cbs_svg = bpy.props.BoolProperty(default=False)
    cbs_mats_equalize = bpy.props.BoolProperty(default=False)
    cbs_mats_colorize = bpy.props.BoolProperty(default=False)

