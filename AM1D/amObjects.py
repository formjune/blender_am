import math
import fnmatch
import functools
from abc import ABCMeta, abstractmethod
import mathutils
import bpy
import bmesh


class MeshGroup(object):
    """class for close meshes group. Used in UV placement"""

    x_max = x_min = y_max = y_min = None
    x_length = y_length = area = 0
    pivot = None
    name = ""

    def __init__(self, mesh):
        self.verts = self.face_area = 0
        self.meshes = [mesh]
        self._updateBoundaries()
        self.name = mesh.name
        self._findFaceArea()

    def addMesh(self, mesh):
        """add new mesh to group"""
        self.meshes.append(mesh)
        self._updateBoundaries()
        self.name = max(self.meshes, key=self._findVolume).name
        self._findFaceArea()

    @classmethod
    def _findVolume(cls, mesh):
        """find volume of mesh"""
        x = cls.findBoundary(mesh, max, 0) - cls.findBoundary(mesh, min, 0)
        y = cls.findBoundary(mesh, max, 1) - cls.findBoundary(mesh, min, 1)
        z = cls.findBoundary(mesh, max, 2) - cls.findBoundary(mesh, min, 2)
        return x * y * z

    def _findFaceArea(self):
        """find sum of all polygons"""
        bm = bmesh.new()
        bm.from_mesh(self.meshes[-1].data)
        self.face_area += sum([face.calc_area() for face in bm.faces])
        self.verts += len(bm.verts)

    @staticmethod
    def findBoundary(mesh, func, number, class_value=None):
        """find boundary. func - either min or max, 0 - x, 1 - y, class_value - value to compare"""
        matrix = mesh.matrix_world
        bm = bmesh.new()
        bm.from_mesh(mesh.data)

        vertex = func(bm.verts, key=lambda v: (matrix * v.co)[number])
        value = (matrix * vertex.co)[number]

        if class_value is None:
            return round(value, 3)
        else:
            return round(func((value, class_value)), 3)

    def _updateBoundaries(self):
        """find new boundaries"""

        mesh = self.meshes[-1]
        self.x_max = self.findBoundary(mesh, max, 0, self.x_max)
        self.x_min = self.findBoundary(mesh, min, 0, self.x_min)
        self.y_max = self.findBoundary(mesh, max, 1, self.y_max)
        self.y_min = self.findBoundary(mesh, min, 1, self.y_min)
        self.x_length = self.x_max - self.x_min
        self.y_length = self.y_max - self.y_min
        self.area = self.x_length * self.y_length
        self.pivot = mathutils.Vector((self.x_min, self.y_max))

    def move(self, x, y):
        """set position of pivot and move other objects"""

        x_move = x - self.pivot.x
        y_move = y - self.pivot.y
        self.pivot.x = x
        self.pivot.y = y
        for mesh in self.meshes:
            mesh.matrix_world[0][3] += x_move
            mesh.matrix_world[1][3] += y_move


class IslandsPlacementUV(bpy.types.Operator):
    """split edges in 2D projection on Z"""

    bl_idname = "mesh.am1d_islands_placement_uv"
    bl_label = "Arrange"
    bl_description = "Arrange selected objects using given setup"
    bl_options = {'REGISTER', 'UNDO'}

    @staticmethod
    def checkGroup(context, pivot, mesh_group):
        """check whether group matches or not"""
        if not context.scene.am_settings.islands_group:
            return False

        for mesh in mesh_group.meshes:
            mesh_pivot = mathutils.Vector((mesh.matrix_world[0][3], mesh.matrix_world[1][3], mesh.matrix_world[2][3]))
            if (mesh_pivot - pivot).length <= .2:
                return True
        return False

    @classmethod
    def createGroups(cls, context):

        meshes = [mesh for mesh in bpy.data.objects if mesh.select and mesh.type == "MESH"]
        mesh_groups = []
        for mesh in meshes:
            pivot = mathutils.Vector((mesh.matrix_world[0][3], mesh.matrix_world[1][3], mesh.matrix_world[2][3]))
            for mesh_group in mesh_groups:
                # check every group and create new if required
                if cls.checkGroup(context, pivot, mesh_group):
                    mesh_group.addMesh(mesh)
                    break

            else:  # create new group
                mesh_groups.append(MeshGroup(mesh))

        return mesh_groups

    @classmethod
    def simpleMethod(cls, context, x_min, y_pivot, groups):
        """first method of sorting"""

        def pack(y_pivot, islands_compact=True):
            """place all meshes in rectangle"""
            x_pivot = x_min
            y_move = 0
            for group in groups:
                island = group.x_length if islands_compact else x_step
                if x_pivot + island > x_max:
                    x_pivot = x_min
                    y_pivot -= y_move + context.scene.am_settings.islands_distance
                    y_move = 0

                group.move(x_pivot, y_pivot)
                y_move = max((y_move, group.y_length))
                x_pivot += island + context.scene.am_settings.islands_distance

        # set scale
        x_step = max([group.x_length for group in groups])  # for table only
        if context.scene.am_settings.islands_mode == "compact":
            ln = sum([group.x_length for group in groups]) + len(groups) * context.scene.am_settings.islands_distance
            x_max = x_min + ln / context.scene.am_settings.islands_split * 1.2
        else:
            in_row = math.ceil(len(groups) / context.scene.am_settings.islands_split)
            x_max = x_min + (context.scene.am_settings.islands_distance + x_step) * in_row

        pack(y_pivot, context.scene.am_settings.islands_mode == "compact")

    @classmethod
    def areaMethod(cls, context, y_max, x_pivot, groups):
        """second method of sorting. Objects must be already sorted"""

        def pack(y_min, y_max, x_pivot):
            """place all meshes in rectangle"""

            y_pivot = y_max
            x_move = 0
            for number, group in enumerate(current_groups):

                if y_pivot - group.y_length < y_min:
                    y_pivot = y_max
                    x_pivot += x_move + context.scene.am_settings.islands_distance
                    x_move = 0

                group.move(x_pivot, y_pivot)
                x_move = max(x_move, group.x_length)
                y_pivot -= group.y_length + context.scene.am_settings.islands_distance

        y_pivot = y_max
        average_area = sum([group.face_area for group in groups]) / context.scene.am_settings.islands_split
        for i in range(1, context.scene.am_settings.islands_split + 1):
            area = 0
            current_groups = []
            y_length = 0
            while (area <= average_area or i == context.scene.am_settings.islands_split) and groups:
                group = groups.pop(0)
                current_groups.append(group)
                area += group.face_area
                y_length = max(y_length, group.y_length)
            pack(y_pivot - y_length, y_pivot, x_pivot)
            y_pivot -= y_length + context.scene.am_settings.islands_distance * 10

    def execute(self, context):

        groups = self.createGroups(context)
        if not groups:
            return {"FINISHED"}

        # sort either by name or size
        if context.scene.am_settings.islands_sort == "space":
            groups.sort(key=lambda group: group.area, reverse=True)
        elif context.scene.am_settings.islands_sort == "verts":
            groups.sort(key=lambda group: group.verts, reverse=True)
        else:
            groups.sort(key=lambda group: group.name.lower())

        # place origin
        if context.scene.am_settings.islands_origin:
            x_min = y_max = x_pivot = y_pivot = 0
        else:
            x_min = min([group.x_min for group in groups])
            y_max = max([group.y_max for group in groups])
            y_pivot = max([group.y_max for group in groups])
            x_pivot = min([group.x_min for group in groups])

        if context.scene.am_settings.islands_mode in ("compact", "table"):
            self.simpleMethod(context, x_min, y_pivot, groups)
        else:
            self.areaMethod(context, y_max, x_pivot, groups)

        return {"FINISHED"}


class FarOrigin(bpy.types.Operator):
    """select objects with origin placed so far from center"""

    bl_idname = "mesh.am1d_far_origin"
    bl_label = "Far Origin"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        for mesh in context.scene.objects:
            if mesh.type != "MESH":
                continue
            bound_box = [mathutils.Vector(point) for point in mesh.bound_box]
            max_param = max([max(bound_box, key=lambda p: p[0])[0] - min(bound_box, key=lambda p: p[0])[0],
                             max(bound_box, key=lambda p: p[1])[1] - min(bound_box, key=lambda p: p[1])[1],
                             max(bound_box, key=lambda p: p[2])[2] - min(bound_box, key=lambda p: p[2])[2]])
            mesh.select = mesh.data.vertices[0].co.length > context.scene.am_settings.far_origin * max_param
        return {"FINISHED"}


class ConnectOrigins(bpy.types.Operator):
    """create line per centers"""

    bl_idname = "mesh.am1d_connect_origins"
    bl_label = "Connect Origins"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        locations = [mathutils.Vector(node.matrix_world.col[3][:3]) for node in bpy.data.objects
                     if node.type == "MESH" and node.select]

        if context.scene.am_settings.close_relative:  # search next close to current
            path = []

            next_point = mathutils.Vector((0, 0, 0)) if not context.scene.am_settings.from_active else \
                mathutils.Vector(bpy.context.scene.objects.active.matrix_world.col[3][:3])

            while locations:
                locations.sort(key=lambda point: (point - next_point).length, reverse=True)
                next_point = locations.pop()
                path.append(next_point)
        else:
            center = mathutils.Vector((0, 0, 0))  # sort by distance
            path = sorted(locations, key=lambda point: (point - center).length)

        # bpy.ops.object.mode_set(mode='OBJECT')
        mesh = bpy.data.meshes.new("mesh")
        obj = bpy.data.objects.new("MyObject", mesh)
        scene = bpy.context.scene
        scene.objects.link(obj)  # put the object into the scene (link)
        scene.objects.active = obj  # set as the active object in the scene
        obj.select = True  # select object

        bm = bmesh.new()

        for vertex in path:
            bm.verts.new(vertex)

        verts = tuple(bm.verts)
        for v1, v2 in zip(verts[:-1], verts[1:]):
            bm.edges.new((v1, v2))

        bm.to_mesh(mesh)
        bm.free()

        return {"FINISHED"}


class OriginToCenter(bpy.types.Operator):
    bl_idname = "mesh.am1d_origin_to_xy_middle"
    bl_label = "Origin To XY middle"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.mode == "OBJECT"

    def execute(self, context):
        for o in context.selected_objects:
            vector = mathutils.Vector()
            for i in o.bound_box:
                vector += mathutils.Vector(i)
            vector /= 8
            vector[2] = 0
            o.data.transform(mathutils.Matrix.Translation(-vector))
            o.location += vector
        return {'FINISHED'}


class OriginToDown(bpy.types.Operator):
    bl_idname = "mesh.am1d_origin_to_down"
    bl_label = "Origin To Down"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.mode == "OBJECT"

    def execute(self, context):
        for o in bpy.context.selected_objects:
            vector = mathutils.Vector((0, 0, min(o.bound_box, key=lambda v: v[2])[2]))
            o.data.transform(mathutils.Matrix.Translation(-vector))
            o.location += vector
        return {'FINISHED'}


class OriginToZero(bpy.types.Operator):
    bl_idname = "mesh.am1d_origin_to_zero"
    bl_label = "Origin To Zero"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.mode == "OBJECT"

    def execute(self, context):
        for o in context.selected_objects:
            vector = mathutils.Vector((0, 0, o.location[2]))
            o.data.transform(mathutils.Matrix.Translation(vector))
            o.location -= vector
        return {'FINISHED'}


class PutTriangulation(bpy.types.Operator):
    bl_idname = "mesh.am1d_put_triangulation"
    bl_label = "Put Triangulation"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.object.mode_set(mode="OBJECT")
        for mesh in bpy.context.selected_objects:
            if mesh.type != "MESH":
                continue
            bpy.context.scene.objects.active = mesh
            bpy.ops.object.modifier_add(type="TRIANGULATE")
            mesh.modifiers[-1].quad_method = "BEAUTY"
            mesh.modifiers[-1].ngon_method = "BEAUTY"
        return {"FINISHED"}


class RemoveTriangulation(bpy.types.Operator):
    bl_idname = "mesh.am1d_remove_triangulation"
    bl_label = "Remove Triangulation"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for modifier in tuple(context.active_object.modifiers):
            if modifier.type != "TRIANGULATE":
                continue
            bpy.ops.object.modifier_remove(modifier=modifier.name)
        return {"FINISHED"}


class SelectTriangulate(bpy.types.Operator):
    bl_idname = "mesh.am1d_select_triangulate"
    bl_label = "Select Triangulate"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.object.mode_set(mode="OBJECT")
        for mesh in bpy.context.scene.objects:
            mesh.select = mesh.type == "MESH" and bool(len(mesh.modifiers)) and mesh.modifiers[-1].type == "TRIANGULATE"
        return {"FINISHED"}


class InvertVisibility(bpy.types.Operator):
    bl_idname = "mesh.am1d_invert_visibility"
    bl_label = "Invert Visibility"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for node in context.scene.objects:
            node.hide = not node.hide
        if context.scene.am_settings.invert_visibility == "RESTRICT_VIEW_OFF":
            context.scene.am_settings.invert_visibility = "RESTRICT_VIEW_ON"
        else:
            context.scene.am_settings.invert_visibility = "RESTRICT_VIEW_OFF"
        return {"FINISHED"}


class InvertLayersVisibility(bpy.types.Operator):
    bl_idname = "mesh.am1d_invert_layers"
    bl_label = "Invert Layers Visibility"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        context.scene.layers = [not layer for layer in context.scene.layers]
        if context.scene.am_settings.invert_layer_visibility == "RESTRICT_VIEW_OFF":
            context.scene.am_settings.invert_layer_visibility = "RESTRICT_VIEW_ON"
        else:
            context.scene.am_settings.invert_layer_visibility = "RESTRICT_VIEW_OFF"
        return {"FINISHED"}


class ConvertRenderability(bpy.types.Operator):
    bl_idname = "mesh.am1d_convert_render"
    bl_label = "Convert Renderability"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for node in context.scene.objects:
            node.hide, node.hide_render = node.hide_render, node.hide
        if context.scene.am_settings.convert_renderability == "RESTRICT_VIEW_OFF":
            context.scene.am_settings.convert_renderability = "RESTRICT_RENDER_OFF"
        else:
            context.scene.am_settings.convert_renderability = "RESTRICT_VIEW_OFF"
        return {"FINISHED"}


class UVMapSearch(bpy.types.Operator):
    """search for meshes with wrong uv sets"""

    bl_idname = "mesh.am1d_uvmap_search"
    bl_label = "UVMap search"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for node in context.scene.objects:
            if node.type != "MESH":
                node.select = False
                continue
            uv = node.data.uv_layers
            if not uv or ("UVMap" in uv and len(uv) == 1):
                node.select = False
            else:
                node.select = True
        self.report({"INFO"}, "found %i objects " % len(context.selected_objects))
        return {"FINISHED"}



class SSRead(bpy.types.Operator):
    """save list of selected nodes into file for autocad export"""

    bl_idname = "mesh.am1d_ssreveal"
    bl_label = 'SSREVEAL'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        if "SS" in bpy.data.texts:
            text_block = bpy.data.texts["SS"]
            line = text_block.as_string().split("\n")
            for name in line:
                if name in bpy.data.objects:
                    bpy.data.objects[name].select = True
        return {"FINISHED"}


class SSName(bpy.types.Operator):
    """save list of selected nodes into file for autocad export"""

    bl_idname = "mesh.am1d_ssread"
    bl_label = 'SSREAD'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        if "SS" in bpy.data.texts:
            text_block = bpy.data.texts["SS"]
        else:
            text_block = bpy.data.texts.new(name="SS")
        text_block.clear()
        for name in bpy.data.objects:
            if name.select:
                text_block.write(name.name + "\n")
        return {"FINISHED"}


class SSFind(bpy.types.Operator):
    """save list of selected nodes into file for autocad export"""

    bl_idname = "mesh.am1d_ssfind"
    bl_label = 'SSFIND'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        if "SS" not in bpy.data.texts:
            return {"FINISHED"}
        name_list = ["*%s*" % n for n in bpy.data.texts["SS"].as_string().split("\n") if n and not n.startswith("/")]
        node_list = {}
        for node in context.scene.objects:
            node.select = False
            for v1, v2 in zip(node.layers, context.scene.layers):
                if v1 and v2:
                    node_list[node.name] = node
                    break

        for name in name_list:
            for node in fnmatch.filter(node_list.keys(), name):
                node_list[node].select = True
        return {"FINISHED"}


class SSFix(bpy.types.Operator):
    """save list of selected nodes into file for autocad export"""

    bl_idname = "mesh.am1d_ssfixnames"
    bl_label = 'SSFIXNAMES'
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        i = 0
        for name in context.scene.objects:
            try:
                name.select = False
                name.name  # just an attempt to take name
            except:
                i += 1
                name.name = "fixed name"
                name.select = True

        self.report({"INFO"}, "%i names are fixed" % i)
        return {"FINISHED"}


class SpreadByInstances(bpy.types.Operator):
    """spread mesh by instances"""

    bl_idname = "mesh.am1d_spread_by_instances"
    bl_label = "Spread By Instanced"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        meshes = [m for m in context.selected_objects if m.type == "MESH" and m != context.object]
        source = context.object
        bpy.ops.object.select_linked(type='OBDATA')
        source.select = False
        self.report({"INFO"}, "%i/%i bases used; %i created" % (len(meshes),
                                                                len(context.selected_objects) + 1,
                                                                len(meshes) * len(context.selected_objects)))
        for target in context.selected_objects:
            for mesh in meshes:
                new_mesh = mesh.copy()
                bpy.data.scenes[0].objects.link(new_mesh)
                new_mesh.layers = mesh.layers
                new_mesh.matrix_world = target.matrix_world * (source.matrix_world.inverted() * mesh.matrix_world)
        source.select = True
        return {"FINISHED"}


class SpreadByPoints(bpy.types.Operator):
    """spread mesh by instances"""

    bl_idname = "mesh.am1d_spread_by_points"
    bl_label = "Spread By Points"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        points = [m for m in context.selected_objects if m != context.object]
        for point in points:
            new_mesh = context.object.copy()
            bpy.data.scenes[0].objects.link(new_mesh)
            new_mesh.matrix_world = point.matrix_world
            new_mesh.name = "PT_" + point.name
            point.select = False
            new_mesh.select = True

        return {"FINISHED"}


class InstancesApplyRotation(bpy.types.Operator):
    """freeze rotation and spread"""

    bl_idname = "mesh.am1d_instances_apply_rotation"
    bl_label = "Instances Apply Rotation"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        # store layers and activate
        layers = [v for v in bpy.data.scenes["Scene"].layers]
        bpy.data.scenes["Scene"].layers = [True] * 20

        for source in [node for node in context.selected_objects if node.type == "MESH"]:

            mesh = source.copy()
            bpy.data.scenes[0].objects.link(mesh)
            mesh.layers = source.layers
            bpy.ops.object.select_all(action='DESELECT')
            context.scene.objects.active = mesh
            mesh.select = True
            bpy.ops.object.make_single_user(type='SELECTED_OBJECTS',
                                            object=True,
                                            obdata=True,
                                            material=False,
                                            texture=False,
                                            animation=False)
            bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

            # spread and delete previous meshes
            context.scene.objects.active = source
            bpy.ops.object.select_linked(type='OBDATA')
            source.select = False
            mesh.select = False
            for target in context.selected_objects:
                new_mesh = mesh.copy()
                bpy.data.scenes[0].objects.link(new_mesh)
                new_mesh.layers = target.layers
                new_mesh.matrix_world = target.matrix_world * (source.matrix_world.inverted() * mesh.matrix_world)
                bpy.data.objects.remove(target, True)
            bpy.data.objects.remove(source, True)

        # return layers
        bpy.data.scenes["Scene"].layers = layers
        return {"FINISHED"}


class SpreadGroup(bpy.types.Operator):
    bl_idname = "mesh.am1d_spread_group"
    bl_label = "OBJ cleanup"
    bl_description = "Distributes selected Dupligroups to the vertices of the active mesh"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        active_object = context.object
        meshes = [mesh for mesh in bpy.data.objects if mesh.select and mesh != active_object]
        for mesh in bpy.data.objects:
            mesh.select = False

        for mesh in meshes:
            duplicate = False
            for vertex in active_object.data.vertices:
                if duplicate:
                    for d_mesh in bpy.data.objects:
                        d_mesh.select = False
                    bpy.context.scene.objects.active = mesh
                    mesh.select = True
                    bpy.ops.object.duplicate_move()
                matrix_world = mesh.matrix_world
                matrix_world[0][3], matrix_world[1][3], matrix_world[2][3] = active_object.matrix_world * vertex.co
                duplicate = True
        return {"FINISHED"}


class SelectScale(bpy.types.Operator):
    """save list of selected nodes into file for autocad export"""

    bl_idname = "mesh.am1d_select_scale"
    bl_label = "Select Scale"
    bl_description = "Select objects by scale, comparable to active selected"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        # compare vectors
        if context.scene.am_settings.select_relative_type == "=":
            scale = mathutils.Vector((round(x, 2) for x in bpy.context.scene.objects.active.scale))
            if context.scene.am_settings.select_absolute_scale:
                scale = mathutils.Vector((abs(x) for x in scale))

            for node in bpy.data.objects:
                node_scale = mathutils.Vector((round(x, 2) for x in node.scale))
                if context.scene.am_settings.select_absolute_scale:
                    node_scale = mathutils.Vector((abs(x) for x in node_scale))

                # select or deselect if necessary
                if scale == node_scale:
                    node.select = True

        # multiply and then compare
        else:
            vector = bpy.context.scene.objects.active.scale
            value = round(abs(functools.reduce(lambda x, y: x * y, vector, 1)), 2)
            signs = mathutils.Vector((math.copysign(1, x) for x in vector))

            for node in bpy.data.objects:
                node_value = round(abs(functools.reduce(lambda x, y: x * y, node.scale, 1)), 2)
                node_signs = mathutils.Vector((math.copysign(1, x) for x in node.scale))

                # we compare signs vector and in case of no match continue
                if not context.scene.am_settings.select_absolute_scale and signs != node_signs:
                    continue

                if context.scene.am_settings.select_relative_type == "<" and node_value < value:
                    node.select = True

                elif context.scene.am_settings.select_relative_type == ">" and node_value > value:
                    node.select = True

                elif context.scene.am_settings.select_relative_type == "<=" and node_value <= value:
                    node.select = True

                elif context.scene.am_settings.select_relative_type == ">=" and node_value >= value:
                    node.select = True

        return {"FINISHED"}


class BatchOperatorMixin(object):
    """
    Abstract base class for batch processing objects
    Inheritors must define:
        filter_object method to define what objects to process
        process_object method to define what to do with each object
    This mixin is for use in batch_operator_factory, because most of operators
    just filter some objects and make simple things with them
    """
    __metaclass__ = ABCMeta

    bl_options = {'REGISTER', 'UNDO'}

    use_only_selected_objects = True
    context = None

    def get_use_selected_objects(self):
        return self.use_only_selected_objects

    def execute(self, context):
        """
        Template method pattern
        Must override filter_object, process_object and pre_process_objects
        """
        self.context = context
        # Select and filter objects
        self.selected_objects = context.selected_objects[:]
        self.active_object = context.active_object
        if self.get_use_selected_objects():
            self.objects = context.selected_objects
        else:
            for o in context.scene.objects:
                o.select = False
            self.objects = context.scene.objects
        self.pre_filter_objects()
        self.work_objects = [obj for obj in self.objects if self.filter_object(obj)]
        # Cache old active object. At the end we will return activeness
        old_active = bpy.context.scene.objects.active
        for obj in self.work_objects:
            # As I understood, objects for bpy.ops operators must be
            # active in most cases
            bpy.context.scene.objects.active = obj
            # Fight!
            self.process_object(obj)
        bpy.context.scene.objects.active = old_active
        self.post_process_objects()
        return {'FINISHED'}

    def filter_object(self, obj):
        return True

    @abstractmethod
    def process_object(self, obj):
        raise NotImplementedError

    def pre_filter_objects(self):
        pass

    def post_process_objects(self):
        pass


class CombineOperator(BatchOperatorMixin, bpy.types.Operator):
    bl_idname = "mesh.am1d_combine_object"
    bl_label = 'CBS Combine selected'

    use_only_selected_objects = True

    def process_object(self, obj):
        obj.location = self.active_object.location
        obj.rotation_euler = self.active_object.rotation_euler
        obj.scale = self.active_object.scale
