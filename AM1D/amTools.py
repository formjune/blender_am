import bpy
import bmesh


def safeEditMode():
    """reset bmesh for edit mode"""
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.object.mode_set(mode="EDIT")


def activeVertex(bm):
    for elem in reversed(bm.select_history):
        if isinstance(elem, bmesh.types.BMVert):
            return elem
    return None


def activeEdge(bm):
    for elem in reversed(bm.select_history):
        if isinstance(elem, bmesh.types.BMEdge):
            return elem
    return None


def activeFace(bm):
    for elem in reversed(bm.select_history):
        if isinstance(elem, bmesh.types.BMFace):
            return elem
    return None


def view3d():
    # returns first 3d view, normally we get from context
    for area in bpy.context.window.screen.areas:
        if area.type == "VIEW_3D":
            v3d = area.spaces[0]
            rv3d = v3d.region_3d
            for region in area.regions:
                if region.type == "WINDOW":
                    return region, rv3d, v3d, area
    return None, None, None, None


class BlenderParam(object):
    """wrapper for blender global options"""

    def __init__(self, name):
        self.name = name

    def __call__(self):
        return self.get()

    def get(self):
        return bpy.context.scene.am_settings.__getattribute__(self.name)

    def set(self, value):
        bpy.context.scene.am_settings.__setattribute__(self.name, value)
