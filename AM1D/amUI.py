import bpy


class CollapseMaker(object):
    """creates collapse boxes"""

    def __init__(self, main_layout, settings):
        self.main_layout = main_layout
        self.settings = settings

    def get(self, prop_name, text, layout=None):
        if layout:
            column = layout.column()
        else:
            column = self.main_layout.column()
        splitter = column.split()
        prop = self.settings.__getattribute__(prop_name)
        icon = 'DOWNARROW_HLT' if prop else 'RIGHTARROW'
        splitter.prop(self.settings, prop_name, text=text, icon=icon)
        if prop:
            box = column.column(align=True).box().column()
            column_inside = box.column(align=True)
            return column_inside


class Layout(bpy.types.Panel):
    bl_label = "AM 1D Tools"
    bl_idname = "Andrey 1D Tools"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = '1D'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        """col - main column layout. do not rewrite
        col_in - main column inside every section
        col_in_n - sub layout"""
        maker = CollapseMaker(self.layout, context.scene.am_collapse_settings)  # create collapsable columns
        s = context.scene.am_settings

        # files
        column = maker.get("cbs_files", "AM Files")
        if column:

            # scribe
            column_1 = maker.get("cbs_scribe", "Scribe", column)
            if column_1:
                column_1.prop(s, "scribe_use_obname", text="use obname")
                row_2 = column_1.row()
                row_2.prop(s, "scribe_use_offset", text="use offset")
                row_2.prop(s, "scribe_offset", text="offset")
                column_1.prop(s, "scribe_use_cursor", text="use 3d cursor")
                column_1.prop(s, "scribe_font", text="font")
                column_1.operator("mesh.am1d_scribe", text="Create")

            column_1 = maker.get("cbs_scribe_editor", "Scribe Editor", column)
            if column_1:
                column_1.prop(s, "scribe_font_editor", text="font")
                row_2 = column_1.row()
                row_2.operator("mesh.am1d_scribe_reader", text="Load")
                row_2.operator("mesh.am1d_scribe_writer", text="Save")

            # obj split
            column_1 = maker.get("cbs_obj_split", "OBJ tools", column)
            if column_1:
                column_1.prop(s, "obj_split_input", text="input file or directory")
                column_1.prop(s, "obj_split_output", text="output directory")
                column_1.prop(s, "obj_split_count", text="pieces")
                column_1.prop(s, "obj_split_remove_uvs", text="remove uvs")
                column_1.prop(s, "obj_split_remove_normals", text="remove normals")
                column_1.operator("mesh.am1d_obj_split", text="OBJ split")
                column_1.operator("mesh.am1d_obj_topology", text="OBJ topology split")
                column_1.operator("mesh.am1d_obj_cleanup", text="OBJ cleanup")

            # lst
            column_1 = maker.get("cbs_lst", "LST tools", column)
            if column_1:
                column_1.prop(s, "lst_input", text="input directory")
                column_1.prop(s, "lst_output", text="output directory")
                column_1.prop(s, "lst_create", text="create LST")
                column_1.prop(s, "lst_zip", text="expand archives (lst read only)")
                column_1.prop(s, "lst_show", text="show as")
                column_1.prop(s, "lst_size", text="min size (GB)")
                column_1.operator("mesh.am1d_lst", text="proceed")
                column_1.operator("mesh.am1d_lst_structure", text="create tree")

            column_1 = maker.get("cbs_svg", "SVG Split", column)
            if column_1:
                column_1.prop(s, "svg_input", text="input file or directory")
                column_1.prop(s, "svg_output", text="output directory")
                column_1.prop(s, "svg_size", text="max size (MB)")
                column_1.prop(s, "svg_crop_abs", text="crop absolute names")
                column_1.prop(s, "svg_label", text="slice transformed")
                column_1.operator("mesh.am1d_svg_split", text="SVG input split")
                column_1.operator("mesh.am1d_svg_merge", text="SVG output merge")
                column_1.operator("mesh.am1d_svg_parse_images", text="SVG parse images")
                column_1.operator("mesh.am1d_svg_copy_images", text="SVG copy images")
                column_1.operator("mesh.am1d_svg_icon_slicer", text="SVG Icon Slicer")

        # materials
        column = maker.get("cbs_materials", "AM Materials")
        if column:
            column_1 = maker.get("cbs_raster_scan", "Raster Scan", column)
            if column_1:
                column_1.prop(s, "raster_scan_index", text="sort by")
                column_1.operator("mesh.am1d_raster_scan", text="Raster Scan")

            column_1 = maker.get("cbs_mats_equalize", "Mats Equalize", column)
            if column_1:
                column_1.operator("mesh.am1d_mats_equalize_between", text="Between")
                column_1.operator("mesh.am1d_mats_equalize_add", text="Add")

            column_1 = maker.get("cbs_mats_colorize", "Mats Colorize", column)
            if column_1:
                row_2 = column_1.row()
                row_2.prop(s, "mats_colorize_h", text="Hue")
                row_2.prop(s, "mats_colorize_h_reversed", text="Reversed")
                row_2 = column_1.row()
                row_2.prop(s, "mats_colorize_h_full", text="Full Range")
                row_2.prop(s, "mats_colorize_h_other_way", text="Other Way")
                row_2 = column_1.row()
                row_2.prop(s, "mats_colorize_h_min", text="min")
                row_2.prop(s, "mats_colorize_h_max", text="max")
                column_1.separator()
                row_2 = column_1.row()
                row_2.prop(s, "mats_colorize_s", text="Saturation")
                row_2.prop(s, "mats_colorize_s_reversed", text="Reversed")
                row_2 = column_1.row()
                row_2.prop(s, "mats_colorize_s_min", text="min")
                row_2.prop(s, "mats_colorize_s_max", text="max")
                column_1.separator()
                row_2 = column_1.row()
                row_2.prop(s, "mats_colorize_v", text="Value")
                row_2.prop(s, "mats_colorize_v_reversed", text="Reversed")
                row_2 = column_1.row()
                row_2.prop(s, "mats_colorize_v_min", text="min")
                row_2.prop(s, "mats_colorize_v_max", text="max")
                column_1.separator()
                row_2 = column_1.row()
                row_2.prop(s, "mats_colorize_name", expand=True)
                column_1.operator("mesh.am1d_mats_colorize", text="Colorize")

            column.operator("mesh.am1d_funclone", text="Mats Funclone From Active")
            column.operator("mesh.am1d_mats_plane", text="Mats ShowPlane")
            column.operator("mesh.am1d_mats_texlist", text="Matlist Textures Compare")
            column.operator("mesh.am1d_get_materials_selected", text="Matlist Selected")
            column.operator("mesh.am1d_get_materials_all", text="Matlist Scene")

        # mesh tools
        column = maker.get("cbs_mesh_tools", "AM Meshtools")
        if column:
            column_1 = maker.get("cbs_extend_cross", "Extend Cross", column)
            if column_1:
                column_1.operator("mesh.am1d_extend_cross", text="extend cross")
                column_1.operator("mesh.am1d_corner_cross", text="corner cross")
                column_1.prop(s, "overlap")

            column_1 = maker.get("cbs_project_edges", "Project edges to faces", column)
            if column_1:
                column_1.prop(s, "project_edges_active", text="use active edge")
                row_2 = column_1.row(align=True)
                row_2.alignment = "EXPAND"
                row_2.operator("mesh.am1d_project_edges_store", text="store")
                row_2.operator("mesh.am1d_project_edges", text="project")

            column_1 = maker.get("cbs_rotate_patch", "Spin Profile", column)
            if column_1:
                column_1.prop(s, "rotate_patch_number", text="side number")
                column_1.prop(s, "rotate_patch_closed", text="close profile")
                row_2 = column_1.row(align=True)
                row_2.alignment = "EXPAND"
                row_2.operator("mesh.am1d_rotate_patch", text="X").side = 0
                row_2.operator("mesh.am1d_rotate_patch", text="Y").side = 1
                row_2.operator("mesh.am1d_rotate_patch", text="Z").side = 2

            column_1 = maker.get("cbs_surface_sew", "Surface Sew", column)
            if column_1:
                column_1.prop(s, "surface_sew_selected", text="Selection")
                row_2 = column_1.row(align=True)
                row_2.alignment = "EXPAND"
                row_2.prop(s, "surface_sew_is_radius", text="radius")
                if context.scene.am_settings.surface_sew_is_radius:
                    row_2.prop(s, "surface_sew_radius", text="")
                column_1.prop(s, "surface_sew_boundary", text="Boundary")
                column_1.prop(s, "surface_sew_select_found", text="select all found vertices")
                column_1.prop(s, "surface_sew_quick", text="quick faces")
                column_1.operator("mesh.am1d_surface_sew", text="Surface Sew To Active")
                column_1.operator("mesh.am1d_surface_sew_select_by_radius", text="Select By Radius")

            column_1 = maker.get("cbs_poke_loops", "Poke Loops", column)
            if column_1:
                column_1.prop(s, "poke_loops_radius", text="radius")
                column_1.prop(s, "poke_loops_threshold", text="threshold")
                row_2 = column_1.row()
                row_2.prop(s, "poke_loops_project", text="project", expand=True)
                column_1.prop(s, "poke_loops_once", text="project vertex once")
                column_1.prop(s, "poke_loops_group", text="group vertices")
                column_1.operator("mesh.am1d_poke_loops", text="apply")

            # surface levelset
            row_1 = column.row(align=True)
            row_1.alignment = "EXPAND"
            row_1.operator("mesh.am1d_surface_levelset", text="Surface Levelset")
            icon = "EDITMODE_HLT" if context.scene.am_settings.surface_levelset else "MESH_CUBE"
            row_1.prop(s, "surface_levelset", icon=icon, icon_only=True)
            column.operator("mesh.am1d_dissect", text="Dissect")
            column.operator("mesh.am1d_map_cut", text="Map Cut")
            column.operator("mesh.am1d_seam_from_bevel", text="Seam from Bevel")
            column.operator("mesh.am1d_bevel_from_seam", text="Bevel from Seam")

        # objects
        column = maker.get("cbs_objects", "AM Object")
        if column:

            column_1 = maker.get("cbs_islands_sort", "Asset Sort", column)
            if column_1:
                column_1.prop(s, "islands_sort", text="sort")
                column_1.prop(s, "islands_mode", text="mode")
                column_1.prop(s, "islands_distance", text="distance")
                column_1.prop(s, "islands_split", text="split")
                row_2 = column_1.row(align=True)
                row_2.alignment = "EXPAND"
                row_2.prop(s, "islands_origin", text="from origin")
                row_2.prop(s, "islands_group", text="group close")
                column_1.operator("mesh.am1d_islands_placement_uv", text="Arrange")

            column_1 = maker.get("cbs_origins", "Origins", column)
            if column_1:

                # far origin
                column_2 = maker.get("cbs_far_origin", "Far Origin", column_1)
                if column_2:
                    column_2.prop(s, "far_origin", text="multiplier")
                    column_2.operator("mesh.am1d_far_origin", text="Far Origin")

                column_2 = maker.get("cbs_connect_origins", "Connect Origins", column_1)
                if column_2:
                    column_2.operator("mesh.am1d_connect_origins", text="connect origins")
                    row_3 = column_2.row(align=True)
                    row_3.alignment = "EXPAND"
                    row_3.prop(s, "close_relative")
                    row_3.prop(s, "from_active", text="start from active")

                column_1.operator("mesh.am1d_origin_to_xy_middle", text="Origin to XY middle")
                column_1.operator("mesh.am1d_origin_to_down", text="Origin To Down")
                column_1.operator("mesh.am1d_origin_to_zero", text="Origin To Zero")

            column_1 = maker.get("cbs_instances", "Instances", column)
            if column_1:
                column_1.operator("mesh.am1d_spread_by_instances", text="Spread By Instances")
                column_1.operator("mesh.am1d_spread_by_points", text="Spread By Points")
                column_1.operator("mesh.am1d_spread_group", text="Spread Group")
                column_1.operator("mesh.am1d_instances_apply_rotation", text="Instances Apply Rotation")

            column_1 = maker.get("cbs_states_and_search", "States and search", column)
            if column_1:
                # select scale
                column_2 = maker.get("cbs_select_scale", "Select Scale", column_1)
                if column_2:
                    row_3 = column_2.row(align=True)
                    row_3.alignment = "EXPAND"
                    row_3.operator("mesh.am1d_select_scale", text="Select scale")
                    row_3.prop(s, "select_absolute_scale", icon="MESH_CUBE", icon_only=True)
                    column_2.prop(s, "select_relative_type", text="Operator")

                column_1.operator("mesh.am1d_invert_visibility", text="Invert Visibility", icon=s.invert_visibility)
                column_1.operator("mesh.am1d_convert_render", text="Convert Renderability", icon=s.convert_render)
                column_1.operator("mesh.am1d_invert_layers", text="Invert Layer Visibility", icon=s.invert_l_visibility)
                column_1.operator("mesh.am1d_uvmap_search", text="UVMap search")
                column_1.operator("mesh.am1d_ssread", text="SSREAD")
                column_1.operator("mesh.am1d_ssreveal", text="SSREVEAL")
                column_1.operator("mesh.am1d_ssfind", text="SSFIND")
                column_1.operator("mesh.am1d_ssfixnames", text="SSFIXNAMES")
            column.operator("mesh.am1d_combine_object", text="CBS Combine selected")
            column.operator("mesh.am1d_put_triangulation", text="Put Triangulation")
            column.operator("mesh.am1d_remove_triangulation", text="Remove Triangulation")
            column.operator("mesh.am1d_select_triangulate", text="Select Triangulate")

        column = maker.get("cbs_builder", "AM Builders")
        if column:

            column_1 = maker.get("cbs_worm_edges", "Worm Edges", column)
            if column_1:
                column_1.prop(s, "worm_edges_height", text="height")
                column_1.prop(s, "worm_edges_width", text="width")
                column_1.operator("mesh.am1d_worm_edges", "Apply")

            column_1 = maker.get("cbs_build_slope", "Build Slope", column)
            if column_1:
                column_1.prop(s, "slope_length", text="length")
                column_1.prop(s, "slope_mode", text="longest slope")
                column_1.prop(s, "slope_center", text="from center")
                column_1.prop(s, "slope_separated", text="separated edge from top")
                column_1.operator("mesh.am1d_build_slope", text="build slope")

            column_1 = maker.get("cbs_build_isolines", "Build Isolines", column)
            if column_1:
                column_1.prop(s, "isolines_main", text="main step")
                column_1.prop(s, "isolines_step", text="step")
                column_1.operator("mesh.am1d_build_isolines", text="build isolines")

            column_1 = maker.get("cbs_fencer", "Fencer", column)
            if column_1:
                column_1.prop(s, "fencer_dist", text="dist")
                column_1.prop(s, "fencer_center", text="center")
                row_2 = column_1.row()
                row_2.label("aim")
                row_2.prop(s, "fencer_aim", text="aim", expand=True)
                row_2 = column_1.row()
                row_2.label("up")
                row_2.prop(s, "fencer_up", text="up", expand=True)
                column_1.prop(s, "fencer_no_z", text="flat rotation")
                column_1.operator("mesh.am1d_fencer", text="apply")

            column_1 = maker.get("cbs_bricker", "Bricker", column)
            if column_1:
                column.prop(s, "bricker_step", text="step")
                column.prop(s, "bricker_height", text="brick height")
                column.prop(s, "bricker_thickness", text="thickness")
                column.prop(s, "bricker_offset", text="offset")
                column.prop(s, "bricker_modifier", text="solidify modifier")
                column.prop(s, "bricker_clean", text="clean mesh")
                column.prop(s, "bricker_double", text="double side thickness")
                column.operator("mesh.am1d_brick_liner", text="create")
                column.operator("mesh.am1d_brick_checker", text="check")
