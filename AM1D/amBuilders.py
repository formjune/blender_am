import collections
import math
import functools
import bpy
import bmesh
import mathutils
from .amTools import *


class BuildSlope(bpy.types.Operator):
    """add ++ into name of upper objects"""

    bl_idname = "mesh.am1d_build_slope"
    bl_label = "Build Slope"
    bl_description = "Build mesh lines representing polygon's slope"
    bl_options = {'REGISTER', 'UNDO'}
    vertex_cluster = collections.namedtuple("TriangleSplit", "vertex world")
    world_up = mathutils.Vector((0, 0, 1))

    def execute(self, context):

        def worldNormal():
            """get world normal in [0, 1] range. 0 - horizontal"""
            return abs((matrix * face.normal).angle(self.world_up) / math.pi - 0.5) * 2

        obj = bpy.context.object
        me = obj.data
        bm = bmesh.from_edit_mesh(me)
        matrix = obj.matrix_world
        matrix_back = matrix.inverted()
        counter = 0

        for face in [f for f in bm.faces if f.select] or bm.faces:
            if len(face.verts) < 3 or not (1e-3 <= worldNormal() <= .999):
                face.select = False
                continue

            face.select = True
            top = max(face.verts, key=lambda v: (matrix * v.co).z)
            edge_1, edge_2 = [edge for edge in top.link_edges if face in edge.link_faces]
            vertices = edge_1.other_vert(top), edge_2.other_vert(top)
            bottom, middle = sorted(vertices, key=lambda v: (matrix * v.co).z)
            a = matrix * top.co
            b = matrix * middle.co
            c = matrix * bottom.co
            center = functools.reduce(lambda x, y: matrix * y.co + x, face.verts, mathutils.Vector()) / len(face.verts)
            if context.scene.am_settings.slope_mode == "constant":
                vector_length = context.scene.am_settings.slope_length
            elif context.scene.am_settings.slope_mode == "vertical":
                vector_length = context.scene.am_settings.slope_length * (1 - worldNormal())
            else:
                vector_length = context.scene.am_settings.slope_length * worldNormal()

            try:
                d = a + (c - a) * ((a - b).z / (a - c).z)
                perpen = (b - d).cross(matrix * (matrix_back * d + face.normal) - d)
                point = mathutils.geometry.intersect_line_line(a, a + perpen, c, b)[1]
                if context.scene.am_settings.slope_center:  # from center as separated
                    result = center + (point - a).normalized() * vector_length
                    bm.edges.new((bm.verts.new(matrix_back * result), bm.verts.new(matrix_back * center)))
                elif context.scene.am_settings.slope_separated:  # from top separated
                    result = a + (point - a).normalized() * vector_length
                    bm.edges.new((bm.verts.new(matrix_back * result), bm.verts.new(matrix_back * a)))
                else:  # from top but connected to face
                    result = a + (point - a).normalized() * vector_length
                    bm.edges.new((bm.verts.new(matrix_back * result), top))
                counter += 1

            except (ZeroDivisionError, TypeError) as e:
                pass

        bmesh.update_edit_mesh(me)
        self.report({"INFO"}, "%i slopes" % counter)

        return {"FINISHED"}


class BuildIsolines(bpy.types.Operator):
    bl_idname = "mesh.am1d_build_isolines"
    bl_label = "Build Isolines"
    bl_description = "Build horizontal Isolines on Terrain starting from scenes zero"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        mesh = context.scene.objects.active
        matrix = mesh.matrix_world
        vector = mathutils.Vector((0, 0, 1))
        step = context.scene.am_settings.isolines_step
        main_step = context.scene.am_settings.isolines_main
        bpy.ops.object.mode_set(mode='EDIT')
        bm = bmesh.from_edit_mesh(mesh.data)
        if step == 0 or main_step == 0:
            return {"FINISHED"}

        # find bottom vertex
        bottom = 0.0
        mesh_bottom = min([(matrix * v.co) for v in bm.verts], key=lambda v: v.z).z
        while bottom > mesh_bottom:
            bottom -= step

        # get information about edges
        edge_data = {}
        edge_maker = collections.namedtuple("edge", "bottom top proportion flat boundary index vector")
        for edge in bm.edges:
            vertex_1 = matrix * edge.verts[0].co
            vertex_2 = matrix * edge.verts[1].co
            if vertex_1.z > vertex_2.z:
                vertex_2, vertex_1 = vertex_1, vertex_2

            proportion = vertex_2.z - vertex_1.z
            flat = vertex_2.z == vertex_1.z
            boundary = edge.is_boundary
            vector_edge = vertex_2 - vertex_1
            edge_data[edge] = edge_maker(vertex_1, vertex_2, proportion, flat, boundary, edge.index, vector_edge)

        # walk through every face
        new_edges = []
        new_edges_main = []
        visited_edges = set()
        for face in bm.faces:
            normal = matrix * face.normal
            top_vertex = max([matrix * point.co for point in face.verts], key=lambda v: v.z).z
            current = bottom

            # if face is non planar
            if 0 < normal.angle(vector) < (math.pi * 2):
                while current <= top_vertex:
                    # check for horizontal line
                    planar_edge = False
                    for edge in face.edges:
                        edge = edge_data[edge]
                        if edge.index in visited_edges:  # skip repeating edge
                            continue
                        if edge.flat and edge.bottom == current:
                            planar_edge = True
                            if round(abs(current) % main_step, 1) == 0.0:
                                new_edges_main.append([edge.bottom, edge.top])
                            else:
                                new_edges.append([edge.bottom, edge.top])
                            visited_edges.add(edge.index)
                            break

                    # search for non planar edges
                    if not planar_edge:
                        found_point = []
                        for edge in face.edges:
                            edge = edge_data[edge]
                            if not (edge.bottom.z <= current <= edge.top.z) or edge.flat:
                                continue
                            percent = (current - edge.bottom.z) / edge.proportion
                            found_point.append(edge.bottom + edge.vector * percent)
                        if len(found_point) >= 2:
                            if round(abs(current) % main_step, 1) == 0.0:
                                new_edges_main.append(found_point[:2])
                            else:
                                new_edges.append(found_point[:2])

                    current += step

            else:  # create planar
                while current <= top_vertex:
                    for edge in face.edges:
                        edge = edge_data[edge]
                        if edge.index in visited_edges:  # skip repeating edge
                            continue
                        if edge.boundary and edge.bottom == current:
                            if round(abs(current) % main_step, 1) == 0.0:
                                new_edges_main.append([edge.bottom, edge.top])
                            else:
                                new_edges.append([edge.bottom, edge.top])
                            visited_edges.add(edge.index)
                            break

                    current += step

        bmesh.update_edit_mesh(mesh.data)
        mesh.select = False
        bpy.ops.object.mode_set(mode='OBJECT')

        # create new mesh
        for edges, name in [[new_edges, "_isolines"], [new_edges_main, "_isolines_main"]]:
            mesh_new = bpy.data.meshes.new("mesh")  # add a new mesh
            obj = bpy.data.objects.new(mesh.name + name, mesh_new)
            obj.select = True
            matrix_back = obj.matrix_world.inverted()
            context.scene.objects.link(obj)  # put the object into the scene (link)
            context.scene.objects.active = obj  # set as the active object in the scene
            bpy.ops.object.mode_set(mode='EDIT')
            bm = bmesh.from_edit_mesh(obj.data)
            for v1, v2 in edges:
                bm.edges.new((bm.verts.new(matrix_back * v1), bm.verts.new(matrix_back * v2)))
            bmesh.update_edit_mesh(obj.data)
            bpy.ops.object.mode_set(mode='OBJECT')

        return {"FINISHED"}


class Fencer(bpy.types.Operator):
    """freeze rotation and spread"""

    bl_idname = "mesh.am1d_fencer"
    bl_label = "Fencer"
    bl_options = {'REGISTER', 'UNDO'}
    vectors = {"x": mathutils.Vector((1, 0, 0)),
               "y": mathutils.Vector((0, 1, 0)),
               "z": mathutils.Vector((0, 0, 1)),
               "-x": mathutils.Vector((-1, 0, 0)),
               "-y": mathutils.Vector((0, -1, 0)),
               "-z": mathutils.Vector((0, 0, -1))
               }
    UP = mathutils.Vector((0, 0, 1))

    @staticmethod
    def createMatrix(p, aim, up):
        """build matrix. aim - X, up - Y"""
        aim = aim.normalized()
        vec = aim.cross(up).normalized()
        up = vec.cross(aim).normalized()
        return mathutils.Matrix(
            [
                [aim.x, up.x, vec.x, p.x],
                [aim.y, up.y, vec.y, p.y],
                [aim.z, up.z, vec.z, p.z],
                [0, 0, 0, 1]
            ]
        )

    def execute(self, context):
        if context.scene.am_settings.fencer_aim[-1] == context.scene.am_settings.fencer_up[-1]:
            self.report({'ERROR'}, "aim and up can't be parallel")
            return {"FINISHED"}
        step_size = context.scene.am_settings.fencer_dist
        center = context.scene.am_settings.fencer_center
        aim_vector = self.vectors[context.scene.am_settings.fencer_aim]
        up_vector = self.vectors[context.scene.am_settings.fencer_up]
        no_z_rotate = context.scene.am_settings.fencer_no_z

        bpy.ops.object.mode_set(mode='EDIT')
        main_mesh = context.active_object
        selected = [node for node in context.selected_objects if node != main_mesh]
        mesh_edit = bmesh.from_edit_mesh(main_mesh.data)
        start_vertices = {v for v in mesh_edit.verts if len(v.link_edges) == 1}
        selected_vertices = {v for v in start_vertices if v.select}
        unused_vertices = set(mesh_edit.verts)
        chains = []

        while start_vertices:
            vertex = start_vertices.pop()
            if not vertex.link_edges:
                continue
            edge = vertex.link_edges[0]
            chain = [vertex]
            while True:
                vertex = edge.other_vert(vertex)
                chain.append(vertex)
                if len(vertex.link_edges) == 1:  # found end chain
                    if chain[-1] in selected_vertices:
                        chain.reverse()
                    start_vertices.discard(vertex)
                    break
                elif len(vertex.link_edges) == 2:  # found next vertex
                    edge = vertex.link_edges[0] if vertex.link_edges[1] == edge else vertex.link_edges[1]
                else:  # something wrong
                    chain.clear()
                    break

            if chain:
                for vertex in chain:
                    unused_vertices.discard(vertex)
                chains.append(chain)

        # find loops
        if unused_vertices:
            for v in mesh_edit.verts:
                v.select = v in unused_vertices
            self.report({'ERROR'}, "wrong vertices")
            return {"FINISHED"}

        for chain in chains:
            chain = [main_mesh.matrix_world * vertex.co for vertex in chain]
            current_pos = 0.0
            for i in range(len(chain) - 1):
                point = chain[i]
                vector = chain[i + 1] - point
                while current_pos < vector.length:
                    position = point + vector.normalized() * current_pos
                    current_pos += step_size
                    if no_z_rotate:
                        matrix_out = self.createMatrix(position, mathutils.Vector((vector.x, vector.y, 0)), self.UP)
                    else:
                        matrix_out = self.createMatrix(position, vector, self.UP)

                    for mesh in selected:
                        new_mesh = mesh.copy()
                        context.scene.objects.link(new_mesh)
                        if center == "pivot 3d":
                            p = bpy.context.scene.cursor_location
                        else:
                            p = mesh.matrix_world.translation
                        matrix_in = self.createMatrix(p, aim_vector, up_vector)
                        new_mesh.matrix_world = matrix_out * (matrix_in.inverted() * mesh.matrix_world)
                current_pos -= vector.length
        bpy.ops.object.mode_set(mode="OBJECT")
        return {"FINISHED"}


class WormEdges(bpy.types.Operator):
    bl_idname = "mesh.am1d_worm_edges"
    bl_label = "Offset Edges"
    bl_options = {'REGISTER', 'UNDO'}

    width = BlenderParam("worm_edges_width")
    height = BlenderParam("worm_edges_height")

    @staticmethod
    def findPath(edge, vertex, left_edges, reverse=False):
        vertices = [vertex.co]
        while True:
            if len(vertex.link_edges) == 1:
                break
            edge = vertex.link_edges[0] if edge == vertex.link_edges[1] else vertex.link_edges[1]
            if edge not in left_edges:
                break
            left_edges.remove(edge)
            vertex = edge.other_vert(vertex)
            vertices.append(vertex.co)
        if reverse:
            vertices.reverse()
        return vertices

    @classmethod
    def createVerts(cls, up, vtx, vtx_prev, vtx_next):
        vector = ((vtx_next - vtx).cross(up) + (vtx - vtx_prev).cross(up)).normalized()
        return vtx + vector * cls.width(), vtx + up * cls.height(), vtx - vector * cls.width()

    def execute(self, context):
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(context.active_object.data)
        left_edges = set(bm.edges)
        bm.verts.ensure_lookup_table()
        up = (bm.verts[1].co - bm.verts[0].co).cross(bm.verts[-1].co - bm.verts[0].co).normalized()
        while left_edges:
            points = []
            edge = left_edges.pop()
            path = self.findPath(edge, edge.verts[1], left_edges, True) + self.findPath(edge, edge.verts[0], left_edges)
            points.append(self.createVerts(up, path[0], path[0] + (path[0] - path[1]), path[1]))
            for i in range(1, len(path) - 1):
                points.append(self.createVerts(up, path[i], path[i - 1], path[i + 1]))
            points.append(self.createVerts(up, path[-1], path[-2], path[-1] + (path[-1] - path[-2])))

            # create
            vertices = []
            for row in points:
                vertices.append([bm.verts.new(p) for p in row])
            for i in range(len(vertices) - 1):
                bm.faces.new((vertices[i][0], vertices[i + 1][0], vertices[i + 1][1], vertices[i][1]))
                bm.faces.new((vertices[i][1], vertices[i + 1][1], vertices[i + 1][2], vertices[i][2]))
                bm.faces.new((vertices[i][2], vertices[i + 1][2], vertices[i + 1][0], vertices[i][0]))

            # create ends
            end = bm.verts.new(path[0] + (path[0] - path[1]))
            bm.faces.new((end, vertices[0][0], vertices[0][1]))
            bm.faces.new((end, vertices[0][1], vertices[0][2]))
            bm.faces.new((end, vertices[0][2], vertices[0][0]))

            end = bm.verts.new(path[-1] + (path[-1] - path[-2]))
            bm.faces.new((end, vertices[-1][1], vertices[-1][0]))
            bm.faces.new((end, vertices[-1][2], vertices[-1][1]))
            bm.faces.new((end, vertices[-1][0], vertices[-1][2]))

        bmesh.update_edit_mesh(context.active_object.data)
        bm.free()
        bpy.ops.object.mode_set(mode="OBJECT")
        return {"FINISHED"}
