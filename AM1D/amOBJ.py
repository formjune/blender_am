import os
import math
import bpy


class ObjBlock(object):

    def __init__(self, obj_file, element_list, always_none=False):
        self.elements_list = element_list  # data lines
        self.elements_dict = {}  # absolute -> relative index
        self.obj_file = obj_file
        self.always_none = always_none

    def __getitem__(self, index):
        """return new index. int or empty string"""
        if index is None or self.always_none:  # remove normals or uvs
            return ""

        if index not in self.elements_dict:
            self.elements_dict[index] = len(self.elements_dict) + 1  # register index
            self.obj_file.write(self.elements_list[index])  # write into file
        return self.elements_dict[index]


class ObjOperator(bpy.types.Operator):
    bl_idname = "mesh.am1d_obj_operator"
    bl_label = "Obj Operator"
    bl_options = {'INTERNAL'}

    def execute(self, context):
        obj_dir = context.scene.am_settings.obj_split_input
        output_dir = context.scene.am_settings.obj_split_output
        uvs = context.scene.am_settings.obj_split_remove_uvs
        normals = context.scene.am_settings.obj_split_remove_normals
        split_number = context.scene.am_settings.obj_split_count

        if os.path.isfile(obj_dir):
            self.run(obj_dir, output_dir or os.path.dirname(obj_dir), split_number, uvs, normals)

        elif os.path.isdir(obj_dir):
            for obj_file in os.listdir(obj_dir):
                if os.path.splitext(obj_file)[1].lower() == ".obj":
                    self.run(os.path.join(obj_dir, obj_file), output_dir or obj_dir, split_number, uvs, normals)

        return {"FINISHED"}

    @classmethod
    def run(cls, obj_name, output_dir, split_number, remove_uvs, remove_normals):
        """execute function"""
        pass

    @staticmethod
    def getOutputName(obj_name, output_dir):
        obj_name = os.path.split(obj_name)[1]  # local name
        obj_name = os.path.splitext(obj_name)[0]  # remove ext
        return os.path.join(output_dir, obj_name + "_%s.obj")

    @staticmethod
    def parseFace(face_line):
        """[[v, vt, vn], ...]"""
        result = []
        for face_block in face_line.split():
            fb = face_block.split("/")
            if len(fb) == 3:  # v/vt/vn, v//vn
                result.append([int(fb[0]), int(fb[1]) if fb[1] else None, int(fb[2]) if fb[2] else None])

            elif len(fb) == 2:  # v/vt
                result.append([int(fb[0]), int(fb[1]) if fb[1] else None, None])

            elif len(fb) == 1 and fb[0].isdigit():  # v
                result.append([int(fb[0]), None, None])

        return result

    @staticmethod
    def parseLine(line_line):
        """[v, v]"""
        line = line_line.split()
        return int(line[1]), int(line[2])

    @staticmethod
    def scanFullFile(obj_name, remove_uvs=False, remove_normals=False):
        """scan full file to grab all elements"""
        vertices = [""]
        uvs = [""]
        normals = [""]
        mtllib = ""
        object_count = 0
        face_count = 0

        # store all vertices and count number of objects
        file = open(obj_name, encoding="utf-8")
        while True:
            line = file.readline()
            if not line:
                break
            elif line[:2] == "v ":  # vertex
                vertices.append(line)
            elif line[:2] == "vt" and not remove_uvs:  # UV
                uvs.append(line)
            elif line[:2] == "vn" and not remove_normals:  # normal
                normals.append(line)
            elif line[:2] == "o " or line[:2] == "g ":  # announce new object
                object_count += 1
            elif line[:6] == "mtllib":
                mtllib += line
            elif line[0] == "f":
                face_count += 1
        file.close()
        return vertices, uvs, normals, mtllib, object_count, face_count

    @staticmethod
    def buildFace(v, vt, vn):
        if not vn and not vt:
            return "%s " % v
        elif not vt:
            return "%s//%s " % (v, vn)
        elif not vn:
            return "%s/%s " % (v, vt)
        return "%s/%s/%s " % (v, vt, vn)


class ObjCleanup(ObjOperator):
    bl_idname = "mesh.am1d_obj_cleanup"
    bl_label = "OBJ cleanup"
    bl_description = "Creates copy of input OBJ, cleaned from UV or Normals data"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def run(cls, obj_name, output_dir, *args):
        obj_out = cls.getOutputName(obj_name, output_dir) % "mod"
        obj_in_file = open(obj_name, encoding="utf-8")
        obj_out_file = open(obj_out, "w", encoding="utf-8")
        while True:
            line = obj_in_file.readline()
            if not line:
                break
            if line[:2] == "v " or line[0] == "g" or line[0] == "o" or line[0] == "l":
                obj_out_file.write(line)
            elif line[0] == "f":
                obj_out_file.write(" ".join([x.split("/")[0] for x in line.rstrip().split()]) + "\n")
        obj_in_file.close()
        obj_out_file.close()
        return


class ObjSplit(ObjOperator):
    bl_idname = "mesh.am1d_obj_split"
    bl_label = "OBJ split"
    bl_description = "splits input OBJ file to output directory"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def run(cls, obj_name, output_dir, split_number, remove_uvs, remove_normals):
        vertices, uvs, normals, mat, object_count, face_count = cls.scanFullFile(obj_name, remove_uvs, remove_normals)

        # split
        obj_per_file = math.ceil(object_count / split_number)
        out_name = cls.getOutputName(obj_name, output_dir)
        file = open(obj_name, encoding="utf-8")
        is_end = False
        header = [mat, "", "", ""]  # object, material, smooth

        for file_number in range(split_number):
            if is_end:
                break
            obj_out = open(out_name % file_number, "w", encoding="utf-8")
            obj_out.write("".join(header))
            v_dict = ObjBlock(obj_out, vertices)
            vt_dict = ObjBlock(obj_out, uvs, always_none=remove_uvs)
            vn_dict = ObjBlock(obj_out, normals, always_none=remove_normals)
            i = 0  # num of objects per file
            while i <= obj_per_file:
                line = file.readline()
                if not line:
                    is_end = True
                    break

                elif line[0] == "f":
                    data_line = "f "
                    for data_v, data_vt, data_vn in cls.parseFace(line.rstrip()):
                        data_line += cls.buildFace(v_dict[data_v], vt_dict[data_vt], vn_dict[data_vn])
                    obj_out.write(data_line + "\n")

                elif line[0] == "l":
                    v1, v2 = cls.parseLine(line.rstrip())
                    obj_out.write("l %s %s \n" % (v_dict[v1], v_dict[v2]))

                elif line[0] == "g" or line[0] == "o":  # mark as last object
                    i += 1
                    if i <= obj_per_file:
                        obj_out.write(line)
                        header[1] = ""
                    else:
                        header[1] = line

                elif line[:6] == "usemtl":  # mark as last material
                    header[2] = line
                    obj_out.write(line)

                elif line[0] == "s":  # mark as last material
                    header[3] = line
                    obj_out.write(line)

                elif line[0] == "v" or line[0] == "#" or line[:6] == "mtllib":  # skip vertex data
                    continue

                else:
                    obj_out.write(line)
            obj_out.close()
        file.close()


class ObjTopologySplit(ObjOperator):
    bl_idname = "mesh.am1d_obj_topology"
    bl_label = "OBJ topology split"
    bl_description = "splits input OBJ file to output directory"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def run(cls, obj_name, output_dir, split_number, *args):
        vertices, uvs, normals, mat, object_count, face_count = cls.scanFullFile(obj_name)
        faces_per_file = math.ceil(face_count / split_number)
        file = open(obj_name, encoding="utf8")
        file_num = 0
        out_name = cls.getOutputName(obj_name, output_dir)
        header = [mat, "", "", ""]
        while face_count > 0:
            obj_out = open(out_name % file_num, "w", encoding="utf-8")
            obj_out.write("".join(header))
            v_dict = ObjBlock(obj_out, vertices)
            vt_dict = ObjBlock(obj_out, uvs)
            vn_dict = ObjBlock(obj_out, normals)
            i = 0
            while i < faces_per_file:
                line = file.readline()
                if not line:
                    face_count = 0
                    break

                if line[0] == "f":  # proceed face
                    data_line = "f "
                    for data_v, data_vt, data_vn in cls.parseFace(line.rstrip()):
                        data_line += cls.buildFace(v_dict[data_v], vt_dict[data_vt], vn_dict[data_vn])
                    obj_out.write(data_line + "\n")
                    i += 1

                elif line[0] == "l":  # proceed line
                    v1, v2 = cls.parseLine(line.rstrip())
                    obj_out.write("l %s %s \n" % (v_dict[v1], v_dict[v2]))
                    i += 1

                elif line[:2] == "o " or line[:2] == "g ":  # mark as last object
                    header[1] = line
                    obj_out.write(line)

                elif line[:6] == "usemtl":  # mark as last material
                    header[2] = line
                    obj_out.write(line)

                elif line[0] == "s":  # mark as last material
                    header[3] = line
                    obj_out.write(line)

            obj_out.close()
            face_count -= i
            file_num += 1


