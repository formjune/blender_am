import os
import bmesh
import bpy
import datetime


class LSTUnit(object):

    def __init__(self, long_name=""):
        self.long_name = long_name.replace("\\", "/").rstrip("/")
        self.dirs = self.long_name.split("/")
        self.name = self.dirs[-1]
        self.r_size = 0  # relative size
        self.a_size = 0  # absolute size
        self.r_num = 0  # files number
        self.a_num = 0  # filees number
        self.array = []  # array of children
        self.dict = {}  # dict of children
        self.zip = os.path.splitext(self.name)[1].lower() in (".zip", ".rar", ".7z")
        self.sort = self.r_size, self.a_size, self.r_num, self.a_num, self.name


class LSTTree(object):

    def __init__(self):
        self.tree = LSTUnit()
        self.last_item = self.tree

    def addFolder(self, long_name, size=0, files_num=0, zip_as_one=False):
        long_name = long_name.replace("\\", "/").rstrip("/")
        dirs = long_name.split("/")
        unit = self.tree
        for i, name in enumerate(dirs, 1):  # reach last branch
            if name not in unit.dict:  # create path
                new_unit = LSTUnit("/".join(dirs[:i]))
                unit.dict[name] = new_unit
                unit.array.append(new_unit)
            unit = unit.dict[name]
            if zip_as_one and unit.zip:
                break

        self.last_item = unit
        self.addFile(size, files_num)

    def addFile(self, size=0, files_num=0):
        if not self.last_item.long_name:  # files from root
            return
        size /= 2 ** 30
        self.last_item.r_size += size
        self.last_item.r_num += files_num
        parent = self.tree

        for name in self.last_item.dirs:
            parent = parent.dict[name]
            parent.a_size += size
            parent.a_num += files_num

    def getPlainList(self):

        def proceed(item):

            if item.name:  # and item.__getattribute__(filter_method) > .1:
                plain_array.append(item)

            item.array.sort(key=lambda x: x.name)
            for next_item in item.array:
                proceed(next_item)

        plain_array = []
        proceed(self.tree)
        return plain_array


class LSTParser(object):

    @staticmethod
    def getTime(path):
        try:
            stat = os.stat(path)
            data = datetime.datetime.fromtimestamp(stat.st_mtime)
            date = "%i.%i.%i" % (data.year, data.month, data.day)
            hour = "%i:%i.%i" % (data.hour, data.minute, data.second)
            return date, hour
        except OSError:
            return "1980.1.1", "0.0:00"

    @classmethod
    def readFromFile(cls, file_in, zip_as_one=False):
        """scan lst file"""

        file = open(file_in, encoding="utf-8")
        try:
            file.read()
            file.close()
            file = open(file_in, encoding="utf-8")  # reopen file
        except UnicodeDecodeError:
            file.close()
            file = open(file_in)  # ascii codec

        tree = LSTTree()
        while True:
            line = file.readline().replace("\\", "/")
            if not line:
                break

            if not line.count("\t"):
                continue

            folder, size = line.split("\t")[:2]

            if folder[-1] == "/":
                tree.addFolder(folder, 0, 0, zip_as_one)
            else:
                tree.addFile(int(size), 1)
        return tree.getPlainList()

    @classmethod
    def readFromDirectory(cls, folder_in, folder_out="", create_lst=False):
        os.chdir(folder_in)
        folder_in = folder_in.rstrip("/\\")
        if not folder_out:
            folder_out = os.path.dirname(folder_in)

        result = []
        tree = LSTTree()

        for directory, folders, files in os.walk("."):
            directory = directory.replace("\\", "/").lstrip("./")  # get rid of ./ in start

            size = 0
            for file in files:
                size += os.path.getsize(os.path.join(directory, file))

            if directory:
                result.append("%s\t%i\t%s\t%s" % (directory + "/", size, *cls.getTime(directory)))

            tree.addFolder(directory, size, len(files))

            for file in files:
                full_name = os.path.join(directory, file)
                size = os.path.getsize(full_name)
                result.append("%s\t%i\t%s\t%s" % (file, size, *cls.getTime(full_name)))

        if create_lst:
            result = "\n".join(result).replace("/", "\\")
            os.chdir(folder_out)
            with open(os.path.split(folder_in)[1] + ".lst", "w", encoding="utf-8") as file:
                file.write(result)

        return tree.getPlainList()


class LSTStructure(bpy.types.Operator):
    bl_idname = "mesh.am1d_lst_structure"
    bl_label = "LST create structure"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        for mesh in context.scene.objects:
            if mesh.type != "MESH":
                continue
            name = mesh.name
            while name.count("/"):
                name = name.rsplit("/", 1)[0]
                if name in context.scene.objects:
                    b_matrix = mesh.matrix_world
                    mesh.parent = context.scene.objects[name]
                    mesh.matrix_world = b_matrix
                    break
        return {"FINISHED"}


class LSTOperator(bpy.types.Operator):
    bl_idname = "mesh.am1d_lst"
    bl_label = "LST create"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        dir_in = context.scene.am_settings.lst_input
        dir_out = context.scene.am_settings.lst_output
        show_as = context.scene.am_settings.lst_show
        create_lst = context.scene.am_settings.lst_create
        zip_as_one = not context.scene.am_settings.lst_zip

        if os.path.isfile(dir_in):
            data = LSTParser.readFromFile(dir_in, zip_as_one)
        else:
            data = LSTParser.readFromDirectory(dir_in, dir_out, create_lst)

        num_mat = bpy.data.materials.new("num_mat")
        num_mat.diffuse_color = 1, 1, 1

        size_mat = bpy.data.materials.new("size_mat")
        size_mat.diffuse_color = .24, .36, .75

        i = 0
        for cube in data:
            # Create an empty mesh and the object.
            if cube.r_size < context.scene.am_settings.lst_size:
                continue

            # size
            mesh = bpy.data.meshes.new('Basic_Cube')
            basic_cube = bpy.data.objects.new("Basic_Cube", mesh)
            basic_cube.active_material = size_mat
            basic_cube.name = cube.long_name
            basic_cube.location[0] = len(cube.dirs)
            basic_cube.location[2] = -i
            basic_cube.scale[0] = .8
            basic_cube.scale[2] = .8

            if show_as == "relative":
                basic_cube.location[1] = cube.r_size / 2 - cube.r_size
                basic_cube.scale[1] = cube.r_size
            else:
                basic_cube.location[1] = cube.a_size / 2 - cube.a_size
                basic_cube.scale[1] = cube.a_size

            # Add the object into the scene.
            context.scene.objects.link(basic_cube)
            context.scene.objects.active = basic_cube

            # Construct the bmesh cube and assign it to the blender mesh.
            bm = bmesh.new()
            bmesh.ops.create_cube(bm, size=1.0)
            bm.to_mesh(mesh)
            bm.free()

            # number of files
            mesh = bpy.data.meshes.new('Basic_Cube')
            basic_cube = bpy.data.objects.new("Basic_Cube", mesh)
            basic_cube.active_material = num_mat
            basic_cube.name = "_" + cube.long_name
            basic_cube.location[0] = len(cube.dirs)
            basic_cube.location[2] = -i
            basic_cube.scale[0] = .8
            basic_cube.scale[2] = .8

            if show_as == "relative":
                basic_cube.location[1] = (cube.r_num / 2 - cube.r_num) * -.01
                basic_cube.scale[1] = cube.r_num * -.01
            else:
                basic_cube.location[1] = (cube.a_num / 2 - cube.a_num) * -.01
                basic_cube.scale[1] = cube.a_num * -.01

            # Add the object into the scene.
            context.scene.objects.link(basic_cube)
            context.scene.objects.active = basic_cube

            # Construct the bmesh cube and assign it to the blender mesh.
            bm = bmesh.new()
            bmesh.ops.create_cube(bm, size=1.0)
            bm.to_mesh(mesh)
            bm.free()

            i += 1

        return {"FINISHED"}
