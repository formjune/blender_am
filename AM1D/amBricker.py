import bpy
import bmesh
import mathutils
import collections
import math
from .amTools import *


def findEdges(context, mesh, step, bottom):
    """find edges list [[Vector, Vector], ]"""
    context.scene.objects.active = mesh
    matrix = mesh.matrix_world
    vector = mathutils.Vector((0, 0, 1))
    bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(mesh.data)

    mesh_bottom = min([(matrix * v.co) for v in bm.verts], key=lambda v: v.z).z
    while bottom > mesh_bottom:
        bottom -= step

    # get information about edges
    edge_data = {}
    edge_maker = collections.namedtuple("edge", "bottom top proportion flat boundary index vector")
    for edge in bm.edges:
        vertex_1 = matrix * edge.verts[0].co
        vertex_2 = matrix * edge.verts[1].co
        if vertex_1.z > vertex_2.z:
            vertex_2, vertex_1 = vertex_1, vertex_2

        proportion = vertex_2.z - vertex_1.z
        flat = vertex_2.z == vertex_1.z
        boundary = edge.is_boundary
        vector_edge = vertex_2 - vertex_1
        edge_data[edge] = edge_maker(vertex_1, vertex_2, proportion, flat, boundary, edge.index, vector_edge)

    # walk through every face
    edge_list = []
    visited_edges = set()
    for face in bm.faces:
        normal = matrix * face.normal
        top_vertex = max([matrix * point.co for point in face.verts], key=lambda v: v.z).z
        current = bottom
        if 0 < normal.angle(vector) < (math.pi * 2):
            while current <= top_vertex:
                planar_edge = False
                for edge in face.edges:
                    edge = edge_data[edge]
                    if edge.index in visited_edges:  # skip repeating edge
                        continue
                    if edge.flat and edge.bottom == current:
                        planar_edge = True
                        edge_list.append([edge.bottom, edge.top])
                        visited_edges.add(edge.index)
                        break
                if not planar_edge:
                    found_point = []
                    for edge in face.edges:
                        edge = edge_data[edge]
                        if not (edge.bottom.z <= current <= edge.top.z) or edge.flat:
                            continue
                        percent = (current - edge.bottom.z) / edge.proportion
                        found_point.append(edge.bottom + edge.vector * percent)
                    if len(found_point) >= 2:
                        edge_list.append(found_point[:2])
                current += step
        else:  # create planar
            while current <= top_vertex:
                for edge in face.edges:
                    edge = edge_data[edge]
                    if edge.index in visited_edges:  # skip repeating edge
                        continue
                    if edge.boundary and edge.bottom == current:
                        edge_list.append([edge.bottom, edge.top])
                        visited_edges.add(edge.index)
                        break
                current += step
    bmesh.update_edit_mesh(mesh.data)
    mesh.select = False
    bpy.ops.object.mode_set(mode='OBJECT')
    return sorted(edge_list, key=lambda e: e[0].z)


def createPaths(mesh, unsorted_edges):
    """convert list of edges into list of paths [[Vector, ], ]"""
    edge_paths = []
    while unsorted_edges:
        edges = []
        height = round(unsorted_edges[-1][0].z, 2)
        while unsorted_edges and round(unsorted_edges[-1][0].z, 2) == height:
            edges.append(unsorted_edges.pop())
        sorted_edges = []
        need_sort = True
        while need_sort:
            need_sort = False
            for edge_1 in edges:
                for i, edge_2 in enumerate(sorted_edges):
                    if (edge_1[0] - edge_2[0]).length <= 1e-3:
                        sorted_edges[i] = edge_1[-1:0:-1] + edge_2
                    elif (edge_1[-1] - edge_2[0]).length <= 1e-3:
                        sorted_edges[i] = edge_1[:-1] + edge_2
                    elif (edge_1[0] - edge_2[-1]).length <= 1e-3:
                        sorted_edges[i] = edge_2[:-1] + edge_1
                    elif (edge_1[-1] - edge_2[-1]).length <= 1e-3:
                        sorted_edges[i] = edge_1 + edge_2[-2::-1]
                    else:
                        continue
                    need_sort = True
                    break
                else:
                    sorted_edges.append(edge_1)
            edges = sorted_edges
            sorted_edges = []
        edge_paths.extend(edges)
    # remove collapsed points and filter out paths with 1 point length
    matrix = mesh.matrix_world.inverted()
    up = matrix * mathutils.Vector((0, 0, 1)) - matrix * mathutils.Vector()
    sorted_paths = []
    for path in edge_paths:
        sorted_path = [path.pop(0)]
        for p in path:
            if (p - sorted_path[-1]).length > 1e-3:
                sorted_path.append(p)
        if len(sorted_path) > 1:
            point = matrix * ((sorted_path[0] + sorted_path[1]) / 2)
            vector = matrix * sorted_path[1] - point
            result, location, normal, index = mesh.closest_point_on_mesh(point)
            if up.cross(vector).angle(normal) > math.pi / 2:
                sorted_path.reverse()
            sorted_paths.append(sorted_path)
    return sorted_paths


def createBuildData(cls, edge_paths):
    build_data = []
    for path in edge_paths:
        closed = (path[0] - path[-1]).length <= 1e-3
        vertices = [cls.Vertex(path[0], path[-2] if closed else None, path[1])]
        for i in range(1, len(path) - 1):
            vertices.append(cls.Vertex(path[i], path[i - 1], path[i + 1]))
        if not closed:
            vertices.append(cls.Vertex(path[-1], path[-2], None))
        build_data.append([vertices, closed])
    return build_data


def cleanPaths(paths):
    for path in paths:
        i = 1
        while i < (len(path) - 1):
            vector_1 = path[i] - path[i - 1]
            vector_2 = path[i + 1] - path[i]
            if vector_1.angle(vector_2) <= math.pi * 1e-3:
                path.pop(i)
            else:
                i += 1


def createMesh(context, mesh_name):
    mesh_new = bpy.data.meshes.new("mesh")  # add a new mesh
    obj = bpy.data.objects.new(mesh_name + "_bricks", mesh_new)
    obj.select = True
    context.scene.objects.link(obj)  # put the object into the scene (link)
    context.scene.objects.active = obj  # set as the active object in the scene
    bpy.ops.object.mode_set(mode='EDIT')
    return obj, bmesh.from_edit_mesh(obj.data)


def generateNoDeform(obj, bm, build_data, height, thickness, double):
    for array, closed in build_data:
        loops = []
        for point in array:
            loops.append([bm.verts.new(p) for p in point.getLoop(height, thickness, double)])
        for i in range(len(loops) - 1):
            for j in range(4):
                bm.faces.new((loops[i][j], loops[i + 1][j], loops[i + 1][(j + 1) % 4], loops[i][(j + 1) % 4]))
        if closed and len(loops) > 2:
            for j in range(4):
                bm.faces.new((loops[-1][j], loops[0][j], loops[0][(j + 1) % 4], loops[-1][(j + 1) % 4]))
        else:
            bm.faces.new(loops[0])
            bm.faces.new(reversed(loops[-1]))
    bmesh.update_edit_mesh(obj.data)
    bpy.ops.object.mode_set(mode='OBJECT')


def generateDeform(obj, bm, build_data, height, thickness, double):
    for array, closed in build_data:
        loops = []
        for point in array:
            loops.append([bm.verts.new(p) for p in point.getPlane(height)])
        for i in range(len(loops) - 1):
            bm.faces.new((loops[i][0], loops[i + 1][0], loops[i + 1][1], loops[i][1]))
        if closed and len(loops) > 2:
            bm.faces.new((loops[-1][0], loops[0][0], loops[0][1], loops[-1][1]))
    bmesh.update_edit_mesh(obj.data)
    modifier = obj.modifiers.new('Solid_brick', type='SOLIDIFY')
    modifier.use_even_offset = True
    if double:
        modifier.offset = 0
        modifier.thickness = thickness * 2
    else:
        modifier.offset = 1
        modifier.thickness = thickness
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.object.mode_set(mode='OBJECT')


class Vertex(object):
    UP = mathutils.Vector((0, 0, 1))

    def __init__(self, pos, prev_vertex=None, next_vertex=None):
        self.vectors = []
        self.pos = pos
        if prev_vertex is not None:
            self.vectors.append(self.UP.cross(pos - prev_vertex).normalized())
        if next_vertex is not None:
            self.vectors.append(self.UP.cross(next_vertex - pos).normalized())

    def getOffset(self, offset):
        vector_n = sum(self.vectors, mathutils.Vector()).normalized()
        try:
            return vector_n / (vector_n * self.vectors[0]) * offset
        except ZeroDivisionError:
            return mathutils.Vector()

    def getLoop(self, height, offset, double):
        up = mathutils.Vector((0, 0, -height))
        p_in = self.pos + self.getOffset(offset)
        p_out = self.pos + self.getOffset(-offset if double else 0)
        return p_in + up, p_out + up, p_out, p_in

    def getPlane(self, height):
        return self.pos, self.pos + mathutils.Vector((0, 0, -height))


class BrickLiner(bpy.types.Operator):
    bl_idname = "mesh.am1d_brick_liner"
    bl_label = "Brick Liner"
    bl_options = {'REGISTER', 'UNDO'}

    step = BlenderParam("bricker_step")
    height = BlenderParam("bricker_height")
    offset = BlenderParam("bricker_offset")
    clean = BlenderParam("bricker_clean")
    double = BlenderParam("bricker_double")
    deform = BlenderParam("bricker_modifier")
    thickness = BlenderParam("bricker_thickness")

    def execute(self, context):

        generated = set()
        for mesh in context.selected_objects:
            edges = self.findEdges(context, mesh, self.step(), self.offset())
            paths = self.createPaths(mesh, edges)
            if self.clean():
                self.cleanPaths(paths)
            build_data = self.createBuildData(paths)
            obj, bm = self.createMesh(context, mesh.name)
            if self.deform():
                self.generateDeform(obj, bm, build_data, self.height(), self.thickness(), self.double())
            else:
                self.generateNoDeform(obj, bm, build_data, self.height(), self.thickness(), self.double())
            generated.add(obj)
        for node in context.scene.objects:
            node.select = node in generated
        return {"FINISHED"}


class Check(bpy.types.Operator):
    bl_idname = "mesh.am1d_brick_checker"
    bl_label = "Bricker Check"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bad_meshes = set()
        bad_edges = 0
        for mesh in context.selected_objects:
            is_bad = False
            context.scene.objects.active = mesh
            bpy.ops.object.mode_set(mode='EDIT')
            bm = bmesh.from_edit_mesh(mesh.data)
            for face in bm.faces:
                face.select = False
            for edge in bm.edges:
                if len(edge.link_faces) > 2:
                    is_bad = True
                    edge.select = True
                    bad_edges += 1
            if is_bad:
                bad_meshes.add(mesh)
            bpy.ops.object.mode_set(mode='OBJECT')
        if bad_meshes:
            for node in context.scene.objects:
                node.select = node in bad_meshes
            self.report({"INFO"}, "bad: %i meshes, %i edges" % (len(bad_meshes), bad_edges))
        else:
            self.report({"INFO"}, "everything is ok")
        return {"FINISHED"}
