# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# https://bitbucket.org/formjune/blender_am


import bpy
from . import amBricker
from . import amBuilders
from . import amLST
from . import amMaterials
from . import amMeshTools
from . import amOBJ
from . import amObjects
from . import amScribe
from . import amSVG
from . import amSettings
from . import amUI


bl_info = {
    "name": "AM 1D Tools",
    "author": "Andrey Menshikovn, Paul Kotelevets aka 1D_Inc (concept design)",
    "version": ("1", "0", "1"),
    "blender": (2, 7, 9),
    "location": "View3D > Tool Shelf > 1D > AM1D",
    "description": "AM 1D script pack",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Mesh"}


classes = [
    amSettings.Settings, amSettings.CollapseSettings, amUI.Layout, amBricker.BrickLiner, amBricker.Check,
    amBuilders.BuildSlope, amBuilders.BuildIsolines, amBuilders.Fencer, amBuilders.WormEdges, amLST.LSTStructure,
    amLST.LSTOperator, amMaterials.RasterScan, amMaterials.MatsEqualizeBetween, amMaterials.MatsEqualizeAdd,
    amMaterials.MatsColorize, amMaterials.FuncloneFromActive, amMaterials.MatsPlane, amMaterials.MatsTexlist,
    amMaterials.SelectedMaterials, amMaterials.AllMaterials, amMeshTools.CornerCross, amMeshTools.ExtendCross,
    amMeshTools.StoreFacesOperator, amMeshTools.ProjectEdgesOperator, amMeshTools.RotatePatch, amMeshTools.SurfaceSew,
    amMeshTools.SurfaceSewSelectByRadius, amMeshTools.PokeLoops, amMeshTools.PokeLoopsDebug,
    amMeshTools.SurfaceLevelset, amMeshTools.Dissect, amMeshTools.MapCut, amMeshTools.BevelFromSeam,
    amMeshTools.SeamFromBevel, amOBJ.ObjOperator, amOBJ.ObjSplit, amOBJ.ObjTopologySplit, amOBJ.ObjCleanup,
    amObjects.CombineOperator, amObjects.IslandsPlacementUV, amObjects.FarOrigin, amObjects.ConnectOrigins,
    amObjects.OriginToCenter, amObjects.OriginToDown, amObjects.OriginToZero, amObjects.PutTriangulation,
    amObjects.SelectTriangulate, amObjects.InvertVisibility, amObjects.InvertLayersVisibility,
    amObjects.ConvertRenderability, amObjects.UVMapSearch, amObjects.SSRead, amObjects.SSName, amObjects.SSFind,
    amObjects.SSFix, amObjects.SpreadByInstances, amObjects.SpreadByPoints, amObjects.InstancesApplyRotation,
    amObjects.SpreadGroup, amObjects.SelectScale, amScribe.Scribe, amScribe.ScribeWriter, amScribe.ScribeReader,
    amSVG.SVGSplit, amSVG.SVGMerge, amSVG.SVGParseImages, amSVG.SVGCopyImages, amSVG.SVGIconSlicer,
    amObjects.RemoveTriangulation
]


def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.Scene.am_settings = bpy.props.PointerProperty(type=amSettings.Settings)
    bpy.types.Scene.am_collapse_settings = bpy.props.PointerProperty(type=amSettings.CollapseSettings)
    

def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
