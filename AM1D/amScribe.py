import os
import mathutils
import bpy
import bmesh
from .amTools import *


def createText(name, message, location, scale, offset, height, names, colors, mesh_data):
    bpy.ops.object.mode_set(mode="OBJECT")
    mesh = bpy.data.meshes.new("mesh")
    obj = bpy.data.objects.new(name, mesh)
    obj.location = location
    scene = bpy.context.scene
    scene.objects.link(obj)
    scene.objects.active = obj

    # set up materials
    for name, color in zip(names, colors):
        mat = bpy.data.materials.get(name)
        if mat is None:
            mat = bpy.data.materials.new(name=name)
            mat.diffuse_color = color
        mesh.materials.append(mat)

    # create text
    bm = bmesh.new()
    y = 0
    for line in message.split("\n"):
        x = 0

        for letter in line:
            if letter not in mesh_data:
                continue

            width, points, faces, mats = mesh_data[letter]
            verts = [bm.verts.new(((p[0] + x) * scale, (p[1] + y) * scale, p[2] * scale)) for p in points]
            for face, mat_index in zip(faces, mats):
                bm.faces.new([verts[v] for v in face]).material_index = mat_index
            x += width

        y -= (1 + offset) * height

    bm.to_mesh(mesh)
    bm.free()
    return obj


def readFile(file_name):
    try:
        file = open(file_name, encoding="utf-8")
        height = eval(file.readline().rstrip("\n"))
        names = eval(file.readline().rstrip("\n"))
        colors = eval(file.readline().rstrip("\n"))
        mesh_data = eval(file.readline().rstrip("\n"))
        file.close()
        return height, names, colors, mesh_data
    except Exception:
        raise RuntimeError


class Scribe(bpy.types.Operator):

    bl_idname = "mesh.am1d_scribe"
    bl_label = "Scribe"
    bl_options = {'REGISTER', 'UNDO'}

    use_cursor = BlenderParam("scribe_use_cursor")
    use_offset = BlenderParam("scribe_use_offset")
    use_obname = BlenderParam("scribe_use_obname")
    font = BlenderParam("scribe_font")
    offset = BlenderParam("scribe_offset")
    scale = BlenderParam("scribe_scale")

    def execute(self, context):
        if self.use_obname():
            message = context.active_object.name
        elif len(bpy.data.texts) == 1:
            message = bpy.data.texts[0].as_string()
        elif "Text" in bpy.data.texts:
            message = bpy.data.texts["Text"].as_string()
        else:
            return {"FINISHED"}

        cursor = context.scene.cursor_location if self.use_cursor() else mathutils.Vector()
        try:
            font = self.font() or os.path.dirname(__file__) + "/default"
            obj = createText("text", message, cursor, self.scale(), self.offset(), *readFile(font))
            for mesh in context.scene.objects:
                mesh.select = False
            obj.select = True
            context.scene.objects.active = obj
        except RuntimeError:
            pass
        return {"FINISHED"}


class ScribeWriter(bpy.types.Operator):
    bl_idname = "mesh.am1d_scribe_writer"
    bl_label = "Scribe Writer"
    bl_options = {'REGISTER', 'UNDO'}

    font = BlenderParam("scribe_font_editor")

    def execute(self, context):

        mesh_data = {}
        height = 0
        names = []
        colors = []

        for slot in context.active_object.material_slots:
            material = slot.material
            if not material:
                continue
            colors.append(tuple(material.diffuse_color))
            names.append(material.name)

        for mesh in context.selected_objects:

            context.scene.objects.active = mesh
            bpy.ops.object.mode_set(mode='EDIT')
            bm = bmesh.from_edit_mesh(mesh.data)
            points_w = [mesh.matrix_world * v.co for v in bm.verts]
            min_x = min(points_w, key=lambda p: p.x).x
            max_x = max(points_w, key=lambda p: p.x).x
            min_y = min(points_w, key=lambda p: p.y).y
            max_y = max(points_w, key=lambda p: p.y).y
            max_z = max(points_w, key=lambda p: p.z).z

            origin = mathutils.Vector((min_x, max_y, max_z))
            height = max(height, round(max_y - min_y, 3))
            width = round(max_x - min_x, 3)

            points = []
            faces = []
            materials = []
            for point in points_w:
                vector = point - origin
                points.append((round(vector.x, 3), round(vector.y, 3), round(vector.z, 3)))

            for face in bm.faces:
                faces.append([v.index for v in face.verts])
                materials.append(face.material_index)

            bm.free()
            bpy.ops.object.mode_set(mode='OBJECT')
            mesh_data[mesh.name[:1]] = width, points, faces, materials

        with open(self.font(), "w", encoding="utf-8") as file:
            file.write("%s\n%s\n%s\n%s" % (height, names, colors, mesh_data))

        return {"FINISHED"}


class ScribeReader(bpy.types.Operator):
    bl_idname = "mesh.am1d_scribe_reader"
    bl_label = "Scribe Reader"
    bl_options = {'REGISTER', 'UNDO'}

    font = BlenderParam("scribe_font_editor")

    def execute(self, context):

        try:
            data = readFile(self.font())
        except RuntimeError:
            return {"FINISHED"}

        cursor = mathutils.Vector()
        letters = []
        for key in sorted(data[3].keys()):
            letters.append(createText(key, key, cursor, 1, 1, *data))
            cursor.x += data[3][key][0]

        for mesh in context.scene.objects:
            mesh.select = False
        for mesh in letters:
            mesh.select = True
        return {"FINISHED"}
